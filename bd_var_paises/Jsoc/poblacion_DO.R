library(dplyr)
library(haven)

lapop12 <- read_sav("/home/julian/Documents/tesis-maestria_participacion-politica/bd_LAPOP/datos-brutos/regional-merge-files/2012/data-set_AmericasBarometer Merged 2012 Spanish Rev1.5_W.sav") |> 
  as.data.frame()

keep <- c("pais", "ed", "gi0", "www1", 
          "etid", "vb2", "q10new", 
          "vb10", "pol1", "cct1new", 
          "ur", "tamano", "exc7")

lapop12 <- lapop12[ , names(lapop12) %in% keep]


lapop12 <- as.data.frame(lapply(lapop12, as_factor))

do <- lapop12 |> 
  select(pais, etid) |> 
  filter(pais == "Rep. Dom.") |> 
  group_by(etid) |> 
  droplevels()

levels(do$etid)

unique(do)

round(prop.table(table(do)), 3)

levels(lapop12$etid)
