README
===

Plan de gestión de datos para la tesis
===


**Descripción general** 

En este plan se encuentra toda la información de la tesis sobre participación política en América Latina realizado por Julian Mokwa Félix y dirigido por el Dr. Fernando Nieto Morales. Para mayores informes comuníquese con jmokwa@colmex.mx o julianmfx@gmail.com.

En enlace de este plan se encuentra en [docutopia](https://docutopia.tupale.co/hQ7WGtUNRjOqRQik5QlWUA?view) y también en en [GitLab](https://gitlab.com/julianmfx/tesis-maestria-pp).

A continuación se describe como está organizada la información. Si usted se integra al grupo de trabajo y va a recuperar o generar información, siga las siguientes instrucciones para identificar las carpetas correspondientes y el sistema de nombrado de los archivos.

# 1  Términos de uso del material del proyecto: restricciones, licencias.

:warning: Por definir :warning:

# 2 Personas que participan en el proyecto y abreviaciones con las que se identificaran como creadores o encargados de procesos de los documentos.
   * JMF: Julian Mokwa Félix

# 3 Bases de datos
## 3.1 Captura
:warning: **Eso no ha sido implementado**

- En que formato voy a hacer
- Captura manual

:warning:

- Las bases de datos serán capturas a partir de del [Proyecto de Opinión Pública para América Latina (LAPOP)](http://datasets.americasbarometer.org/database/).
- Las bases están disponibles por país y por año.
- No hay una base de datos general completa. Es decir, hay un subconjunto de datos desde 2004-2018, pero este conjunto está disponible de manera parcial para usuarios gratuitos. El conjunto completo está disponible solamente para usuarios pagos.
- El proceso de captura empezará por el año de 2012 y por los países en orden alfabética.

### 3.1.1 Captura de los archivos generales

Están disponibles archivos generales sobre las encuestas realizadas a cada año.
Los archivos se componen de:
- Un cuestionario común separado por año
- Manual del encuestador para los años de 2014 y de 2018/19
- Información técnicas
- Un archivo con las fechas de trabajo de campo de todas las encuestas reunidas en un archivo general
- Un archivo con el esquema de ponderación
- Todos los archivos generales serán descargados y guardados en: **"~/archivos_generales"** 
- Seguirán las mismas reglas de nombramiento de las bases de datos.

**Nombramiento de los archivos generales**
1. Se conservarán los nombres originales.
2. A la encuestas se les agregará al inicio: "encuesta-general_AÑO".
3. A las informaciones técnicas se les agregará al inicio: "inf-tec_AÑO".
4. A los manuales se les agregará al inicio: "manual_AÑO".
5. A las fechas de trabajo de campo se les agregará al inicio: "trabajo-campo"
6. Al esquema de ponderación se le agregará al inicio: "ponderacion"



###  3.1.2 Captura de las bases de datos
- Son de tres a cuatro archivos por país y por año.

    1. La base de datos en formato .sav (SPSS), que serán convertidas en dataframes en R
    2. La información técnica en formato .pdf
    3. El cuestionario en formato .pdf
    4. (opcional) El changelog


## 3.2 Almacenamiento
La carpeta madre del proyecto está en **"/home/julian/Documents/tesis-maestria_participacion-politica"**. Abreviada en este plan como **"~"**.

Las bases de datos serán almacenadas en: **"~/bd_LAPOP"**. De ahí serán creadas dos subcarpetas, "**datos_brutos**" y "**datos_procesados**".

- La carpeta **datos_brutos** será una carpeta de solamente lectura para los datos originales capturados.
- La carpeta **datos_procesados** contedrá las bases modificadas por cualquier tipo de análisis, limpieza, enriquecimiento, etc.

- Las bases son guardadas en un carpeta "**país**", seguida de una carpeta "**año**".

        Ejemplo:  
        /home/julian/Documents/tesis-maestria_participacion-politica/bd_LAPOP/datos_brutos/AR/2012

 - Serán almacenadas 4 encuestas: 2012, 2014, 2016 y 2018.

## 3.3 Nombramiento
El nombramiento seguirá el siguiente patrón:
- "SIGLA PAÍS" 
- "Año" 
- "tipo de objeto" 
- "tema-subtema" 
- "nombre original sin espacios, tildes y signos" 
- "extensión"
        
        Ejemplos:
        1. SP_0000_bd_nombre-original.abc
        2. AR_2012_bd_1744791410Argentina-LAPOP-AmericasBarometer-2012-Rev1_W.sav



### 3.3.1 Criterios y diccionario de abreviaciones
   * Los nombres arriba identificados serán separados por guíon bajo entre ellos y por hífen dentro de un mismo nombre.
   * Del nombre original serán quitados espacios, guiones bajos, tildes y signos.
   * Se buscará que los nombres sean los más cortos posibles.
   * El nombramiento será sin espacios, tildes y signos.
   * Sigla país y año será en caja alta.
   * Tipo de objeto y tema serán en caja baja.
   * Si en tema hay más de una palabra, serán separadas por guion.
   * Tipo de objeto se refiere al objeto: puede ser libro digital, base datos, artículo, imagen, audio.
   * Tema se refiere al tema del objeto: los temas involucrán desde encuestas hasta palabras-claves específicas a fin de identificar el contenido central del objeto.

## 3.4 Lectura de las bases
Las bases están en formato .sav y es necesario un paquete estadítsico para leerlas. Se optó por utilizar el paquete denominado de [haven](https://cran.r-project.org/web/packages/haven/haven.pdf).

# 4. Cuadro de clasificación
:warning: Por definir :warning:

# 5. Git (control de versiones)
El control de versiones será realizado mediante git.
Se optó por subir el proyecto a [GitLab](https://about.gitlab.com) por ser más de código abierto.

# 6. Análisis
Los análisis se dividirán en dos partes. Una es la parte de revisión teórica y otra serán los análisis estadísticos realizados a partir de las bases de datos. Ambos estarán en sus carpetas respectivas **~/revision_teorica** y **~/bd_LAPOP/datos_procesados**. Además, también estarán en la carpeta **~/entregas**.

## 6.1 Nombramiento de las subcarpetas de entregas
Las subcarpetas de entregas serán nombradas a partir del siguiente orden:
1. La palabra reporte
2. El número del reporte
3. La fecha en que el reporte fue entregue
4. Una o un par de palabras-claves identificando el tema (y subtema) del reporte
**Ejemplo:** reporte01_19-08_preguntas_comportamiento-politico

----
# 7. Organización de las carpetas (ejemplo)
    1. tesis-maestría
        1.1 bd_LAPOP
            1.1.1 datos_brutos
                1.1.1.1 regional-merge-files
            1.1.2 datos_procesados
        1.2 codigo
        1.3 entregas
        1.4 archivos_generales
        1.5 revision_teorica
        
## 7.1 Función de las carpetas
1. La carpeta **datos_brutos** será una carpeta de solamente lectura para los datos originales capturados.
2. La carpeta **datos_procesados** contedrá las bases modificadas por cualquier tipo de análisis, limpieza, enriquecimiento, etc.
3. La carpeta **codigos** contendra los codigos utilizados por los análsis
4. La carpeta **entregas** contendrá todos los documentos necesarios para exponer el avanze de la tesis, como reportes, tablas, imágenes, archivos de texto y cualquier otro documento que se considere necesario para realizar una entrega.
5. La carpeta **archivos_generales** contienen los archivos descargados en la página de [LAPOP](https://www.vanderbilt.edu/lapop/studies-country.php) con documentos que se refieren a todos los países.
6. La carpeta **revision_teorica** contedrá los documentos para construir la fundamentación teórica de la tesis, como artículos, reportes, libros, entre otros.


# 8. Abreviaciones
- La sigla país seguirá la normativa [ISO 3166](https://www.iso.org/iso-3166-country-codes.html).
- LAPOP: se refiere al *Latin America Public Opinion Project*.
- bd: base de datos
- "~": /home/julian/Documents/tesis-maestria_participacion-politica
- JMF: Julian Mokwa Félix
- inf-tec: información técnica
---

Proceso de investigación
===

# 1. Fase 1 - Descripción de los datos

## 1.1 Selección de los cuestionarios

Haré un programa piloto. Voy a utilizar la encuesta regional de 2012 sobre Argentina y la Encuesta General de 2012 para hacer una selección de las preguntas relevantes. Con base en eso, voy a ver si eso se repite y, por lo tanto, se puede hacer para las otras encuestas.

**Resultado esperado:** positivo debido a mi selección solamente de las preguntas generales.

## 1.2 Cambios del proceso

:warning: Actualizar :warning: