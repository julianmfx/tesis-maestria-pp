---
title: "bases por país"
author: "jmf"
date: "`r Sys.Date()`"
output:   
  html_document:
    toc: TRUE
    toc_float: TRUE
    theme: cosmo
    highlight: zenburn
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)
```

# Introducción

Generar bases por país

# Librerías
```{r librerias, results = "hide", message = FALSE, warning = FALSE}
library(dplyr) # to use pipe operators
library(haven) # to import database
library(tidyverse) # to use ggplot()
library(purrr) # to use map()
```

# Importando la base de datos
```{r import}
lapop12 <- readr::read_rds(
  "/home/julian/Documents/tesis-maestria_participacion-politica/bd_LAPOP/datos-procesados/lapop12_dic.Rds")
```  

## Verificación de los valores
```{r}
glimpse(lapop12)
```

```{r}
str(lapop12)
```

```{r}
names(lapop12)
```

# Generando base de datos general
```{r}
drop <- c("cp20", "prot7", "prot6", "vb20", "q1", "q2")
lapop12_EFA <- lapop12[ , !(names(lapop12) %in% drop)]
```

## Limpieza de la base general

Transformando todas las variables para un valor numérico
```{r}
lapop12_EFA <- lapply(lapop12_EFA, as.numeric)
lapop12_EFA <- as.data.frame(lapop12_EFA)
```

```{r}
str(lapop12_EFA)
```
```{r}
glimpse(lapop12_EFA)
```

```{r}
names(lapop12_EFA)
```


# Exportación de las bases según el país

Los valores númericos están asociados a los valores codificados en la base original.
Para estar seguro de cual base era para cada país, consulté la base original, la base dicotomizada, la base EFA y el script de funciones. Aunque todo seguía el patrón de orden en que los países aparecían en otras tablas y funciones de la tesis, fue bueno checar uno a uno.

```{r}
setwd("/home/julian/Documents/tesis-maestria_participacion-politica/codigo/c08_EFA/by_country")
lapop12_EFA %>%
  split(.$pais) %>%
  imap(~write.table(.x, 
                    paste0(.y, '.csv'), 
                    row.names = FALSE, 
                    quote = FALSE, 
                    sep = ","))
```

Codificación de la variable país:

* 1: México
* 2: Guatemala
* 3: El Salvador
* 4: Honduras
* 5: Nicaragua
* 6: Costa Rica
* 7: Panamá
* 8: Colombia
* 9: Ecuador
* 10: Bolivia
* 11: Perú
* 12: Paraguay
* 13: Chile
* 14: Uruguay
* 15: Brasil
* 16: Argentina
* 17: Rep. Dom.


```{r}
library("haven")
lapop12_base <- read_sav(file = "/home/julian/Documents/tesis-maestria_participacion-politica/bd_LAPOP/datos-brutos/regional-merge-files/2012/data-set_AmericasBarometer Merged 2012 Spanish Rev1.5_W.sav")
unique(lapop12_base$pais)
```

```{r}
lapop12_EFA %>% 
  filter(pais == 11)
```

```{r}
lapop12 %>% 
  filter(pais == "Ecuador")
```


```{r}
lapop12_EFA %>% 
  filter(pais == 9)
```

```{r}
is.labelled(lapop12$pais)
```


```{r}
View(lapop12)
```






