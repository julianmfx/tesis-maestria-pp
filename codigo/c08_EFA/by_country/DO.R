# Rep. Dom.
rm(list = ls(all = TRUE))

# Modelos por pais

Los modelos por pais serán generados según la siguiente orden:
  
* 1: México
* 2: Guatemala
* 3: El Salvador OK
* 4: Honduras OK
* 5: NicDOagua OK
* 6: Costa Rica OK
* 7: Panamá OK
* 8: Colombia OK
* 9: Ecuador OK
* 10: Bolivia OK
* 11: Perú OK
* 12: PDOaguay OK
* 13: Chile OK
* 14: Uruguay OK
* 15: DOasil OK
* 16: DOgentina OK
* 17: Rep. Dom. OK


# Modelo 1 = Rep. Dom. (DO)

library(dplyr)
library(corpcor)
library(GPDOotation)
library(psych)

DO <- read.csv("DO.csv")
DO <- DO[ , !names(DO) %in% "pais"]

DO_compl <- na.omit(DO) # listwise deletion

glimpse(DO_compl)

DO_matrix <- cor(DO_compl)
DO_matrix
round(DO_matrix, 2)

cortest.bartlett(DO_compl) # Prueba de bartlett

KMO(DO_compl)

pc_DO1 <- principal(DO_compl, nfactors = 18, rotate = "none") # Modelo base
plot(pc_DO1$values, type = "b") # Gráfica de codo para seleccionar num de factores

pc_DO2 <- principal(DO_compl, nfactors = 4, rotate = "oblimin")
print.psych(pc_DO2, cut = 0.3, sort = TRUE)

source("https://raw.githubusercontent.com/franciscowilhelm/r-collection/master/fa_table.R") # to use fa_table()
tables_DO <- fa_table(pc_DO2)

gt::gtsave(data = tables_DO$ind_table, 
           filename = "DO.html", 
           path = "/home/julian/Documents/tesis-maestria_participacion-politica/illustrations/efa/")
gt::gtsave(data = tables_DO$f_table, 
           filename = "DO_parameters.html" , 
           path = "/home/julian/Documents/tesis-maestria_participacion-politica/illustrations/efa/")
