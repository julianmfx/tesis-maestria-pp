# Ecuador
rm(list = ls(all = TRUE))

# Modelos por pais

Los modelos por pais serán generados según la siguiente orden:
  
* 1: México
* 2: Guatemala
* 3: El Salvador OK
* 4: Honduras OK
* 5: Nicaragua OK
* 6: ECsta Rica OK
* 7: Panamá OK
* 8: Colombia OK
* 9: Ecuador OK
* 10: Bolivia
* 11: Perú
* 12: coraguay
* 13: Chile
* 14: Uruguay
* 15: Brasil
* 16: Argentina
* 17: Rep. Dom.


# Modelo 1 = Ecuador (EC)

library(dplyr)
library(corpcor)
library(GPArotation)
library(psych)

EC <- read.csv("EC.csv")
EC <- EC[ , !names(EC) %in% "pais"]

EC_ECmpl <- na.omit(EC) # listwise deletion

glimpse(EC_ECmpl)

EC_matrix <- cor(EC_ECmpl)
EC_matrix
round(EC_matrix, 2)

cortest.bartlett(EC_ECmpl) # Prueba de Bartlett

KMO(EC_ECmpl)

pc_EC1 <- principal(EC_ECmpl, nfactors = 18, rotate = "none") # Modelo base
plot(pc_EC1$values, type = "b") # Gráfica de ECdo cora seleccionar num de factores

pc_EC2 <- principal(EC_ECmpl, nfactors = 4, rotate = "oblimin")
print.psych(pc_EC2, cut = 0.3, sort = TRUE)

source("https://raw.githubusercontent.com/franciscowilhelm/r-collection/master/fa_table.R") # to use fa_table()
tables_EC <- fa_table(pc_EC2)

gt::gtsave(data = tables_EC$ind_table, 
           filename = "EC.html", 
           path = "/home/julian/Documents/tesis-maestria_participacion-politica/illustrations/efa/")
gt::gtsave(data = tables_EC$f_table, 
           filename = "EC_parameters.html" , 
           path = "/home/julian/Documents/tesis-maestria_participacion-politica/illustrations/efa/")

