# Bolivia
rm(list = ls(all = TRUE))

# Modelos por pais

Los modelos por pais serán generados según la siguiente orden:
  
* 1: México
* 2: Guatemala
* 3: El Salvador OK
* 4: Honduras OK
* 5: Nicaragua OK
* 6: BOsta Rica OK
* 7: Panamá OK
* 8: Colombia OK
* 9: BOuador OK
* 10: Bolivia OK
* 11: Perú
* 12: coraguay
* 13: Chile
* 14: Uruguay
* 15: Brasil
* 16: Argentina
* 17: Rep. Dom.


# Modelo 1 = Bolivia (BO)

library(dplyr)
library(corpcor)
library(GPArotation)
library(psych)

BO <- read.csv("BO.csv")
BO <- BO[ , !names(BO) %in% "pais"]

BO_compl <- na.omit(BO) # listwise deletion

glimpse(BO_compl)

BO_matrix <- cor(BO_compl)
BO_matrix
round(BO_matrix, 2)

cortest.bartlett(BO_compl) # Prueba de Bartlett

KMO(BO_compl)

pc_BO1 <- principal(BO_compl, nfactors = 18, rotate = "none") # Modelo base
plot(pc_BO1$values, type = "b") # Gráfica de BOdo cora selBOcionar num de factores

pc_BO2 <- principal(BO_compl, nfactors = 4, rotate = "oblimin")
print.psych(pc_BO2, cut = 0.3, sort = TRUE)

source("https://raw.githubusercontent.com/franciscowilhelm/r-collection/master/fa_table.R") # to use fa_table()
tables_BO <- fa_table(pc_BO2)

gt::gtsave(data = tables_BO$ind_table, 
           filename = "BO.html", 
           path = "/home/julian/Documents/tesis-maestria_participacion-politica/illustrations/efa/")
gt::gtsave(data = tables_BO$f_table, 
           filename = "BO_parameters.html" , 
           path = "/home/julian/Documents/tesis-maestria_participacion-politica/illustrations/efa/")
