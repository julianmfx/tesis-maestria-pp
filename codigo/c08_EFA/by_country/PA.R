# Panamá
rm(list = ls(all = TRUE))

# Modelos por país

Los modelos por país serán generados según la siguiente orden:
  
* 1: México
* 2: Guatemala
* 3: El Salvador OK
* 4: Honduras OK
* 5: Nicaragua OK
* 6: Costa Rica OK
* 7: Panamá OK
* 8: Colombia
* 9: Ecuador
* 10: Bolivia
* 11: Perú
* 12: Paraguay
* 13: Chile
* 14: Uruguay
* 15: Brasil
* 16: Argentina
* 17: Rep. Dom.


# Modelo 1 = Panamá (PA)

library(dplyr)
library(corpcor)
library(GPArotation)
library(psych)
library(gt) # to export PCA table
library(gtsummary) # to use as_gt()

PA <- read.csv("PA.csv")
PA <- PA[ , !names(PA) %in% "pais"]

PA_compl <- na.omit(PA) # listwise deletion

glimpse(PA_compl)

PA_matrix <- cor(PA_compl)
PA_matrix
round(PA_matrix, 2)

cortest.bartlett(PA_compl) # Prueba de Bartlett

KMO(PA_compl)

pc_PA1 <- principal(PA_compl, nfactors = 18, rotate = "none") # Modelo base
plot(pc_PA1$values, type = "b") # Gráfica de codo para seleccionar num de factores

pc_PA2 <- principal(PA_compl, nfactors = 4, rotate = "oblimin")
print.psych(pc_PA2, cut = 0.3, sort = TRUE)

source("https://raw.githubusercontent.com/franciscowilhelm/r-collection/master/fa_table.R") # to use fa_table()
tables_PA <- fa_table(pc_PA2)

gt::gtsave(data = tables_PA$ind_table, 
           filename = "PA.html", 
           path = "/home/julian/Documents/tesis-maestria_participacion-politica/illustrations/efa/")
gt::gtsave(data = tables_PA$f_table, 
           filename = "PA_parameters.html" , 
           path = "/home/julian/Documents/tesis-maestria_participacion-politica/illustrations/efa/")

