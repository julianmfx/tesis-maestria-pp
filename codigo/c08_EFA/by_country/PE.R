# Perú
rm(list = ls(all = TRUE))

# Modelos por pais

Los modelos por pais serán generados según la siguiente orden:
  
* 1: México
* 2: Guatemala
* 3: El Salvador OK
* 4: Honduras OK
* 5: Nicaragua OK
* 6: PEsta Rica OK
* 7: Panamá OK
* 8: Colombia OK
* 9: Ecuador OK
* 10: Bolivia OK
* 11: Perú OK
* 12: Paraguay
* 13: Chile
* 14: Uruguay
* 15: Brasil
* 16: Argentina
* 17: Rep. Dom.


# Modelo 1 = Perú (PE)

library(dplyr)
library(corpcor)
library(GPArotation)
library(psych)

PE <- read.csv("PE.csv")
PE <- PE[ , !names(PE) %in% "pais"]

PE_compl <- na.omit(PE) # listwise deletion

glimpse(PE_compl)

PE_matrix <- cor(PE_compl)
PE_matrix
round(PE_matrix, 2)

cortest.bartlett(PE_compl) # Prueba de Bartlett

KMO(PE_compl)

pc_PE1 <- principal(PE_compl, nfactors = 18, rotate = "none") # Modelo base
plot(pc_PE1$values, type = "b") # Gráfica de PEdo cora selPEcionar num de factores

pc_PE2 <- principal(PE_compl, nfactors = 4, rotate = "oblimin")
print.psych(pc_PE2, cut = 0.3, sort = TRUE)

source("https://raw.githubusercontent.com/franciscowilhelm/r-collection/master/fa_table.R") # to use fa_table()
tables_PE <- fa_table(pc_PE2)

gt::gtsave(data = tables_PE$ind_table, 
           filename = "PE.html", 
           path = "/home/julian/Documents/tesis-maestria_participacion-politica/illustrations/efa/")
gt::gtsave(data = tables_PE$f_table, 
           filename = "PE_parameters.html" , 
           path = "/home/julian/Documents/tesis-maestria_participacion-politica/illustrations/efa/")

