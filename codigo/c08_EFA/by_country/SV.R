# El Salvador
rm(list = ls(all = TRUE))

# Modelos por país

Los modelos por país serán generados según la siguiente orden:
  
* 1: México
* 2: Guatemala
* 3: El Salvador OK
* 4: Honduras
* 5: Nicaragua
* 6: Costa Rica
* 7: Panamá
* 8: Colombia
* 9: Ecuador
* 10: Bolivia
* 11: Perú
* 12: Paraguay
* 13: Chile
* 14: Uruguay
* 15: Brasil
* 16: Argentina
* 17: Rep. Dom.


# Modelo 1 = El Salvador (SV)

library(dplyr)
library(corpcor)
library(GPArotation)
library(psych)

SV <- read.csv("SV.csv")
SV <- SV[ , !names(SV) %in% "pais"]

SV_compl <- na.omit(SV) # listwise deletion

glimpse(SV_compl)

SV_matrix <- cor(SV_compl)
SV_matrix
round(SV_matrix, 2)

cortest.bartlett(SV_compl) # Prueba de Bartlett

KMO(SV_compl)

pc_SV1 <- principal(SV_compl, nfactors = 18, rotate = "none") # Modelo base
plot(pc_SV1$values, type = "b") # Gráfica de codo para seleccionar num de factores

pc_SV2 <- principal(SV_compl, nfactors = 4, rotate = "oblimin")
print.psych(pc_SV2, cut = 0.3, sort = TRUE)

source("https://raw.githubusercontent.com/franciscowilhelm/r-collection/master/fa_table.R") # to use fa_table()
tables_SV <- fa_table(pc_SV2)

gt::gtsave(data = tables_SV$ind_table, 
           filename = "SV.html", 
           path = "/home/julian/Documents/tesis-maestria_participacion-politica/illustrations/efa/")
gt::gtsave(data = tables_SV$f_table, 
           filename = "SV_parameters.html" , 
           path = "/home/julian/Documents/tesis-maestria_participacion-politica/illustrations/efa/")

