# Chile
rm(list = ls(all = TRUE))

# Modelos por pais

Los modelos por pais serán generados según la siguiente orden:
  
* 1: México
* 2: Guatemala
* 3: El Salvador OK
* 4: Honduras OK
* 5: Nicaragua OK
* 6: CLsta Rica OK
* 7: Panamá OK
* 8: Colombia OK
* 9: Ecuador OK
* 10: Bolivia OK
* 11: Perú OK
* 12: Paraguay OK
* 13: Chile OK
* 14: Uruguay
* 15: Brasil
* 16: Argentina
* 17: Rep. Dom.


# Modelo 1 = Chile (CL)

library(dplyr)
library(corpcor)
library(GPArotation)
library(psych)

CL <- read.csv("CL.csv")
CL <- CL[ , !names(CL) %in% "pais"]

CL_compl <- na.omit(CL) # listwise deletion

glimpse(CL_compl)

CL_matrix <- cor(CL_compl)
CL_matrix
round(CL_matrix, 2)

cortest.bartlett(CL_compl) # Prueba de Bartlett

KMO(CL_compl)

pc_CL1 <- principal(CL_compl, nfactors = 18, rotate = "none") # Modelo base
plot(pc_CL1$values, type = "b") # Gráfica de codo para seleccionar num de factores

pc_CL2 <- principal(CL_compl, nfactors = 4, rotate = "oblimin")
print.psych(pc_CL2, cut = 0.3, sort = TRUE)

source("https://raw.githubusercontent.com/franciscowilhelm/r-collection/master/fa_table.R") # to use fa_table()
tables_CL <- fa_table(pc_CL2)

gt::gtsave(data = tables_CL$ind_table, 
           filename = "CL.html", 
           path = "/home/julian/Documents/tesis-maestria_participacion-politica/illustrations/efa/")
gt::gtsave(data = tables_CL$f_table, 
           filename = "CL_parameters.html" , 
           path = "/home/julian/Documents/tesis-maestria_participacion-politica/illustrations/efa/")

