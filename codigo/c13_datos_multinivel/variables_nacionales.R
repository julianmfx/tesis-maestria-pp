library(dplyr)

# CPI

c("AR", "BO", "BR", 
  "CL", "CO", "CR", 
  "EC", "SV", "GT",
  "HN", "MX", "NI",
  "PA", "PY", "PE",
  "DO", "UY")

cpi_data <- read_csv("bd_var_paises/Jpol/CPI_2011.csv")

head(cpi)

View(cpi)

unique(cpi$country)

countries <- c("Argentina", "Bolivia", "Brazil",
               "Chile", "Colombia", "Costa Rica",
               "Ecuador", "El Salvador", "Guatemala",
               "Honduras", "Mexico", "Nicaragua",
               "Panama", "Paraguay", "Peru",
               "Dominican Republic", "Uruguay")
cpi_data <- cpi_data |> 
  select(country, score) |> 
  filter(country %in% countries)

cpi_data

# GDP growth

gdp_growth_data <- read_csv("bd_var_paises/Jeco/WB_gdp-growth_2011/78de43d5-93a7-4f20-9943-3f905142de3a_Data.csv")
View(gdp_growth_data)
gdp_growth$value
gdp_growth

# GDP growth pc

gdp_growth_pc_data <- read_csv("bd_var_paises/Jeco/WB_gdp-growth-per-capita_2011/0200a021-8aa6-4ec4-977a-294e93c29dc7_Data.csv")
View(gdp_growth_pc_data)
gdp_growth_pc_data$value
gdp_growth_pc_data


# GDP_PC

gdp_data <- read_csv("bd_var_paises/Jeco/WB_gpd-percapita-ppp_2011_2017-USS/626864f4-f81f-4587-9bee-cedbf572801e_Data.csv")

View(gdp_data)
gdp_data
gdp_data$value



# Population

pob <- read_csv("bd_var_paises/Jeco/WB_pop_2011/b81d1465-1906-4968-8b40-14c4bfdfe1a8_Data.csv")
pob$log_pob <- log(pob$value)
View(pob)
pob |>
  select(country, log_pob)
pob$log_pob

# Violence

hom_data <- read_csv("bd_var_paises/Jsoc/WB_hom_2011/3c8b4f26-0df5-4a29-bd2b-ee245869da2a_Data.csv")
View(hom_data)
hom_data |> 
  select(country, value)
hom_data$value
