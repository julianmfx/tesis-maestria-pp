---
title: "c13_multinivel_numerica"
author: "jmf"
date: "`r Sys.Date()`"
output: html_document
---

Exportando base multinivel numérica.

Decidí rehacer la exportación con los puntajes ponderados y calculados a partir de correlaciones policlóricas.
Debido a que los puntajes son fundamentales para el modelo mulitinivel, quiero tener los mejores puntajes para que el modelo multinivel tenga sentido.

Esa decisisión se fundamentó en dos razones:

  1. La sugerencia de Javier de calcular correlaciones policlóricas que cambian mucho los valores de los puntajes;
  2. El documento que leí de LAPOP sobre ponderaciones que afirmar que la variable que debe ser utilizada en comparaciones entre países es la `weight1500` y que ella cambia sustancialmente los residuales de mi modelo. 

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)
```

# Bibliotecas

```{r, message = FALSE}
library(dplyr)
library(haven)
library(corpcor)
library(GPArotation)
library(psych)
library(labelled)
library(gt) # to export PCA table
library(gtsummary) # to use as_gt()
library(corrplot) # to visualize correlation of residuals
library(FactoMineR) # to conduct PCA and FA
library(factoextra) # to conduct PCA and FA
library(purrr) # to use map()
library(tidyverse) # to use drop_na and gather
library(janitor) # to use compare_df_cols()
library(vetr) # to use alike()
library(diffdf) # to use diffdf()
```

# Base de datos base

```{r}
datos <- read.csv("/home/julian/Documents/tesis-maestria_participacion-politica/bd_LAPOP/datos-procesados/efa/lapop12_EFA_Juli.csv")
drop <- c("vb2", "cp21", "cp6")
datos <- datos[ , !(names(datos) %in% drop)]
datos_compl <- na.omit(datos) # listwise deletion
```

```{r}
str(datos_compl)
```

## Agregar variable de ponderación al ambiente de R

```{r}
ponderadores <- readr::read_rds(file = "/home/julian/Documents/tesis-maestria_participacion-politica/bd_LAPOP/datos-procesados/ponderadores.Rds")
```

### Prueba de la base de datos

datos_i <- readr::read_rds(
  "/home/julian/Documents/tesis-maestria_participacion-politica/bd_LAPOP/datos-procesados/individual/datos_individuos.Rds")

datos <- read.csv("bd_LAPOP/datos-procesados/efa/lapop12_EFA_Juli.csv")

colnames(datos)[which(names(datos) == "cp2")] <-  "Ayuda_diputado"
colnames(datos)[which(names(datos) == "cp4a")] <-  "Ayuda_autoridad_local"
colnames(datos)[which(names(datos) == "cp4")] <-  "Ayuda_Estado"
colnames(datos)[which(names(datos) == "np1")] <-  "Asistir_cabildo"
colnames(datos)[which(names(datos) == "np2")] <-  "Ayuda_municipalidad"
colnames(datos)[which(names(datos) == "cp5")] <-  "Problemas_comunitarios"
colnames(datos)[which(names(datos) == "cp7")] <-  "Reuniones_escolares"
colnames(datos)[which(names(datos) == "cp8")] <-  "Reuniones_comunitarias"
colnames(datos)[which(names(datos) == "cp9")] <-  "Reuniones_laborales"
colnames(datos)[which(names(datos) == "cp13")] <-  "Reuniones_partido"
colnames(datos)[which(names(datos) == "prot3")] <-  "Participar_protesta"
colnames(datos)[which(names(datos) == "prot8")] <-  "Compartir_inf_política"
colnames(datos)[which(names(datos) == "vic44")] <-  "Org_comunitaria"
colnames(datos)[which(names(datos) == "pp1")] <-  "Convencer_votar"
colnames(datos)[which(names(datos) == "pp2")] <-  "Trabajar_partido"

nombres <- names(datos)
nombres <- c(nombres, "wt", "weight1500")

modelo <- datos_i[ , (names(datos_i) %in% nombres)]

Comparación

```{r}
table(ponderadores$`modelo$weight1500` == wt_ind$`modelo$weight1500`)
head(ponderadores)
```
```{r}
wt_ind <- unlabelled(wt_ind)
wt_ind <- sapply(wt_ind, as.numeric)
wt_ind <- as.vector(wt_ind)
```

### Solo esta parte es necesaria
```{r}
weight1500 <- ponderadores$`modelo$weight1500`
weight1500 <- sapply(weight1500, as.numeric)
weight1500 <- as.vector(weight1500)
```

```{r}
table(wt_ind == weight1500)
```

```{r}
rm(wt_ind)
rm(datos_i)
rm(ponderadores)
rm(modelo)
rm(nombres)
```


# Modelo componentes principales

```{r}
pc1 <- principal(r = datos_compl, nfactors = 15, rotate = "oblimin", 
                 n.obs = length(datos_compl),
                 covar = F, # Si es F,  encuentra la matriz de correlaciones a partir de los datos base. Cambia sustancialmente si utilizamos covarianza
                 residuals = TRUE, 
                 scores = TRUE,  
                 oblique.scores = TRUE, # component scores based on structure matrix
                 method = "regression", 
                 cor = "poly", # polychoric correlation if using a raw data matrix
                 use = "pairwise", # no es necesario porque ya lo hice, pero me gusta ser explícito
                 weight = weight1500 # ponderaciones
) 
plot(pc1$values, type = "b") # Gráfica de codo para seleccionar num de factores
```

```{r}
pc2 <- principal(r = datos_compl, nfactors = 4, rotate = "oblimin",
                 n.obs = length(datos_compl),
                 covar = F, # Si es F,  encuentra la matriz de correlaciones a partir de los datos base. Cambia sustancialmente si utilizamos covarianza
                 residuals = TRUE, # Cálculo de residuales
                 scores = TRUE,  # Cálculo de los puntajes
                 oblique.scores = TRUE, # component scores based on structure matrix
                 method = "regression",
                 cor = "poly", # polychoric correlation if using a raw data matrixx
                 use = "pairwise", # no es necesario porque ya lo hice, pero me gusta ser explícito
                 weight = weight1500 # ponderaciones
)
print.psych(pc2, cut = 0.3, sort = TRUE)
```

# Puntajes

```{r}
scores_modelo <- as.data.frame(pc2$scores)
head(scores_modelo)
datos_compl$ID <- as.numeric(rownames(datos_compl))
scores_modelo$ID <- as.numeric(rownames(scores_modelo))
table(datos_compl$ID == scores_modelo$ID)
```

## base de datos con variables y puntajes

```{r}
test1 <- left_join(x = datos_compl, 
                   y = scores_modelo,
                   by = "ID")
```

```{r}
str(test1)
```


## Agregando país

Vamos agregar país porque vamos a necesitar de esa variable después y, también, para hacer la prueba del gráfico de puntajes

```{r}
lapop12 <- read_sav("/home/julian/Documents/tesis-maestria_participacion-politica/bd_LAPOP/datos-brutos/regional-merge-files/2012/data-set_AmericasBarometer Merged 2012 Spanish Rev1.5_W.sav") |> 
  as.data.frame()
lapop12 <- lapop12[ , (names(lapop12) %in% c("pais", "weight1500"))]
lapop12$pais <- as_factor(lapop12$pais)
lapop12 <- lapop12 %>%
  group_by(pais) %>% 
  filter(!pais %in% c("Belice", "Canadá", "Guyana", "Haití", "Jamaica", "Surinam",
                      "Trinidad y Tobago", "Estados Unidos", "Venezuela"))
lapop12$ID <- as.numeric(rownames(lapop12))



lapop12[] <- map_if(.x = lapop12, .p = is.factor, .f = droplevels)
map(lapop12, levels)
glimpse(lapop12)
```

```{r}
pais <- lapop12[ , names(lapop12) %in% c("pais", "ID")]
glimpse(lapop12)
```

```{r}
test_join <- left_join(x = test1,
                       y = pais,
                       by= "ID")
glimpse(test_join)
```


```{r}
levels(test_join$pais)
levels(test_join$pais) <- c("MX", "GT", "SV", "HN", "NI", "CR", "PA", "CO", "EC", "BO", "PE", "PY", "CL", "UY", "BR", "AR", "DO")
levels(test_join$pais)
colnames(test_join)[which(names(test_join) == "TC1")] <-  "Comunitaria"
colnames(test_join)[which(names(test_join) == "TC2")] <-  "Voz"
colnames(test_join)[which(names(test_join) == "TC3")] <-  "Partidista"
colnames(test_join)[which(names(test_join) == "TC4")] <-  "Protesta"
glimpse(test_join)
```

### Teste del gráfico

Verificando que la nueva base tenga los puntajes correctos = OK

La comparación se fundamenta en el gráfico que ya tengo sobre puntajes. Hay que ser cualitativamente igual, puesto que los número cambian, pero los patrones nacionales son los mismos.

```{r}
# gráfico
voz_color <- "#0000a7" 
comunitaria_color <- "#008176"
partidista_color <- "#c1272d"
protesta_color <- "#eecc16"

## Total por pais

pais_AL <- test_join |> 
  group_by(pais) |> 
  summarise(Comunitaria = mean(Comunitaria),
            Voz = mean(Voz),
            Partidista = mean(Partidista),
            Protesta = mean(Protesta))
pais_AL

voz <- pais_AL |> 
  ggplot(aes(x = pais, y = Voz)) +
  geom_bar(position = "dodge", stat = "identity", fill = voz_color) +
  theme_minimal()

comunitaria <- pais_AL |> 
  ggplot(aes(x = pais, y = Comunitaria)) +
  geom_bar(position = "dodge", stat = "identity", fill = comunitaria_color) +
  theme_minimal()

partidista <- pais_AL |> 
  ggplot(aes(x = pais, y = Partidista)) +
  geom_bar(position = "dodge", stat = "identity", fill = partidista_color) +
  theme_minimal()

protesta <- pais_AL |> 
  ggplot(aes(x = pais, y = Protesta)) +
  geom_bar(position = "dodge", stat = "identity", fill = protesta_color) +
  theme_minimal()

#### Gráfico

# plot_grid(voz, comunitaria, partidista, protesta)

#### Gráfico para el árbol de decisión

test <- pais_AL %>%
  gather(key = tipo, value = Value, c(Comunitaria, Voz, Partidista, Protesta))

tipos_pais <- test |> 
  ggplot() +
  geom_point(aes(x = factor(pais, 
                            levels = c("DO", "NI", "GT", "UY", "CO", "AR", "MX", "SV", "PY", "EC", "BO", "PE", "HN", "BR", "CL", "CR", "PA")), 
                 y = Value, 
                 color = tipo,
                 shape = tipo,
  ), # cambiar forma 
  size = 3,
  alpha = 0.66,
  stroke = 0.9) +
  scale_color_manual(values = c("Voz" = voz_color, 
                                "Comunitaria" = comunitaria_color,
                                "Partidista" = partidista_color,
                                "Protesta" = protesta_color)) +
  scale_shape_manual(values = c(16, 17, 15, 4)) +
  # scale_y_continuous(breaks = c(-0.5, -0.25, -0.125, 0, 0.125, 0.25, 0.5)) +
  geom_hline(yintercept = 0, linetype = "dashed", color = "black") +
  # geom_hline(yintercept = 0.125, linetype = "dashed", color = "black", alpha = 0.5, linewidth = 0.5) +
  # geom_hline(yintercept = -0.125, linetype = "dashed", color = "black", alpha = 0.5, linewidth = 0.5) +
  xlab("Países") +
  ylab("Puntaje promedio de las personas") + 
  labs(shape = "Tipo de participación política") +
  labs(color = "Tipo de participación política") +
  theme_minimal() +
  theme(panel.grid = element_blank(),
        legend.position = c(0.85, 0.9),
        legend.background = element_rect(fill = "white"))
tipos_pais
```



```{r}
glimpse(test1)
```


# Reuniones deportivas, religiosas y voto

Están en la base del análisis exploratorio
* Deporte: variable cp21
* Religión: variable cp6
* Voto: variable vb2

```{r}
datos_EFA <- read.csv("/home/julian/Documents/tesis-maestria_participacion-politica/bd_LAPOP/datos-procesados/efa/lapop12_EFA_Juli.csv")
datos_EFA$ID <- as.numeric(rownames(datos_EFA))
datos_EFA <- datos_EFA[ , names(datos_EFA) %in% c("cp21", "cp6", "vb2", "ID")]
glimpse(datos_EFA)
```


### left_join()

```{r}
test2 <- left_join(x = test_join, 
                        y = datos_EFA,
                        by = "ID")
```

```{r}
glimpse(test2)
```

### Verificación
```{r}
datos_EFA$ID[1:31]
datos_EFA$cp6[1:31]
test2$ID[1:31]
test2$cp6[1:31]
```

```{r}
tail(test2$ID)
tail(test2$cp6)
tail(datos_EFA$ID)
tail(datos_EFA$cp6)
```

```{r}
datos_EFA$ID[1:31]
datos_EFA$cp21[1:31]
test2$ID[1:31]
test2$cp21[1:31]
```

```{r}
tail(test2$ID)
tail(test2$cp21)
tail(datos_EFA$ID)
tail(datos_EFA$cp21)
```


```{r}
datos_EFA$ID[1:31]
datos_EFA$vb2[1:31]
test2$ID[1:31]
test2$vb2[1:31]
```

```{r}
tail(test2$ID)
tail(test2$vb2)
tail(datos_EFA$ID)
tail(datos_EFA$vb2)
```


```{r}
names(test2)
```


```{r}
test2 <- test2 |> 
  relocate(-Voz, -Comunitaria, -Partidista, -Protesta, -ID)
```

```{r}
str(test2)
```


# Base con todas las variables

* lapop12 ya estaba cargada en el script
* Iremos subscribir la base que cargamos antes para **agregar países** y, ahora, vamos a juntar otras variables de estudio

```{r}
lapop12 <- haven::read_sav(
  "/home/julian/Documents/tesis-maestria_participacion-politica/bd_LAPOP/datos-brutos/regional-merge-files/2012/data-set_AmericasBarometer Merged 2012 Spanish Rev1.5_W.sav") |> 
  as.data.frame()
```

```{r}
glimpse(lapop12)
```
## Variables de interés

Variables socioeconómicas
  * Género: q1
  * Edad: q2
  * Ingreso: q10new
  * Religión: q3c
    * La variable de religiosidad es: q3c. Otras interesantes son: q5a, q5b
  * Urbano/Rural: ur
  * Tamaño: tamano
  * Recibe ayuda: cct1new
  * Simpatizar partido: vb10
  * Interes_política: pol1
  * Educación: ed
  * Etnia: etid
  * Uso internet: www1
  * Seguir noticias: gi0


Las variables de confianza institucional son (hay otras):
   * Elecciones: b47a
   * Instituciones (tener respeto): b2
   * Justicia: b10a
   * Congreso: b13
   * Partidos: b21
   * Presidente: b21a
   * Municipalidad: b32
  
Las variables de corrupción son (hay otras):
   * Combate: n9
   * Funcionarios públicos: exc7

Seleccionamos las variables de interés

```{r}
keep <- c("pais", "q1", "q2", "q10new", "q3c", "ur", "tamano", "cct1new", "vb10", "pol1", "ed", "etid", "www1", "gi0",
          "q5a", "q5b", 
          "b47a", "b2", "b10a", "b13", "b21", "b21a", "b32", 
          "b1", "b3", "b4", "b6", "b11", "b18", "b20", "b20a", "b31", "b43", "b37",
          "n9", "exc7", 
          "weight1500", "wt")
```


Filtramos el conjunto de datos por esas variables

```{r}
lapop12 <- lapop12[ , names(lapop12) %in% keep]
```

```{r}
glimpse(lapop12)
```


Todas las variables que tienen 999999, 988888 y 888888 son valores faltantes

```{r}
sapply(lapop12, unique)
```

```{r}
table(lapop12 == 999999)
table(lapop12 == 988888)
table(lapop12 == 888888)
```

Transformamos el país en factor

```{r}
lapop12$pais <- as_factor(lapop12$pais)
```

```{r}
glimpse(lapop12)
```

Filtramos los países
```{r}
lapop12 <- lapop12 %>%
  group_by(pais) %>% 
  filter(!pais %in% c("Belice", "Canadá", "Guyana", "Haití", "Jamaica", "Surinam",
                      "Trinidad y Tobago", "Estados Unidos", "Venezuela"))
```

La base tiene 27.756 de líneas, el mismo número de la base utilizada para los análisis.
```{r}
glimpse(lapop12)
```

Generamos el número identificador

```{r}
lapop12$ID <- as.numeric(rownames(lapop12))
```

```{r}
glimpse(lapop12)
```

Tenemos que sacar los niveles no utilizados
```{r}
map(lapop12, levels)
```

```{r}
lapop12[] <- map_if(lapop12, is.factor, droplevels)
```

```{r}
map(lapop12, levels)
```

## left_join()

left_join() mantiene solamente los elementos que están en los dos dataframes

```{r}
glimpse(test2)
```


```{r}
glimpse(lapop12)
```

La columna país está presente en los dos objetos.
Iremos sacar de lapop12 porque nuestro conjunto base es test2

```{r}
lapop12 <- lapop12[ , !names(lapop12) %in% "pais"]
glimpse(lapop12)
```


```{r}
individual <- left_join(x = test2, 
                    y = lapop12,
                    by = "ID")
```

```{r}
glimpse(individual)
```


#### Verificación

```{r}
lapop12$ID[1:20]
lapop12$b21[1:20]
individual$ID[1:20]
individual$b21[1:20]
```

```{r}
tail(lapop12$ID)
tail(lapop12$b21)
tail(individual$ID)
tail(individual$b21)
```

```{r}
names(individual)
```

```{r}
glimpse(individual)
```

## Limpieza: variables y sus NAs

Yo estaba haciendo la limpieza después, pero es mejor hacerlo ahora porque es solamente en esta base que están los valores valores de 999999, 988888 y 888888


```{r}
glimpse(individual)
```

Verificando si hay NAs


```{r}
table(is.na(individual))
```

Los NAs están solamente en las variables que estaban dicotimizadas y que agregué después: cp6, cp21 y vb2.

```{r}
new_DF <- individual[, colSums(is.na(individual)) > 0]
new_DF
```

Esta cria un dataframe con cada línea que tiene NA. Está interesante porque no conocía este código, pero no me sirve tanto como conocer solamente las columnas.

```{r}
new_DF <- individual[rowSums(is.na(individual)) > 0, ]
new_DF
```


### Valores faltantes

Como estoy utilizando las variables como numéricas, las codificaciones de NAs de LAPOP para esas variables eran 999999, 988888 y 888888. Voy a cambiarlas manualmente por NAs.

Así se verifican los NAs. No ejecutar.

```{r}
# sapply(multinivel, unique) # no ejecutar, pero así se ven los valores únicos
```

Son 22188 NAs.
Pero ahora la función is.na solo afirma que hay 306

```{r}
table(individual == 999999)
table(individual == 988888)
table(individual == 888888)
141+6552+15495
```

```{r}
individual[individual == 999999] <- NA
individual[individual == 988888] <- NA
individual[individual == 888888] <- NA
```

La transformación fue existosa: ahora son 22494 NAs!!!!!

```{r}
table(is.na(individual))
22188 + 306
```


```{r}
glimpse(individual)
```

#### Comparación con la base factorizada: ya no es necesario

Como antes había hecho ese código para la base con todos los valores, tuve que hacer toda esa verificación para estar seguro que yo no hubiera cambiado valores en otras variables.

Hay más datos faltantes que la base factorizada porque allá no factorizé todas las variables, entonces todavía no están todos los NAs

```{r}
sum(is.na(multinivel))
sum(is.na(datos))
```


```{r}
table(multinivel == 999999)
table(multinivel == 988888)
table(multinivel == 888888)
```

```{r}
table(is.na(multinivel$wt))
table(is.na(multinivel$weight1500))
table(is.na(multinivel$Voz))
table(is.na(multinivel$Comunitaria))
table(is.na(multinivel$Partidista))
table(is.na(multinivel$Protesta))
```

Tampoco hay valores faltantes en las variables nacionales.

```{r}
fun <- function(x) {
  sum(is.na(x))
}
```

Identificando dónde hay más NAs
```{r}
res <- sapply(multinivel, fun)
res <- as.data.frame(res)
arrange(res, -res)
```


```{r}
var_nacionales <- c("gdp_growth", "gdp_growth_pc", "gdp_pc_ppp", "log_pob", "ele_nac", "ele_reg", "ele_nac_2011", "ele_reg_2011", "federalista", "cpi", "tas_hom", "indg_pob_2010")
```

```{r}
subset <- lapop12[ , names(lapop12) %in% var_nacionales]
```

```{r}
sum(is.na(subset))
```


```{r}
glimpse(multinivel)
```




# Base de datos nacionales

```{r}
nacional <- readr::read_rds(file = "/home/julian/Documents/tesis-maestria_participacion-politica/bd_var_paises/J_var_nacionales.Rds")
```


```{r}
glimpse(nacional)
```

## Limpieza

```{r}
nacional$pais <- as.factor(nacional$pais)
```

```{r}
levels(individual$pais)
levels(nacional$pais)
```

## left_join

```{r}
glimpse(individual)
```

```{r}
glimpse(nacional)
```


```{r}
61 + 13
multinivel <- left_join(x = individual,
                        y = nacional,
                        by = "pais")
```

```{r}
glimpse(multinivel)
```


# Nombramiento

Verificar código `export_I_ind.Rmd`.

```{r}
colnames(multinivel)
```

```{r}
colnames(multinivel)[which(names(multinivel) == "ur")] <-  "Urbano_Rural"
colnames(multinivel)[which(names(multinivel) == "tamano")] <-  "Tamano"
colnames(multinivel)[which(names(multinivel) == "cct1new")] <-  "Recibe_ayuda"
colnames(multinivel)[which(names(multinivel) == "vb2")] <-  "Votar"
colnames(multinivel)[which(names(multinivel) == "vb10")] <-  "Simpatizar_partido"
colnames(multinivel)[which(names(multinivel) == "pol1")] <-  "Interes_política"
colnames(multinivel)[which(names(multinivel) == "ed")] <-  "Educacion"
colnames(multinivel)[which(names(multinivel) == "etid")] <-  "Etnia"
colnames(multinivel)[which(names(multinivel) == "www1")] <-  "Uso_internet"
colnames(multinivel)[which(names(multinivel) == "gi0")] <-  "Seguir_noticias"
colnames(multinivel)[which(names(multinivel) == "cp2")] <-  "Ayuda_diputado"
colnames(multinivel)[which(names(multinivel) == "cp4a")] <-  "Ayuda_autoridad_local" # cambié
colnames(multinivel)[which(names(multinivel) == "cp4")] <-  "Ayuda_Estado"
colnames(multinivel)[which(names(multinivel) == "np1")] <-  "Asistir_cabildo"
colnames(multinivel)[which(names(multinivel) == "np2")] <-  "Ayuda_municipalidad"
colnames(multinivel)[which(names(multinivel) == "cp5")] <-  "Problemas_comunitarios"

colnames(multinivel)[which(names(multinivel) == "cp6")] <-  "Reuniones_religiosas"
colnames(multinivel)[which(names(multinivel) == "cp21")] <-  "Reuniones_deportivas"

colnames(multinivel)[which(names(multinivel) == "cp7")] <-  "Reuniones_escolares"
colnames(multinivel)[which(names(multinivel) == "cp8")] <-  "Reuniones_comunitarias"
colnames(multinivel)[which(names(multinivel) == "cp9")] <-  "Reuniones_laborales"
colnames(multinivel)[which(names(multinivel) == "cp13")] <-  "Reuniones_partido"
colnames(multinivel)[which(names(multinivel) == "prot3")] <-  "Participar_protesta"
colnames(multinivel)[which(names(multinivel) == "prot8")] <-  "Compartir_inf_política"
colnames(multinivel)[which(names(multinivel) == "vic44")] <-  "Org_comunitaria"
colnames(multinivel)[which(names(multinivel) == "pp1")] <-  "Convencer_votar"
colnames(multinivel)[which(names(multinivel) == "pp2")] <-  "Trabajar_partido"
colnames(multinivel)[which(names(multinivel) == "q1")] <-  "Sexo"
colnames(multinivel)[which(names(multinivel) == "q2")] <-  "Edad"

colnames(multinivel)[which(names(multinivel) == "q10new")] <-  "Ingreso"

# colnames(multinivel)[which(names(multinivel) == "TC1")] <-  "Comunitaria"
# colnames(multinivel)[which(names(multinivel) == "TC2")] <-  "Voz"
# colnames(multinivel)[which(names(multinivel) == "TC3")] <-  "Partidista"
# colnames(multinivel)[which(names(multinivel) == "TC4")] <-  "Protesta"

colnames(multinivel)[which(names(multinivel) == "q3c")] <-  "Religion"
colnames(multinivel)[which(names(multinivel) == "b47a")] <-  "C_Elecciones"
colnames(multinivel)[which(names(multinivel) == "b2")] <-  "R_Instituciones"
colnames(multinivel)[which(names(multinivel) == "b10a")] <-  "C_Justicia"
colnames(multinivel)[which(names(multinivel) == "b13")] <-  "C_Congreso"
colnames(multinivel)[which(names(multinivel) == "b21")] <-  "C_Partidos"
colnames(multinivel)[which(names(multinivel) == "b21a")] <-  "C_Presidente"
colnames(multinivel)[which(names(multinivel) == "b32")] <-  "C_Municipalidad"
colnames(multinivel)[which(names(multinivel) == "exc7")] <-  "Corrupción"
```

```{r}
colnames(multinivel)
```

## teste para reproducir el gráfico de puntajes = OK

```{r}
voz_color <- "#0000a7" 
comunitaria_color <- "#008176"
partidista_color <- "#c1272d"
protesta_color <- "#eecc16"

pais_AL <- multinivel |> 
  group_by(pais) |> 
  summarise(Comunitaria = mean(Comunitaria),
            Voz = mean(Voz),
            Partidista = mean(Partidista),
            Protesta = mean(Protesta))
pais_AL

voz <- pais_AL |> 
  ggplot(aes(x = pais, y = Voz)) +
  geom_bar(position = "dodge", stat = "identity", fill = voz_color) +
  theme_minimal()

comunitaria <- pais_AL |> 
  ggplot(aes(x = pais, y = Comunitaria)) +
  geom_bar(position = "dodge", stat = "identity", fill = comunitaria_color) +
  theme_minimal()

partidista <- pais_AL |> 
  ggplot(aes(x = pais, y = Partidista)) +
  geom_bar(position = "dodge", stat = "identity", fill = partidista_color) +
  theme_minimal()

protesta <- pais_AL |> 
  ggplot(aes(x = pais, y = Protesta)) +
  geom_bar(position = "dodge", stat = "identity", fill = protesta_color) +
  theme_minimal()

test <- pais_AL %>%
  gather(key = tipo, value = Value, c(Comunitaria, Voz, Partidista, Protesta))
test

tipos_pais <- test |> 
  ggplot() +
  geom_point(aes(x = factor(pais, 
                            levels = c("DO", "NI", "GT", "UY", "CO", "AR", "MX", "SV", "PY", "EC", "BO", "PE", "HN", "BR", "CL", "CR", "PA")), 
                 y = Value, 
                 color = tipo,
                 shape = tipo,
  ), # cambiar forma 
  size = 3,
  alpha = 0.66,
  stroke = 0.9) +
  scale_color_manual(values = c("Voz" = voz_color, 
                                "Comunitaria" = comunitaria_color,
                                "Partidista" = partidista_color,
                                "Protesta" = protesta_color)) +
  scale_shape_manual(values = c(16, 17, 15, 4)) +
  # scale_y_continuous(breaks = c(-0.5, -0.25, -0.125, 0, 0.125, 0.25, 0.5)) +
  geom_hline(yintercept = 0, linetype = "dashed", color = "black") +
  # geom_hline(yintercept = 0.125, linetype = "dashed", color = "black", alpha = 0.5, linewidth = 0.5) +
  # geom_hline(yintercept = -0.125, linetype = "dashed", color = "black", alpha = 0.5, linewidth = 0.5) +
  xlab("Países") +
  ylab("Puntaje promedio de las personas") + 
  labs(shape = "Tipo de participación política") +
  labs(color = "Tipo de participación política") +
  theme_minimal() +
  theme(panel.grid = element_blank(),
        legend.position = c(0.85, 0.9),
        legend.background = element_rect(fill = "white"))
tipos_pais
```



# Exportación

```{r}
readr::write_rds(multinivel, file = "/home/julian/Documents/tesis-maestria_participacion-politica/bd_LAPOP/datos-procesados/multinivel_final.Rds")
```


Se carga la base de datos exportada como um objeto de teste para verificar que no hubo problemas de exportación.

```{r}
multinivel_test <- readr::read_rds(
  "/home/julian/Documents/tesis-maestria_participacion-politica/bd_LAPOP/datos-procesados/multinivel_final.Rds")
```


Las pruebas de exportación permiten concluir que no hubo error de exportación.
```{r}
all.equal(multinivel, multinivel_test)
compare_df_cols(multinivel, multinivel_test, return = "mismatch", bind_method = "rbind")
alike(multinivel, multinivel_test)
diffdf(multinivel, multinivel_test)
```
