
# Religiosidad
str(lapop12$q3c)
table(lapop12$q3c)
unique(lapop12$q3c)
table(paste0(lapop12$q3c))
Son 553 + 615 sin codificación
levels(lapop12$q3c) <- list("Católico" = "Católico",
                           "Protestante" = "Protestante, Protestante Tradicional o Protestante no Evangé",
                           "Ning/Ateo/Agn" = c("Ninguna", "Agnóstico o ateo"),
                           "Evan/Pent" = "Evangélica y Pentecostal",
                           "Otro" = c("Religiones Orientales no Cristianas", 
                                      "Iglesia de los Santos de los Últimos Días",
                                      "Religiones Tradicionales", 
                                      "Judío", 
                                      "Testigos de Jehová", 
                                      "Otro"),
                           NA)

# Elecciones
str(lapop12$b47a)
table(lapop12$b47a)
table(paste0(lapop12$b47a))
unique(lapop12$b47a)
Son 722+289+3001 de no respuestas
levels(lapop12$b47a) <- list("Nada/Poca" = c("Nada de confianza", "2", "3"),
                            "Mediana" = "4",
                            "Alta/Mucha" = c("5", "6", "Mucha confianza"),
                            NA)




str(lapop12$b2)
table(lapop12$b2)
table(paste0(lapop12$b2))
unique(lapop12$b2)
Son 865+333 de no respuestas
levels(lapop12$b2) <- list("Nada/Poco" = c("Nada de respeto", "2", "3"),
                             "Mediano" = "4",
                             "Alto/Mucho" = c("5", "6", "Mucho respeto"),
                             NA)



# Congreso
str(lapop12$b13)
table(lapop12$b13)
table(paste0(lapop12$b13))
unique(lapop12$b13)
Son 1435+308 de no respuestas
levels(lapop12$b13) <- list("Nada/Poca" = c("Nada de confianza", "2", "3"),
                             "Mediana" = "4",
                             "Alta/Mucha" = c("5", "6", "Mucha confianza"),
                             NA)
# Partidos
str(lapop12$b21)
table(lapop12$b21)
table(paste0(lapop12$b21))
unique(lapop12$b21)
Son 707+297 de no respuestas
levels(lapop12$b21) <- list("Nada/Poca" = c("Nada de confianza", "2", "3"),
                            "Mediana" = "4",
                            "Alta/Mucha" = c("5", "6", "Mucha confianza"),
                            NA)

# Presidente 
str(lapop12$b21a)
table(lapop12$b21a)
table(paste0(lapop12$b21a))
unique(lapop12$b21a)
Son 572+310 de no respuestas
levels(lapop12$b21a) <- list("Nada/Poca" = c("Nada de confianza", "2", "3"),
                            "Mediana" = "4",
                            "Alta/Mucha" = c("5", "6", "Mucha confianza"),
                            NA)

# Municipalidad
str(lapop12$b32)
table(lapop12$b32)
table(paste0(lapop12$b32))
unique(lapop12$b32)
Son 859 + 236 de no respuestas
levels(lapop12$b32) <- list("Nada/Poca" = c("Nada de confianza", "2", "3"),
                             "Mediana" = "4",
                             "Alta/Mucha" = c("5", "6", "Mucha confianza"),
                             NA)

# Corrupción
str(lapop12$exc7)
table(lapop12$exc7)
table(paste0(lapop12$exc7))
unique(lapop12$exc7)
Son 2093+364 de no respuestas
levels(lapop12$exc7) <- list("Muy generalizada" = "Muy generalizada",
                             "Algo generalizada" = "Algo generalizada",
                             "Poco generalizada" = "Poco generalizada",
                             "Nada generalizada" = "Nada generalizada",
                             NA)

                             