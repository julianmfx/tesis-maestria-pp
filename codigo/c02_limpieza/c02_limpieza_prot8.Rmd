---
title: "limpieza_variable_prot8"
author: "jmf"
date: "`r Sys.Date()`"
output: 
  html_document:
    toc: TRUE
    toc_float: TRUE
    theme: cosmo
    highlight: zenburn
---

<!-- setwd("/home/julian/Documents/tesis-maestria_participacion-politica/codigo/c02_limpieza") -->

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE, comment = NA)
```

# Introducción

Este código tiene dos objetivos principales: el primero es verificar, limpiar y guardar todas las variables que serán utilizadas en la tesis y el segundo es reproducir el análisis estadístico del [reporte 02](file:///home/julian/Documents/tesis-maestria_participacion-politica/entregas/reporte02_04-09_pre-analisis_np1_pais/reporte02_04-09_pre-analisis_np1_pais.html).
Luego de una revisón manual de las variables, fueron añadidas algunas variables más allá de aquellas en el reporte original. Ellas son:

* q1 corresponde a sexo
* vic44: "Por temor a la delincuencia, ¿se ha organizado con los vecinos de la comunidad?"
* q2 corresponde a edad

| Variables 2012 |
|----------------|
| np1            |
| cp6            |
| cp7            |
| cp8            |
| cp13           |
| cp20           |
| prot3          |
| vb2            |
| vb20           |
| cp4a           |
| np2            |
| cp5            |
| cp2            |
| cp4            |
| cp9            |
| cp21           |
| prot7          |
| prot6          |
| prot8          |
| pp1            |
| pp2            |
| pais           |
| q1             |
| vic44          |
| q2             |


## Librerías utilizadas

```{r librerias, results = "hide", message=FALSE, warning=FALSE}

library(dplyr) # to use pipe operators
library(haven) # to import database
library(tidyverse) # to use ggplot

```

## Importación de la base de datos

```{r import}
lapop12_pais <- readr::read_rds(
  "/home/julian/Documents/tesis-maestria_participacion-politica/bd_LAPOP/datos-procesados/lapop12_pais.Rds")

```

## Variable prot8
La variable tiene el fraseo específico: "En los últimos doce meses, usted leyó o compartió información política por alguna red social de la web como Twitter, Facebook u Orkut?"


```{r}
str(lapop12_pais$prot8)
class(lapop12_pais$prot8)
unique(lapop12_pais$prot8)
table(lapop12_pais$prot8)
```
Transformación y verificación
```{r}
lapop12_pais$prot8 <- as_factor(lapop12_pais$prot8)
str(lapop12_pais$prot8)
class(lapop12_pais$prot8)
unique(lapop12_pais$prot8)
table(lapop12_pais$prot8)
```
Colapsando "NS", "NR" y "N/A"
```{r}
levels(lapop12_pais$prot8)
levels(lapop12_pais$prot8) <- list("Sí" = "Sí, ha hecho", 
                            "No" = "No ha hecho",
                            "NA" = c("NS", "NR", "N/A"))
levels(lapop12_pais$prot8)
```
Verificando proceso de colapsar los niveles.
```{r}
str(lapop12_pais$prot8)
class(lapop12_pais$prot8)
unique(lapop12_pais$prot8)
table(lapop12_pais$prot8)
285+105
2951+24415+285+105
```

# Análisis por frecuencia y porcentaje

```{r base-generator}

prot8_total <- lapop12_pais %>%
  dplyr::group_by(prot8) %>%
  dplyr::summarise(Frequency = n()) %>%
  dplyr::mutate(Percent = round(Frequency/sum(Frequency)*100, 1))
  prot8_total

prot8_pais <- lapop12_pais %>%
  dplyr::group_by(pais, prot8) %>%
  dplyr::summarise(Frequency = n(), .groups="drop_last") %>%
  dplyr::mutate(Percent = round(Frequency/sum(Frequency)*100, 1))
  prot8_pais

prot8_pais_transform <- lapop12_pais %>%
  dplyr::group_by(pais, prot8) %>%
  dplyr::summarise(Frequency = n(), .groups="drop_last") %>%
  dplyr::mutate(Percent = round(Frequency/sum(Frequency)*100, 1)) %>% 
  pivot_wider(id_cols = pais,
              names_from = prot8,
              values_from = c(Frequency, Percent))
  # Exportar la tabla que seguirá adjunta a este reporte
  # write.table(prot8_pais_transform, file = "prot8_pais_transform.csv", sep = ",", quote = FALSE, row.names = F)
  prot8_pais_transform


prot8_MX <- lapop12_pais %>%
  dplyr::filter(pais == "México") %>% 
  dplyr::group_by(prot8) %>%
  dplyr::summarise(Frequency = n()) %>%
  dplyr::mutate(Percent = round(Frequency/sum(Frequency)*100, 1))
  prot8_MX
```
# Visualización

## Histogramas

Con el caso méxico, se generó un histograma.

```{r hist-mx}
prot8_pais %>%
  filter(pais == "México") %>% 
  ggplot(aes(prot8, Percent, fill = prot8)) + 
  # el segundo argumento identifica el eje Y como porcentaje
  geom_bar(stat="identity",  position = position_dodge()) +
  geom_text(aes(y = Percent, label = Percent), nudge_y = 2, color = "black",
             size=3) +
  theme_bw() +
  theme(legend.position="none")

```

## Probando grids and wraps

Para los gráficos finales, fue utilizado facet_wrap() de ggplot.

### Frecuencia por país

```{r}
prot8_frecuencia_por_pais <- prot8_pais %>%
  ggplot(aes(prot8, Percent,
             fill = prot8,
             color = prot8,
             group=1,
             )) +
  facet_wrap(~pais, 
             ncol = 6,
             ) +
  scale_y_continuous(limits = c(0, NA)) +
  geom_bar(stat="identity", position=position_dodge()) +
  labs(x = NULL, 
       y = "Número de respuestas por país",
       title = "Variable prot8: En los últimos doce meses, usted leyó o compartió información política por alguna red social \nde la web como Twitter, Facebook u Orkut?") +
  geom_text(aes(label=Frequency), nudge_y = 8, color = "black",
             size = 2.5) +
  scale_color_brewer(palette="Pastel1") + 
  scale_fill_brewer(palette="Pastel1") +
  theme(
    strip.placement = "outside",
    strip.text.x = element_text(hjust = 0.5, vjust = 0.5),
    strip.background = element_blank(),
    panel.background = element_blank(),
    panel.grid = element_blank(),
    axis.text.x = element_blank(),
    axis.text.x.bottom = element_blank(),
    axis.text.x.top = element_blank(),
    axis.title.x.bottom = element_blank(),
    axis.ticks.x = element_blank(),
    axis.text.y = element_blank(), 
    axis.ticks.y = element_blank(),
    legend.title = element_blank(),
    legend.text = element_text(size = 8),
    plot.title = element_text(size = 10)
    )
  prot8_frecuencia_por_pais
  ggsave("prot8_frecuencia_por_pais.pdf", width = 20, height = 15, units = "cm", dpi = 300)
```

### Porcentaje por país

Los nombres de los países eran demasiados largos para el gráfico y, por lo tanto, se decidió cambiarlos por sus siglas con códigos de 2 letras.

```{r}
levels(prot8_pais$pais)
levels(prot8_pais$pais) <- c("MX", "GT", "SV", "HN", "NI", "CR", "PA", "CO", "EC",
                           "BO", "PE", "PY", "CL", "UY", "BR", "AR", "DO")
levels(prot8_pais$pais)
```

Fue utilizado facet_wrap() de ggplot para construir el gráfico.

```{r}
prot8_facet_wrap <- prot8_pais %>%
  ggplot(aes(prot8, Percent,
             fill = prot8,
             color = prot8,
             group=1,
  )) +
  facet_wrap(~pais,
             ncol = 17,
  ) +
  scale_y_continuous(limits = c(0, NA)) +
  geom_bar(stat="identity", position=position_dodge()) +
  labs(x = NULL,
       y = "Porcentaje de las respuestas (%)",
       title = "Variable prot8: En los últimos doce meses, usted leyó o compartió información política por alguna red social \nde la web como Twitter, Facebook u Orkut?") +
  geom_hline(aes(yintercept = 10.6, linetype = "10.6%"), size = 0.2, color = "red") +
  geom_hline(aes(yintercept = 88, linetype = "88%"), size = 0.2, color = "blue") +
  scale_linetype_manual(name = "Limits",
                        breaks = c("10.6%", "88%"),
                        values = c(2, 2),
                        guide = guide_legend(reverse = FALSE,
                                             override.aes = list(
                                               color = c("red", "blue")))) +
  scale_color_brewer(palette="Pastel1") + 
  scale_fill_brewer(palette="Pastel1") +
  theme(
    strip.placement = "outside",
    strip.text.x = element_text(hjust = 0.5, vjust = 0.5),
    strip.background = element_blank(),
    panel.background = element_blank(),
    panel.grid = element_blank(),
    axis.text.x = element_blank(),
    axis.text.x.bottom = element_blank(),
    axis.text.x.top = element_blank(),
    axis.title.x.bottom = element_blank(),
    axis.ticks.x = element_blank(),
    axis.ticks.y = element_blank(),
    legend.title = element_blank(),
    legend.text = element_text(size = 8),
    plot.title = element_text(size = 10)
  )
prot8_facet_wrap
ggsave("prot8_facet_wrap.pdf", width = 20, height = 15, units = "cm", dpi = 300)

```
