---
title: "limpieza_variable_q2"
author: "jmf"
date: "`r Sys.Date()`"
output: 
  html_document:
    toc: TRUE
    toc_float: TRUE
    theme: cosmo
    highlight: zenburn
---

<!-- setwd("/home/julian/Documents/tesis-maestria_participacion-politica/codigo/c02_limpieza") -->

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE, comment = NA)
```

# Introducción

Este código tiene dos objetivos principales: el primero es verificar, limpiar y guardar todas las variables que serán utilizadas en la tesis y el segundo es reproducir el análisis estadístico del [reporte 02](file:///home/julian/Documents/tesis-maestria_participacion-politica/entregas/reporte02_04-09_pre-analisis_np1_pais/reporte02_04-09_pre-analisis_np1_pais.html).
Luego de una revisón manual de las variables, fueron añadidas algunas variables más allá de aquellas en el reporte original. Ellas son:

* q1 corresponde a sexo
* vic44: "Por temor a la delincuencia, ¿se ha organizado con los vecinos de la comunidad?"
* q2 corresponde a edad

| Variables 2012 |
|----------------|
| np1            |
| cp6            |
| cp7            |
| cp8            |
| cp13           |
| cp20           |
| prot3          |
| vb2            |
| vb20           |
| cp4a           |
| np2            |
| cp5            |
| cp2            |
| cp4            |
| cp9            |
| cp21           |
| prot7          |
| prot6          |
| prot8          |
| pp1            |
| pp2            |
| pais           |
| q2             |
| vic44          |
| q2             |


## Librerías utilizadas

```{r librerias, results = "hide", message=FALSE, warning=FALSE}

library(dplyr) # to use pipe operators
library(haven) # to import database
library(tidyverse) # to use ggplot

```

## Importación de la base de datos

```{r import}
lapop12_pais <- readr::read_rds(
  "/home/julian/Documents/tesis-maestria_participacion-politica/bd_LAPOP/datos-procesados/lapop12_pais.Rds")

```

## Variable q2
La variable busca identificar la edad.

```{r}
str(lapop12_pais$q2)
class(lapop12_pais$q2)
unique(lapop12_pais$q2)
table(lapop12_pais$q2)
```
Transformación y verificación para numérica
```{r}
lapop12_pais$q2 <- as.numeric(lapop12_pais$q2)
str(lapop12_pais$q2)
class(lapop12_pais$q2)
unique(lapop12_pais$q2)
table(lapop12_pais$q2)
```


## Variable q1
La variable busca identificar el sexo de la persona entre hombre y mujer.

```{r}
str(lapop12_pais$q1)
class(lapop12_pais$q1)
unique(lapop12_pais$q1)
table(lapop12_pais$q1)
```
Transformación y verificación para factor
```{r}
lapop12_pais$q1 <- as_factor(lapop12_pais$q1)
str(lapop12_pais$q1)
class(lapop12_pais$q1)
unique(lapop12_pais$q1)
table(lapop12_pais$q1)
```
Colapsando "NS", "NR" y "N/A"
```{r}
levels(lapop12_pais$q1)
levels(lapop12_pais$q1) <- list("Hombre" = "Hombre", 
                                 "Mujer" = "Mujer",
                                 "NA" = c("NS", "NR", "N/A"))
levels(lapop12_pais$q1)
```
Verificando proceso de colapsar los niveles.
```{r}
str(lapop12_pais$q1)
class(lapop12_pais$q1)
unique(lapop12_pais$q1)
table(lapop12_pais$q1)
13633+14122+1
```





# Análisis por frecuencia y porcentaje


```{r base-generator}

q2_total <- lapop12_pais %>%
  dplyr::group_by(q2) %>%
  dplyr::summarise(Frequency = n()) %>%
  dplyr::mutate(Percent = round(Frequency/sum(Frequency)*100, 1))
  q2_total

q2_pais <- lapop12_pais %>%
  dplyr::group_by(pais, q2) %>%
  dplyr::summarise(Frequency = n(), .groups="drop_last") %>%
  dplyr::mutate(Percent = round(Frequency/sum(Frequency)*100, 1))
  q2_pais

q2_pais_transform <- lapop12_pais %>%
  dplyr::group_by(pais, q2) %>%
  dplyr::summarise(Frequency = n(), .groups="drop_last") %>%
  dplyr::mutate(Percent = round(Frequency/sum(Frequency)*100, 1)) %>% 
  pivot_wider(id_cols = pais,
              names_from = q2,
              values_from = c(Frequency, Percent))
  # Exportar la tabla que seguirá adjunta a este reporte
  # write.table(q2_pais_transform, file = "q2_pais_transform.csv", sep = ",", quote = FALSE, row.names = F)
  q2_pais_transform


q2_MX <- lapop12_pais %>%
  dplyr::filter(pais == "México") %>% 
  dplyr::group_by(q2) %>%
  dplyr::summarise(Frequency = n()) %>%
  dplyr::mutate(Percent = round(Frequency/sum(Frequency)*100, 1))
  q2_MX
```

# Visualización

## Histogramas

Con el caso méxico, se generó un histograma.

```{r hist-mx}
q2_pais %>%
  filter(pais == "México") %>% 
  ggplot(aes(q2, Percent, fill = q2)) + 
  # el segundo argumento identifica el eje Y como porcentaje
  geom_bar(stat="identity",  position = position_dodge()) +
  geom_text(aes(y = Percent, label = Percent), nudge_y = 2, color = "black",
             size=3) +
  theme_bw() +
  theme(legend.position="none")

```

## Probando grids and wraps

Para los gráficos finales, fue utilizado facet_wrap() de ggplot.

### Frecuencia por país
La variable de edad es secuenciada en intervalos de 6.
```{r}

q2_by6 <- cut(lapop12_pais$q2, breaks = seq(16, 94, 6), include.lowest = TRUE)

#a <- as.numeric(q2_by6)

q2_by6_dist <- lapop12_pais %>% 
  ggplot(aes(x = q2_by6, 
             fill = q1,
             )) +
  scale_fill_manual(values = c("#999999", "#F0E442", "#D55E00")) +
  geom_bar(position = position_dodge()) +
  ylab("Número de personas") +
  ggtitle("Distribución de grupos de edad separados por sexo en la encuesta de LAPOP de 2012") + 
  theme(
    strip.placement = "outside",
    strip.text.x = element_text(hjust = 0.5, vjust = 0.5),
    strip.background = element_blank(),
    panel.background = element_blank(),
    panel.grid = element_blank(),
    axis.text.x = element_text(color = "black", size = 8),
    axis.ticks.x = element_blank(),
    # axis.text.y = element_blank(),
    # axis.ticks.y = element_blank(),
    legend.title = element_blank(),
    axis.title.x.bottom = element_blank(),
    legend.text = element_text(size = 8),
    plot.title = element_text(size = 12)
    )
q2_by6_dist
ggsave("q2_by6_dist.pdf", width = 20, height = 15, units = "cm", dpi = 300)
```
# No creo de aquí para abajo usaré el código, pero por ahora lo mantendré aquí

```{r}
q2_frecuencia_por_pais <- q2_pais %>%
  ggplot(aes(q2, Percent,
             fill = q2,
             color = q2,
             group=1,
             )) +
  facet_wrap(~pais, 
             ncol = 6,
             ) +
  scale_y_continuous(limits = c(0, NA)) +
  geom_bar(stat="identity", position=position_dodge()) +
  labs(x = NULL, 
       y = "Número de respuestas por país",
       title = "q2: Género") +
  geom_text(aes(label=Frequency), nudge_y = 8, color = "black",
             size = 2.1) +
  scale_color_brewer(palette="Pastel1") + 
  scale_fill_brewer(palette="Pastel1") +
  theme(
    strip.placement = "outside",
    strip.text.x = element_text(hjust = 0.5, vjust = 0.5),
    strip.background = element_blank(),
    panel.background = element_blank(),
    panel.grid = element_blank(),
    axis.text.x = element_blank(),
    axis.text.x.bottom = element_blank(),
    axis.text.x.top = element_blank(),
    axis.title.x.bottom = element_blank(),
    axis.ticks.x = element_blank(),
    axis.text.y = element_blank(), 
    axis.ticks.y = element_blank(),
    legend.title = element_blank(),
    legend.text = element_text(size = 8),
    plot.title = element_text(size = 10)
    )
  q2_frecuencia_por_pais
```

### Porcentaje por país

Los nombres de los países eran demasiados largos para el gráfico y, por lo tanto, se decidió cambiarlos por sus siglas con códigos de 2 letras.

```{r}
levels(q2_pais$pais)
levels(q2_pais$pais) <- c("MX", "GT", "SV", "HN", "NI", "CR", "PA", "CO", "EC",
                           "BO", "PE", "PY", "CL", "UY", "BR", "AR", "DO")
levels(q2_pais$pais)
```

Fue utilizado facet_wrap() de ggplot para construir el gráfico.

```{r}
q2_facet_wrap <- q2_pais %>%
  ggplot(aes(q2, Percent,
             fill = q2,
             color = q2,
             group=1,
  )) +
  facet_wrap(~pais,
             ncol = 17,
  ) +
  scale_y_continuous(limits = c(0, NA)) +
  geom_bar(stat="identity", position=position_dodge()) +
  labs(x = NULL,
       y = "Porcentaje de las respuestas (%)",
       title = "q2: Género") +
  geom_hline(aes(yintercept = 50, linetype = "50%"), size = 0.2, color = "blue") +
  geom_hline(aes(yintercept = 35, linetype = "35%"), size = 0.2, color = "red") +
  scale_linetype_manual(name = "limit",  
                        values = c(2, 2),
                        guide = guide_legend(reverse = TRUE,
                                             override.aes = list(color = c("blue", "red")))) +
  scale_color_brewer(palette="Pastel1") + 
  scale_fill_brewer(palette="Pastel1") +
  theme(
    strip.placement = "outside",
    strip.text.x = element_text(hjust = 0.5, vjust = 0.5),
    strip.background = element_blank(),
    panel.background = element_blank(),
    panel.grid = element_blank(),
    axis.text.x = element_blank(),
    axis.text.x.bottom = element_blank(),
    axis.text.x.top = element_blank(),
    axis.title.x.bottom = element_blank(),
    axis.ticks.x = element_blank(),
    axis.ticks.y = element_blank(),
    legend.title = element_blank(),
    legend.text = element_text(size = 8),
    plot.title = element_text(size = 10)
  )
q2_facet_wrap
```
