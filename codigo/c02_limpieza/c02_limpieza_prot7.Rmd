---
title: "limpieza_variable_prot7"
author: "jmf"
date: "`r Sys.Date()`"
output: 
  html_document:
    toc: TRUE
    toc_float: TRUE
    theme: cosmo
    highlight: zenburn
---

<!-- setwd("/home/julian/Documents/tesis-maestria_participacion-politica/codigo/c02_limpieza") -->

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE, comment = NA)
```

# Introducción

Este código tiene dos objetivos principales: el primero es verificar, limpiar y guardar todas las variables que serán utilizadas en la tesis y el segundo es reproducir el análisis estadístico del [reporte 02](file:///home/julian/Documents/tesis-maestria_participacion-politica/entregas/reporte02_04-09_pre-analisis_np1_pais/reporte02_04-09_pre-analisis_np1_pais.html).
Luego de una revisón manual de las variables, fueron añadidas algunas variables más allá de aquellas en el reporte original. Ellas son:

* q1 corresponde a sexo
* vic44: "Por temor a la delincuencia, ¿se ha organizado con los vecinos de la comunidad?"
* q2 corresponde a edad

| Variables 2012 |
|----------------|
| np1            |
| cp6            |
| cp7            |
| cp8            |
| cp13           |
| cp20           |
| prot3          |
| vb2            |
| vb20           |
| cp4a           |
| np2            |
| cp5            |
| cp2            |
| cp4            |
| cp9            |
| cp21           |
| prot7          |
| prot6          |
| prot8          |
| pp1            |
| pp2            |
| pais           |
| q1             |
| vic44          |
| q2             |


## Librerías utilizadas

```{r librerias, results = "hide", message=FALSE, warning=FALSE}

library(dplyr) # to use pipe operators
library(haven) # to import database
library(tidyverse) # to use ggplot

```

## Importación de la base de datos

```{r import}
lapop12_pais <- readr::read_rds(
  "/home/julian/Documents/tesis-maestria_participacion-politica/bd_LAPOP/datos-procesados/lapop12_pais.Rds")

```

## Variable prot7
La variable prot7 depende de la respuesta positiva a prot3.
Tiene el siguiente fraseo específico: "Y ¿en los últimos doce meses, ha participado en el bloqueo de alguna calle o espacio público como forma de protesta?"


```{r}
str(lapop12_pais$prot7)
class(lapop12_pais$prot7)
unique(lapop12_pais$prot7)
table(lapop12_pais$prot7)
```
Transformación y verificación
```{r}
lapop12_pais$prot7 <- as_factor(lapop12_pais$prot7)
str(lapop12_pais$prot7)
class(lapop12_pais$prot7)
unique(lapop12_pais$prot7)
table(lapop12_pais$prot7)
```
Colapsando "NS", "NR" y "N/A"
```{r}
levels(lapop12_pais$prot7)
levels(lapop12_pais$prot7) <- list("Sí" = "Sí ha participado", 
                            "No" = "No ha participado",
                            "NA" = c("NS", "NR", "N/A"))
levels(lapop12_pais$prot7)
```
Verificando proceso de colapsar los niveles.
```{r}
str(lapop12_pais$prot7)
class(lapop12_pais$prot7)
unique(lapop12_pais$prot7)
table(lapop12_pais$prot7)
3+11+25525
806+1411+3+11+25525
```

# Análisis por frecuencia y porcentaje

```{r base-generator}

prot7_total <- lapop12_pais %>%
  dplyr::group_by(prot7) %>%
  dplyr::summarise(Frequency = n()) %>%
  dplyr::mutate(Percent = round(Frequency/sum(Frequency)*100, 1))
  prot7_total

prot7_pais <- lapop12_pais %>%
  dplyr::group_by(pais, prot7) %>%
  dplyr::summarise(Frequency = n(), .groups="drop_last") %>%
  dplyr::mutate(Percent = round(Frequency/sum(Frequency)*100, 1))
  prot7_pais

prot7_pais_transform <- lapop12_pais %>%
  dplyr::group_by(pais, prot7) %>%
  dplyr::summarise(Frequency = n(), .groups="drop_last") %>%
  dplyr::mutate(Percent = round(Frequency/sum(Frequency)*100, 1)) %>% 
  pivot_wider(id_cols = pais,
              names_from = prot7,
              values_from = c(Frequency, Percent))
  # Exportar la tabla que seguirá adjunta a este reporte
  write.table(prot7_pais_transform, file = "prot7_pais_transform.csv", sep = ",", quote = FALSE, row.names = F)
  prot7_pais_transform


prot7_MX <- lapop12_pais %>%
  dplyr::filter(pais == "México") %>% 
  dplyr::group_by(prot7) %>%
  dplyr::summarise(Frequency = n()) %>%
  dplyr::mutate(Percent = round(Frequency/sum(Frequency)*100, 1))
  prot7_MX
```
# Visualización

## Histogramas

Con el caso méxico, se generó un histograma.

```{r hist-mx}
prot7_pais %>%
  filter(pais == "México") %>% 
  ggplot(aes(prot7, Percent, fill = prot7)) + 
  # el segundo argumento identifica el eje Y como porcentaje
  geom_bar(stat="identity",  position = position_dodge()) +
  geom_text(aes(y = Percent, label = Percent), nudge_y = 2, color = "black",
             size=3) +
  theme_bw() +
  theme(legend.position="none")

```

## Probando grids and wraps

Para los gráficos finales, fue utilizado facet_wrap() de ggplot.

### Frecuencia por país

```{r}
prot7_frecuencia_por_pais <- prot7_pais %>%
  ggplot(aes(prot7, Percent,
             fill = prot7,
             color = prot7,
             group=1,
             )) +
  facet_wrap(~pais, 
             ncol = 6,
             ) +
  scale_y_continuous(limits = c(0, NA)) +
  geom_bar(stat="identity", position=position_dodge()) +
  labs(x = NULL, 
       y = "Número de respuestas por país",
       title = "Variable prot7: ¿en los últimos doce meses, ha participado en el bloqueo de alguna calle o espacio público \ncomo forma de protesta?") +
  geom_text(aes(label=Frequency), nudge_y = 8, color = "black",
             size = 2.5) +
  scale_color_brewer(palette="Pastel1") + 
  scale_fill_brewer(palette="Pastel1") +
  theme(
    strip.placement = "outside",
    strip.text.x = element_text(hjust = 0.5, vjust = 0.5),
    strip.background = element_blank(),
    panel.background = element_blank(),
    panel.grid = element_blank(),
    axis.text.x = element_blank(),
    axis.text.x.bottom = element_blank(),
    axis.text.x.top = element_blank(),
    axis.title.x.bottom = element_blank(),
    axis.ticks.x = element_blank(),
    axis.text.y = element_blank(), 
    axis.ticks.y = element_blank(),
    legend.title = element_blank(),
    legend.text = element_text(size = 8),
    plot.title = element_text(size = 10)
    )
  prot7_frecuencia_por_pais
  ggsave("prot7_frecuencia_por_pais.pdf", width = 20, height = 15, units = "cm", dpi = 300)

```

### Porcentaje por país

Los nombres de los países eran demasiados largos para el gráfico y, por lo tanto, se decidió cambiarlos por sus siglas con códigos de 2 letras.

```{r}
levels(prot7_pais$pais)
levels(prot7_pais$pais) <- c("MX", "GT", "SV", "HN", "NI", "CR", "PA", "CO", "EC",
                           "BO", "PE", "PY", "CL", "UY", "BR", "AR", "DO")
levels(prot7_pais$pais)
```

Fue utilizado facet_wrap() de ggplot para construir el gráfico.

```{r}
prot7_facet_wrap <- prot7_pais %>%
  ggplot(aes(prot7, Percent,
             fill = prot7,
             color = prot7,
             group=1,
  )) +
  facet_wrap(~pais,
             ncol = 17,
  ) +
  scale_y_continuous(limits = c(0, NA)) +
  geom_bar(stat="identity", position=position_dodge()) +
  labs(x = NULL,
       y = "Porcentaje de las respuestas (%)",
       title = "Variable prot7: ¿en los últimos doce meses, ha participado en el bloqueo de alguna calle o espacio público \ncomo forma de protesta?") +
  geom_hline(aes(yintercept = 2.9, linetype = "2.9%"), size = 0.2, color = "red") +
  geom_hline(aes(yintercept = 5.1, linetype = "5.1%"), size = 0.2, color = "blue") +
  geom_hline(aes(yintercept = 92, linetype = "92%"), size = 0.2, color = "green") +
  scale_linetype_manual(name = "Limits",
                        breaks = c("2.9%", "5.1%", "92%"),
                        values = c(2, 2, 2),
                        guide = guide_legend(reverse = FALSE,
                                             override.aes = list(
                                               color = c("red", "blue", "green")))) +
  scale_color_brewer(palette="Pastel1") + 
  scale_fill_brewer(palette="Pastel1") +
  theme(
    strip.placement = "outside",
    strip.text.x = element_text(hjust = 0.5, vjust = 0.5),
    strip.background = element_blank(),
    panel.background = element_blank(),
    panel.grid = element_blank(),
    axis.text.x = element_blank(),
    axis.text.x.bottom = element_blank(),
    axis.text.x.top = element_blank(),
    axis.title.x.bottom = element_blank(),
    axis.ticks.x = element_blank(),
    axis.ticks.y = element_blank(),
    legend.title = element_blank(),
    legend.text = element_text(size = 8),
    plot.title = element_text(size = 10)
  )
prot7_facet_wrap
ggsave("prot7_facet_wrap.pdf", width = 20, height = 15, units = "cm", dpi = 300)
```
