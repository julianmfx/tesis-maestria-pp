---
title: "limpieza_variable_cp4a"
author: "jmf"
date: "`r Sys.Date()`"
output: 
  html_document:
    toc: TRUE
    toc_float: TRUE
    theme: cosmo
    highlight: zenburn
---

<!-- setwd("/home/julian/Documents/tesis-maestria_participacion-politica/codigo/c02_limpieza") -->

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE, comment = NA)
```

# Introducción

Este código tiene dos objetivos principales: el primero es verificar, limpiar y guardar todas las variables que serán utilizadas en la tesis y el segundo es reproducir el análisis estadístico del [reporte 02](file:///home/julian/Documents/tesis-maestria_participacion-politica/entregas/reporte02_04-09_pre-analisis_np1_pais/reporte02_04-09_pre-analisis_np1_pais.html).
Luego de una revisón manual de las variables, fueron añadidas algunas variables más allá de aquellas en el reporte original. Ellas son:

* q1 corresponde a sexo
* vic44: "Por temor a la delincuencia, ¿se ha organizado con los vecinos de la comunidad?"
* q2 corresponde a edad

| Variables 2012 |
|----------------|
| np1            |
| cp6            |
| cp7            |
| cp8            |
| cp13           |
| cp20           |
| prot3          |
| vb2            |
| vb20           |
| cp4a           |
| np2            |
| cp5            |
| cp2            |
| cp4            |
| cp9            |
| cp21           |
| prot7          |
| prot6          |
| prot8          |
| pp1            |
| pp2            |
| pais           |
| q1             |
| vic44          |
| q2             |


## Librerías utilizadas

```{r librerias, results = "hide", message=FALSE, warning=FALSE}

library(dplyr) # to use pipe operators
library(haven) # to import database
library(tidyverse) # to use ggplot

```

## Importación de la base de datos

```{r import}
lapop12_pais <- readr::read_rds(
  "/home/julian/Documents/tesis-maestria_participacion-politica/bd_LAPOP/datos-procesados/lapop12_pais.Rds")

```

## Variable cp4a
La variable cp4a contiene un fraseo general: "Ahora, para hablar de otra cosa, a veces la gente y las comunidades tienen problemas que no pueden resolver por sí mismas, y para poder resolverlos piden ayuda a algún funcionario u oficina del gobierno. ¿Para poder resolver sus problemas alguna vez ha pedido usted ayuda o cooperación ..."
Y un fraseo específico: "¿A alguna autoridad local como el alcalde, municipalidad/corporación municipal, concejal, prefecto, intendente?"


```{r}
str(lapop12_pais$cp4a)
class(lapop12_pais$cp4a)
unique(lapop12_pais$cp4a)
table(lapop12_pais$cp4a)
```
Transformación y verificación
```{r}
lapop12_pais$cp4a <- as_factor(lapop12_pais$cp4a)
str(lapop12_pais$cp4a)
class(lapop12_pais$cp4a)
unique(lapop12_pais$cp4a)
table(lapop12_pais$cp4a)
```
Colapsando "NS", "NR" y "N/A"
```{r}
levels(lapop12_pais$cp4a)
levels(lapop12_pais$cp4a) <- list("Sí" = "Sí",
                                  "No" = "No",
                                  "NA" = c("NS", "NR", "N/A"))
levels(lapop12_pais$cp4a)
```
Verificando proceso de colapsar los niveles.
```{r}
str(lapop12_pais$cp4a)
class(lapop12_pais$cp4a)
unique(lapop12_pais$cp4a)
table(lapop12_pais$cp4a)
62+24+1
3624+24045+62+24+1
```

# Análisis por frecuencia y porcentaje

```{r base-generator}

cp4a_total <- lapop12_pais %>%
  dplyr::group_by(cp4a) %>%
  dplyr::summarise(Frequency = n()) %>%
  dplyr::mutate(Percent = round(Frequency/sum(Frequency)*100, 1))
  cp4a_total

cp4a_pais <- lapop12_pais %>%
  dplyr::group_by(pais, cp4a) %>%
  dplyr::summarise(Frequency = n(), .groups="drop_last") %>%
  dplyr::mutate(Percent = round(Frequency/sum(Frequency)*100, 1))
  cp4a_pais

cp4a_pais_transform <- lapop12_pais %>%
  dplyr::group_by(pais, cp4a) %>%
  dplyr::summarise(Frequency = n(), .groups="drop_last") %>%
  dplyr::mutate(Percent = round(Frequency/sum(Frequency)*100, 1)) %>% 
  pivot_wider(id_cols = pais,
              names_from = cp4a,
              values_from = c(Frequency, Percent))
  # Exportar la tabla que seguirá adjunta a este reporte
  # write.table(cp4a_pais_transform, file = "cp4a_pais_transform.csv", sep = ",", quote = FALSE, row.names = F)
  cp4a_pais_transform


cp4a_MX <- lapop12_pais %>%
  dplyr::filter(pais == "México") %>% 
  dplyr::group_by(cp4a) %>%
  dplyr::summarise(Frequency = n()) %>%
  dplyr::mutate(Percent = round(Frequency/sum(Frequency)*100, 1))
  cp4a_MX
```
# Visualización

## Histogramas

Con el caso méxico, se generó un histograma.

```{r hist-mx}
cp4a_pais %>%
  filter(pais == "México") %>% 
  ggplot(aes(cp4a, Percent, fill = cp4a)) + 
  # el segundo argumento identifica el eje Y como porcentaje
  geom_bar(stat="identity",  position = position_dodge()) +
  geom_text(aes(y = Percent, label = Percent), nudge_y = 2, color = "black",
             size=3) +
  theme_bw() +
  theme(legend.position="none")

```

## Probando grids and wraps

Para los gráficos finales, fue utilizado facet_wrap() de ggplot.

### Frecuencia por país

```{r}
cp4a_frecuencia_por_pais <- cp4a_pais %>%
  ggplot(aes(cp4a, Percent,
             fill = cp4a,
             color = cp4a,
             group=1,
             )) +
  facet_wrap(~pais, 
             ncol = 6,
             ) +
  scale_y_continuous(limits = c(0, NA)) +
  geom_bar(stat="identity", position=position_dodge()) +
  labs(x = NULL, 
       y = "Número de respuestas por país",
       title = "Variable cp4a: Pedido ayuda o cooperación a alguna autoridad local como el alcalde, \nmunicipalidad/corporación municipal, concejal, prefecto, intendente?") +
  geom_text(aes(label=Frequency), nudge_y = 8, color = "black",
             size = 2.5) +
  scale_color_brewer(palette="Pastel1") + 
  scale_fill_brewer(palette="Pastel1") +
  theme(
    strip.placement = "outside",
    strip.text.x = element_text(hjust = 0.5, vjust = 0.5),
    strip.background = element_blank(),
    panel.background = element_blank(),
    panel.grid = element_blank(),
    axis.text.x = element_blank(),
    axis.text.x.bottom = element_blank(),
    axis.text.x.top = element_blank(),
    axis.title.x.bottom = element_blank(),
    axis.ticks.x = element_blank(),
    axis.text.y = element_blank(), 
    axis.ticks.y = element_blank(),
    legend.title = element_blank(),
    legend.text = element_text(size = 8),
    plot.title = element_text(size = 10)
    )
  cp4a_frecuencia_por_pais
  ggsave("cp4a_frecuencia_por_pais.pdf", width = 20, height = 15, units = "cm", dpi = 300)
```

### Porcentaje por país

Los nombres de los países eran demasiados largos para el gráfico y, por lo tanto, se decidió cambiarlos por sus siglas con códigos de 2 letras.

```{r}
levels(cp4a_pais$pais)
levels(cp4a_pais$pais) <- c("MX", "GT", "SV", "HN", "NI", "CR", "PA", "CO", "EC",
                           "BO", "PE", "PY", "CL", "UY", "BR", "AR", "DO")
levels(cp4a_pais$pais)
```

Fue utilizado facet_wrap() de ggplot para construir el gráfico.

```{r}
cp4a_facet_wrap <- cp4a_pais %>%
  ggplot(aes(cp4a, Percent,
             fill = cp4a,
             color = cp4a,
             group=1,
  )) +
  facet_wrap(~pais,
             ncol = 17,
  ) +
  scale_y_continuous(limits = c(0, NA)) +
  geom_bar(stat="identity", position=position_dodge()) +
  labs(x = NULL,
       y = "Porcentaje de las respuestas (%)",
       title = "Variable cp4a: Pedido ayuda o cooperación a ¿A alguna autoridad local como el alcalde, \nmunicipalidad/corporación municipal, concejal, prefecto, intendente?") +
  geom_hline(aes(yintercept = 13.1, linetype = "13.1%"), size = 0.2, color = "red") +
  geom_hline(aes(yintercept = 86.6, linetype = "86.6%"), size = 0.2, color = "blue") +
  scale_linetype_manual(name = "Limits",
                        breaks = c("13.1%", "86.6%"),
                        values = c(2, 2),
                        guide = guide_legend(reverse = FALSE,
                                             override.aes = list(
                                               color = c("red", "blue")))) +
  scale_color_brewer(palette="Pastel1") + 
  scale_fill_brewer(palette="Pastel1") +
  theme(
    strip.placement = "outside",
    strip.text.x = element_text(hjust = 0.5, vjust = 0.5),
    strip.background = element_blank(),
    panel.background = element_blank(),
    panel.grid = element_blank(),
    axis.text.x = element_blank(),
    axis.text.x.bottom = element_blank(),
    axis.text.x.top = element_blank(),
    axis.title.x.bottom = element_blank(),
    axis.ticks.x = element_blank(),
    axis.ticks.y = element_blank(),
    legend.title = element_blank(),
    legend.text = element_text(size = 8),
    plot.title = element_text(size = 10)
  )
cp4a_facet_wrap
ggsave("cp4a_facet_wrap.pdf", width = 20, height = 15, units = "cm", dpi = 300)
```
