---
title: "limpieza_variable_vb20"
author: "jmf"
date: "`r Sys.Date()`"
output: 
  html_document:
    toc: TRUE
    toc_float: TRUE
    theme: cosmo
    highlight: zenburn
---

<!-- setwd("/home/julian/Documents/tesis-maestria_participacion-politica/codigo/c02_limpieza") -->

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE, comment = NA)
```

# Introducción

Este código tiene dos objetivos principales: el primero es verificar, limpiar y guardar todas las variables que serán utilizadas en la tesis y el segundo es reproducir el análisis estadístico del [reporte 02](file:///home/julian/Documents/tesis-maestria_participacion-politica/entregas/reporte02_04-09_pre-analisis_np1_pais/reporte02_04-09_pre-analisis_np1_pais.html).
Luego de una revisón manual de las variables, fueron añadidas algunas variables más allá de aquellas en el reporte original. Ellas son:

* q1 corresponde a sexo
* vic44: "Por temor a la delincuencia, ¿se ha organizado con los vecinos de la comunidad?"
* q2 corresponde a edad

| Variables 2012 |
|----------------|
| np1            |
| cp6            |
| cp7            |
| cp8            |
| cp13           |
| cp20           |
| prot3          |
| vb2            |
| vb20           |
| cp4a           |
| np2            |
| cp5            |
| cp2            |
| cp4            |
| cp9            |
| cp21           |
| prot7          |
| prot6          |
| prot8          |
| pp1            |
| pp2            |
| pais           |
| q1             |
| vic44          |
| q2             |


## Librerías utilizadas

```{r librerias, results = "hide", message=FALSE, warning=FALSE}

library(dplyr) # to use pipe operators
library(haven) # to import database
library(tidyverse) # to use ggplot

```

## Importación de la base de datos

```{r import}
lapop12_pais <- readr::read_rds(
  "/home/julian/Documents/tesis-maestria_participacion-politica/bd_LAPOP/datos-procesados/lapop12_pais.Rds")

```

## Variable vb20
La variable vb20 contiene un fraseo específico: "¿Si esta semana fueran las próximas elecciones presidenciales, qué haría usted?"


```{r}
str(lapop12_pais$vb20)
class(lapop12_pais$vb20)
unique(lapop12_pais$vb20)
table(lapop12_pais$vb20)
```
Transformación y verificación
```{r}
lapop12_pais$vb20 <- as_factor(lapop12_pais$vb20)
str(lapop12_pais$vb20)
class(lapop12_pais$vb20)
unique(lapop12_pais$vb20)
table(lapop12_pais$vb20)
```
Colapsando "NS", "NR" y "N/A"
```{r}
levels(lapop12_pais$vb20)
levels(lapop12_pais$vb20) <- list("No votaría" = "No votaría",
                                  "Votaría por situación" = "Votaría por el candidato o partido de la actual presidenta",
                                  "Votaría por oposición" = "Votaría por algún candidato o partido diferente del actual g",
                                  "Votaría Blanco/Nulo" = "Iría a votar pero dejaría la boleta en blanco o la anularía",
                                  "NA" = c("NS", "NR", "N/A"))
levels(lapop12_pais$vb20)
```
Verificando proceso de colapsar los niveles.
```{r}
str(lapop12_pais$vb20)
class(lapop12_pais$vb20)
unique(lapop12_pais$vb20)
table(lapop12_pais$vb20)
3583+855
4735+7974+7880+2729+4438
```

# Análisis por frecuencia y porcentaje

```{r base-generator}

vb20_total <- lapop12_pais %>%
  dplyr::group_by(vb20) %>%
  dplyr::summarise(Frequency = n()) %>%
  dplyr::mutate(Percent = round(Frequency/sum(Frequency)*100, 1))
  vb20_total

vb20_pais <- lapop12_pais %>%
  dplyr::group_by(pais, vb20) %>%
  dplyr::summarise(Frequency = n(), .groups="drop_last") %>%
  dplyr::mutate(Percent = round(Frequency/sum(Frequency)*100, 1))
  vb20_pais

vb20_pais_transform <- lapop12_pais %>%
  dplyr::group_by(pais, vb20) %>%
  dplyr::summarise(Frequency = n(), .groups="drop_last") %>%
  dplyr::mutate(Percent = round(Frequency/sum(Frequency)*100, 1)) %>% 
  pivot_wider(id_cols = pais,
              names_from = vb20,
              values_from = c(Frequency, Percent))
  # Exportar la tabla que seguirá adjunta a este reporte
  # write.table(vb20_pais_transform, file = "vb20_pais_transform.csv", sep = ",", quote = FALSE, row.names = F)
  vb20_pais_transform


vb20_MX <- lapop12_pais %>%
  dplyr::filter(pais == "México") %>% 
  dplyr::group_by(vb20) %>%
  dplyr::summarise(Frequency = n()) %>%
  dplyr::mutate(Percent = round(Frequency/sum(Frequency)*100, 1))
  vb20_MX
```
# Visualización

## Histogramas

Con el caso méxico, se generó un histograma.

```{r hist-mx}
vb20_pais %>%
  filter(pais == "México") %>% 
  ggplot(aes(vb20, Percent, fill = vb20)) + 
  # el segundo argumento identifica el eje Y como porcentaje
  geom_bar(stat="identity",  position = position_dodge()) +
  geom_text(aes(y = Percent, label = Percent), nudge_y = 2, color = "black",
             size=3) +
  theme_bw() +
  theme(legend.position="none")

```

## Probando grids and wraps

Para los gráficos finales, fue utilizado facet_wrap() de ggplot.

### Frecuencia por país

```{r}
vb20_frecuencia_por_pais <- vb20_pais %>%
  ggplot(aes(vb20, Percent,
             fill = vb20,
             color = vb20,
             group=1,
             )) +
  facet_wrap(~pais, 
             ncol = 6,
             ) +
  scale_y_continuous(limits = c(0, NA)) +
  geom_bar(stat="identity", position=position_dodge()) +
  labs(x = NULL, 
       y = "Número de respuestas por país",
       title = "Variable vb20: ¿Si esta semana fueran las próximas elecciones presidenciales, qué haría usted?") +
  geom_text(aes(label=Frequency), nudge_y = 8, color = "black",
             size = 1.9) +
  scale_color_brewer(palette="Pastel1") + 
  scale_fill_brewer(palette="Pastel1") +
  theme(
    strip.placement = "outside",
    strip.text.x = element_text(hjust = 0.5, vjust = 0.5),
    strip.background = element_blank(),
    panel.background = element_blank(),
    panel.grid = element_blank(),
    axis.text.x = element_blank(),
    axis.text.x.bottom = element_blank(),
    axis.text.x.top = element_blank(),
    axis.title.x.bottom = element_blank(),
    axis.ticks.x = element_blank(),
    axis.text.y = element_blank(), 
    axis.ticks.y = element_blank(),
    legend.title = element_blank(),
    legend.text = element_text(size = 8),
    plot.title = element_text(size = 10)
    )
  vb20_frecuencia_por_pais
  ggsave("vb20_frecuencia_por_pais.pdf", width = 20, height = 15, units = "cm", dpi = 300)
```

### Porcentaje por país

Los nombres de los países eran demasiados largos para el gráfico y, por lo tanto, se decidió cambiarlos por sus siglas con códigos de 2 letras.

```{r}
levels(vb20_pais$pais)
levels(vb20_pais$pais) <- c("MX", "GT", "SV", "HN", "NI", "CR", "PA", "CO", "EC",
                           "BO", "PE", "PY", "CL", "UY", "BR", "AR", "DO")
levels(vb20_pais$pais)
```

Fue utilizado facet_wrap() de ggplot para construir el gráfico.

```{r}
vb20_facet_wrap <- vb20_pais %>%
  ggplot(aes(vb20, Percent,
             fill = vb20,
             color = vb20,
             group=1,
  )) +
  facet_wrap(~pais,
             ncol = 17,
  ) +
  scale_y_continuous(limits = c(0, NA)) +
  geom_bar(stat="identity", position=position_dodge()) +
  labs(x = NULL,
       y = "Porcentaje de las respuestas (%)",
       title = "Variable vb20: ¿Si esta semana fueran las próximas elecciones presidenciales, qué haría usted?") +
  geom_hline(aes(yintercept = 17.1, linetype = "17.1%"), size = 0.2, color = "red") +
  geom_hline(aes(yintercept = 28.7, linetype = "28.7%"), size = 0.2, color = "blue") +
  geom_hline(aes(yintercept = 28.4, linetype = "28.4%"), size = 0.2, color = "green") +
  geom_hline(aes(yintercept = 9.6, linetype = "9.6%"), size = 0.2, color = "purple") +
  geom_hline(aes(yintercept = 16, linetype = "16%"), size = 0.2, color = "orange") +
  scale_linetype_manual(name = "Limits",
                        breaks = c("17.1%", "28.7%", "28.4%", "9.6%", "16%"),
                        values = c(2, 2, 2, 2, 2),
                        guide = guide_legend(reverse = FALSE,
                                             override.aes = list(
                                               color = c("red", "blue", "green", "purple", "orange")))) +
  scale_color_brewer(palette="Pastel1") + 
  scale_fill_brewer(palette="Pastel1") +
  theme(
    strip.placement = "outside",
    strip.text.x = element_text(hjust = 0.5, vjust = 0.5),
    strip.background = element_blank(),
    panel.background = element_blank(),
    panel.grid = element_blank(),
    axis.text.x = element_blank(),
    axis.text.x.bottom = element_blank(),
    axis.text.x.top = element_blank(),
    axis.title.x.bottom = element_blank(),
    axis.ticks.x = element_blank(),
    axis.ticks.y = element_blank(),
    legend.title = element_blank(),
    legend.text = element_text(size = 8),
    plot.title = element_text(size = 10)
  )
vb20_facet_wrap
ggsave("vb20_facet_wrap.pdf", width = 20, height = 15, units = "cm", dpi = 300)
```
