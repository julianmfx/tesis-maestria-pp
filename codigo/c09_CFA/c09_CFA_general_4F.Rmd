---
  title: "Análisis factorial confirmatorio general"
author: "jmf"
date: "`r Sys.Date()`"
output:   
  html_document:
  toc: TRUE
toc_float: TRUE
theme: cosmo
highlight: zenburn
---

# Introducción

Este código tiene como objetivo hacer el análisis factorial exploratorio de la tesis.

```{r setup, include = FALSE}
knitr::opts_chunk$set(echo = TRUE)
```

# Librerías
```{r, message = FALSE}
library(lavaan)
library(semPlot)
library(dplyr)
library(semTable)
library(arm) # to use standardize ()
library(semTools) # to do power analysis
```


# Importando la base de datos

```{r}
lapop12 <- read.csv("/home/julian/Documents/tesis-maestria_participacion-politica/bd_LAPOP/datos-procesados/efa/lapop12_EFA_Juli.csv")
```

```{r}
glimpse(lapop12)
```


# Limpieza

```{r}
# drop <- c("cp20", "prot7", "prot6", "vb20", "q1", "q2", "pais")
# lapop12_EFA <- lapop12[ , !(names(lapop12) %in% drop)]
```

## Omitir NAs

```{r}
# lapop12 <- na.omit(lapop12)
```


* 1. versión completa OK
* 2. versión sin deporte OK
* 3. versión sin religion
* 4. versión sin deporte y sin religión OK
* 5. versión con factor comunitario distinto
* 6. versión con factor protesta distinto
* 7. versión con factor votar distinto

# Definiendo estructura del análisis confirmatorio

## General - Modelo D
```{r}
CFA <- "
voz =~ 1*cp4a + cp4 + np2 + cp2 + np1
comunidad =~ 1*cp8 + cp5 + cp7 + cp6 +  vic44 + cp9
partidista =~ 1*pp2 + pp1 + cp13 + vb2
protesta =~ 1*prot8 + cp21 + prot3 + vb2"
```

## Sin voto - Modelo E
```{r}
CFA <- "
voz =~ 1*cp4a + cp4 + np2 + cp2 + np1
comunidad =~ 1*cp8 + cp5 + cp7 + cp6 +  vic44 + cp9
partidista =~ 1*pp2 + pp1 + cp13
protesta =~ 1*prot8 + cp21 + prot3"
```

## Sin voto y religión, deporte en protesta - modelo P
```{r}
CFA <- "
voz =~ 1*cp4a + cp4 + np2 + cp2 + np1
comunidad =~ 1*cp8 + cp5 + cp7 + cp9 + vic44
partidista =~ 1*pp2 + pp1 + cp13
protesta =~ 1*prot8 + prot3 + cp21"
```

## Sin deporte - Modelo F
```{r}
CFA <- "
voz =~ 1*cp4a + cp4 + np2 + cp2 + np1
comunidad =~ 1*cp8 + cp5 + cp7 + cp6 +  vic44 + cp9
partidista =~ 1*pp2 + pp1 + cp13 + vb2
protesta =~ 1*prot8 + prot3 + vb2"
```

## Sin deporte y con vb2 en partidista
```{r}
CFA <- "
voz =~ 1*cp4a + cp4 + np2 + cp2 + np1
comunidad =~ 1*cp8 + cp5 + cp7 + cp6 +  vic44 + cp9
partidista =~ 1*pp2 + pp1 + cp13 + vb2
protesta =~ 1*prot8 + prot3"
```

## Sin religión - Modelo H
```{r}
CFA <- "
voz =~ 1*cp4a + cp4 + np2 + cp2 + np1
comunidad =~ 1*cp8 + cp5 + cp7 +  vic44 + cp9
partidista =~ 1*pp2 + pp1 + cp13 + vb2
protesta =~ 1*prot8 + cp21 + prot3 + vb2"
```

## Sin deporte y sin religión - modelo I
```{r}
CFA <- "
voz =~ 1*cp4a + cp4 + np2 + cp2 + np1
comunidad =~ 1*cp8 + cp5 + cp7 +  vic44 + cp9
partidista =~ 1*pp2 + pp1 + cp13 + vb2
protesta =~ 1*prot8 + prot3 + vb2"
```

## Sin deporte, religión y voto - modelo J
```{r}
CFA <- "
voz =~ 1*cp4a + cp4 + np2 + cp2 + np1
comunidad =~ 1*cp8 + cp5 + cp7 +  vic44 + cp9
partidista =~ 1*pp2 + pp1 + cp13
protesta =~ 1*prot8 + prot3"
```

## Sin voto y con deporte en comunitario - modelo K
```{r}
CFA <- "
voz =~ 1*cp4a + cp4 + np2 + cp2 + np1
comunidad =~ 1*cp8 + cp5 + cp7 + cp6 +  vic44 + cp9 + cp21
partidista =~ 1*pp2 + pp1 + cp13
protesta =~ 1*prot8 + prot3"
```

## Sin deporte y sin voto - modelo L
```{r}
CFA <- "
voz =~ 1*cp4a + cp4 + np2 + cp2 + np1
comunidad =~ 1*cp8 + cp5 + cp7 + cp6 +  vic44 + cp9
partidista =~ 1*pp2 + pp1 + cp13
protesta =~ 1*prot8 + prot3"
```

## Sin escuela - Modelo M
```{r}
CFA <- "
voz =~ 1*cp4a + cp4 + np2 + cp2 + np1
comunidad =~ 1*cp8 + cp5 + cp6 +  vic44 + cp9
partidista =~ 1*pp2 + pp1 + cp13 + vb2
protesta =~ 1*prot8 + cp21 + prot3 + vb2"
```

## Voto en partidista y deporte en comunitario - modelo Q
```{r}
CFA <- "
voz =~ 1*cp4a + cp4 + np2 + cp2 + np1
comunidad =~ 1*cp8 + cp5 + cp7 + cp6 +  vic44 + cp9 + cp21
partidista =~ 1*pp2 + pp1 + cp13 + vb2
protesta =~ 1*prot8 + prot3"
```

## Voto en partidista y sin deporte - Modelo G
```{r}
CFA <- "
voz =~ 1*cp4a + cp4 + np2 + cp2 + np1
comunidad =~ 1*cp8 + cp5 + cp7 + cp6 +  vic44 + cp9
partidista =~ 1*pp2 + pp1 + cp13 + vb2
protesta =~ 1*prot8 + prot3"
```

## Voto en partidista y deporte comuniatario
```{r}
CFA <- "
voz =~ 1*cp4a + cp4 + np2 + cp2 + np1
comunidad =~ 1*cp8 + cp5 + cp7 + cp6 +  vic44 + cp9 + cp21
partidista =~ 1*pp2 + pp1 + cp13 + vb2
protesta =~ 1*prot8 + prot3"
```

## Sin religión y voto, con deporte en comunidad
```{r}
CFA <- "
voz =~ 1*cp4a + cp4 + np2 + cp2 + np1
comunidad =~ 1*cp8 + cp5 + cp7 +  vic44 + cp9 + cp21
partidista =~ 1*pp2 + pp1 + cp13
protesta =~ 1*prot8 + prot3"
```

## Sin deporte y religión y voto en partidista - modelo X
```{r}
CFA <- "
voz =~ 1*cp4a + cp4 + np2 + cp2 + np1
comunidad =~ 1*cp8 + cp5 + cp7 +  vic44 + cp9
partidista =~ 1*pp2 + pp1 + cp13 + vb2
protesta =~ 1*prot8 + prot3"
```

## Voto solo en protesta
```{r}
CFA <- "
voz =~ 1*cp4a + cp4 + np2 + cp2 + np1
comunidad =~ 1*cp8 + cp5 + cp7 + cp6 +  vic44 + cp9
partidista =~ 1*pp2 + pp1 + cp13 
protesta =~ 1*prot8 + cp21 + prot3 + vb2"
```

## Nuevo

```{r}
CFA <- "
voz =~ 1*cp4a + cp4 + np2 + cp2 + np1
comunidad =~ 1*cp8 + cp5 + cp7 +  vic44 + cp9
partidista =~ 1*pp2 + pp1 + cp13 
protesta =~ 1*prot8 + cp21 + prot3"
```

# Análisis
```{r}
modelo.medicion <- sem(CFA, data = lapop12)
summary(modelo.medicion, fit.measures = TRUE, standardized = TRUE)
semPaths(modelo.medicion, "std", edge.label.cex = 1.2, fade = FALSE)
```

```{r}
fitMeasures(modelo.medicion, c("cfi", "tli", "rmsea.ci.lower", "rmsea", "rmsea.ci.upper", "srmr","chisq", "df", "pvalue", "npar", "aic", "bic"))
```

```{r}
fitMeasures(modelo.medicion)
```


```{r, message = FALSE}
semTable(modelo.medicion, file = "/home/julian/Documents/tesis-maestria_participacion-politica/illustrations/cfa/P_4F_general_sin-vb2-cp6", type = "html")
```

# Matriz de residuales
```{r}
residuals <- residuals(modelo.medicion)
head(residuals)
```

```{r}
# Calculate the correlation matrix of the residuals
residual.cor <- cor(residuals$cov)

# Display the correlation matrix of the residuals
round(residual.cor, 2)
```

```{r}
corrplot(residual.cor, method = "circle")
corrplot(residual.cor, method = "pie")
corrplot(residual.cor, method = "color")
corrplot(residual.cor, method = "circle", type = "upper")
corrplot(residual.cor, method = "circle", type = "lower")
corrplot(residual.cor, method = "circle", type = "upper", order = "hclust") # hierarquichal clustering
```


```{r}
# To compute the matrix of p-value, a custom R function is used :
# mat : is a matrix of data
# ... : further arguments to pass to the native R cor.test function
cor.mtest <- function(mat, ...) {
    mat <- as.matrix(mat)
    n <- ncol(mat)
    p.mat<- matrix(NA, n, n)
    diag(p.mat) <- 0
    for (i in 1:(n - 1)) {
        for (j in (i + 1):n) {
            tmp <- cor.test(mat[, i], mat[, j], ...)
            p.mat[i, j] <- p.mat[j, i] <- tmp$p.value
        }
    }
  colnames(p.mat) <- rownames(p.mat) <- colnames(mat)
  p.mat
}
```

```{r}
# matrix of the p-value of the correlation
p.mat <- cor.mtest(residuals$cov)
head(p.mat)
```
```{r}
# Specialized the insignificant value according to the significant level
corrplot(residual.cor, type = "upper", method = "number", order = "hclust", 
         p.mat = p.mat, sig.level = 0.05, insig = "blank", number.cex = 0.6)
```

## Customize the correlogram
```{r}
col <- colorRampPalette(c("#BB4444", "#EE9988", "#FFFFFF", "#77AADD", "#4477AA"))
corrplot(residual.cor, method = "color", col = col(200),
         type = "upper", order = "hclust", 
         addCoef.col = "black", # Add coefficient of correlation
         tl.col = "black", tl.srt = 45, #Text label color and rotation
         # Combine with significance
         # p.mat = p.mat, sig.level = 0.05, insig = "blank",
         # hide correlation coefficient on the principal diagonal
         diag = FALSE,
         number.cex = 0.5
         )
```

