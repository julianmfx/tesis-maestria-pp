

# Introducción

Este código tiene como objetivo hacer el análisis factorial exploratorio de la tesis.

# Librerías

library(lavaan)
library(semPlot)
library(dplyr)
library(car) # ???

# Importando la base de datos

datos <- read.csv("/home/julian/Documents/tesis-maestria_participacion-politica/bd_LAPOP/datos-procesados/efa/lapop12_EFA_Juli.csv")
datos_compl <- na.omit(datos)


CFA <- "voz =~ 1*cp4a + cp4 + np2 + cp2 + np1
        comunidad =~ 1*cp8 + cp5 + cp9 + cp7 + vic44
        partidista =~ 1*pp2 + pp1 + cp13
        protesta =~ 1*prot8 + prot3
        convencional =~ 1*vb2"

modelo.medicion <- sem(CFA, data = datos_compl)
summary(modelo.medicion, fit.measures = TRUE, standardized = TRUE)
semPaths(modelo.medicion, "std", edge.label.cex = 1.2, fade = FALSE) 
