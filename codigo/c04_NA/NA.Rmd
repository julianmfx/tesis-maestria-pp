```{r}
library(dplyr) # to use pipe operators
```

```{r}
lapop12_A <- readr::read_rds(
  "/home/julian/Documents/tesis-maestria_participacion-politica/bd_LAPOP/datos-procesados/lapop12_A.Rds")
```

# Creation of NAs in the variable

## ifelse()

```{r}
x <- 1
ifelse(x == 1, "this is a true expression", "WRONG")
```

```{r}
x <- 2
ifelse(x == 1, "this is a true expression", "WRONG")
```

## Testing with variable prot6

```{r}
levels(lapop12_A$prot6)

unique(lapop12_A$prot6)

class(lapop12_A$prot6)

table(lapop12_A$prot6)

table(paste(lapop12_A$prot6))

sum(is.na(lapop12_A$prot6)) # the formula is.na cannot identify NAs in my variable. WHY??
```

```{r}
a <- lapop12_A$prot6
table(a)
table(paste(a))
```
```{r}
total_table(lapop12_A, prot6)
```


The ifelse below can be traduced in this way: if data in **a** is equal to "Sí" or "No", return **a**, else return **NA**; or if the output of test expression is TRUE, it returns "yes" argument and f the output of test expression is FALSE, it returns "no" argument.

ifelse(test, yes, no)

-   **test** = an object that can be coerced to logical mode
-   **yes** = return values for true elements of **test**.
-   **no** = return values for false elements of **test**.


```{r}
a <- ifelse(test = a == "Sí" | a == "No", yes = a, no = NA)
```


```{r}
table(a)
sum(is.na(a))

sum(is.na(lapop12_A$prot6))
total_table(lapop12_A, prot6)
```

Testing inside de dataframe
```{r}
lapop12_A$prot6 
table(lapop12_A$prot6) 
table(paste(lapop12_A$prot6))
```


```{r}
lapop12_A$prot6 <- ifelse(lapop12_A$prot6 == "Sí" | lapop12_A$prot6 == "No", lapop12_A$prot6, NA)
```


```{r}
lapop12_A$prot6
```

No is.na() recognize NA values, but transformed the variable to a integer
```{r}
sum(is.na(lapop12_A$prot6))

levels(lapop12_A$prot6)

unique(lapop12_A$prot6)

class(lapop12_A$prot6)

table(lapop12_A$prot6)

table(paste(lapop12_A$prot6))

sum(is.na(lapop12_A$prot6)) 

total_table(lapop12_A, prot6)
```

