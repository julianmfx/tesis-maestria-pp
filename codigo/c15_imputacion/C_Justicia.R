value_imputed <- data.frame(
  original = df$C_Justicia,
  imputed_zero = replace(df$C_Justicia, is.na(df$C_Justicia), 0),
  imputed_mean = replace(df$C_Justicia, is.na(df$C_Justicia), mean(df$C_Justicia, na.rm = TRUE)),
  imputed_median = replace(df$C_Justicia, is.na(df$C_Justicia), median(df$C_Justicia, na.rm = TRUE))
)
value_imputed

h1 <- ggplot(value_imputed, aes(x = original)) +
  geom_histogram(fill = "#ad1538", color = "#000000", position = "identity") +
  ggtitle("Original distribution") +
  theme_classic()
h2 <- ggplot(value_imputed, aes(x = imputed_zero)) +
  geom_histogram(fill = "#15ad4f", color = "#000000", position = "identity") +
  ggtitle("Zero-imputed distribution") +
  theme_classic()
h3 <- ggplot(value_imputed, aes(x = imputed_mean)) +
  geom_histogram(fill = "#1543ad", color = "#000000", position = "identity") +
  ggtitle("Mean-imputed distribution") +
  theme_classic()
h4 <- ggplot(value_imputed, aes(x = imputed_median)) +
  geom_histogram(fill = "#ad8415", color = "#000000", position = "identity") +
  ggtitle("Median-imputed distribution") +
  theme_classic()


cowplot::plot_grid(h1, h2, h3, h4, nrow = 2, ncol = 2)

# Mice imputation

mice_df <- df |>
  select(C_Justicia, C_Congreso, C_Partidos)

mice_imputed <-  mice_df |> 
  data.frame(
    original = mice_df$C_Justicia,
    imputed_pmm = complete(mice(mice_df, method = "pmm"))$C_Justicia,
    imputed_cart = complete(mice(mice_df, method = "cart"))$C_Justicia,
    imputed_lasso = complete(mice(mice_df, method = "lasso.norm"))$C_Justicia
  )

mice_imputed

h1 <- ggplot(mice_imputed, aes(x = original)) +
  geom_histogram(fill = "#ad1538", color = "#000000", position = "identity") +
  ggtitle("Original distribution") +
  theme_classic()
h2 <- ggplot(mice_imputed, aes(x = imputed_pmm)) +
  geom_histogram(fill = "#15ad4f", color = "#000000", position = "identity") +
  ggtitle("PMM-imputed distribution") +
  theme_classic()
h3 <- ggplot(mice_imputed, aes(x = imputed_cart)) +
  geom_histogram(fill = "#1543ad", color = "#000000", position = "identity") +
  ggtitle("Cart-imputed distribution") +
  theme_classic()
h4 <- ggplot(mice_imputed, aes(x = imputed_lasso)) +
  geom_histogram(fill = "#ad8415", color = "#000000", position = "identity") +
  ggtitle("Lasso-imputed distribution") +
  theme_classic()


cowplot::plot_grid(h1, h2, h3, h4, nrow = 2, ncol = 2) # tanto PMM como CART son buenas
