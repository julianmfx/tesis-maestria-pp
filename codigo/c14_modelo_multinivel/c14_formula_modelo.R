Edad como función inversa mejora el modelo protesta

f(edad) =  1 / edad

ln

m <- lmerTest::lmer(Protesta ~1 + Edad + (1 | pais),
                            data = datos,
                            REML = TRUE,
                            verbose = 0,
                            # weights = wt, # disminuye un poco los residuales: peso del país
                            weights = weight1500, # disminuye más residuales: peso para darle el mismo tamaño a cada país
                            na.action = na.omit)
summary(m)

datos$Edad1 <- 1/datos$Edad

m1 <- lmerTest::lmer(Protesta ~1 + Edad1 + (1 | pais),
                    data = datos,
                    REML = TRUE,
                    verbose = 0,
                    # weights = wt, # disminuye un poco los residuales: peso del país
                    weights = weight1500, # disminuye más residuales: peso para darle el mismo tamaño a cada país
                    na.action = na.omit)
summary(m1)

anova(m, m1)

datos$Edad1 <- 1/datos$Edad

m1 <- lmerTest::lmer(Protesta ~1 + Edad1 + (1 | pais),
                     data = datos,
                     REML = TRUE,
                     verbose = 0,
                     # weights = wt, # disminuye un poco los residuales: peso del país
                     weights = weight1500, # disminuye más residuales: peso para darle el mismo tamaño a cada país
                     na.action = na.omit)
summary(m1)


# Educación

unique(datos$Educacion)
table(datos$Educacion, useNA = "ifany")


m <- lmerTest::lmer(Protesta ~ 1 + Educacion + (1 | pais),
                     data = datos,
                     REML = TRUE,
                     verbose = 0,
                     # weights = wt, # disminuye un poco los residuales: peso del país
                     weights = weight1500, # disminuye más residuales: peso para darle el mismo tamaño a cada país
                     na.action = na.omit)
summary(m)

datos$Educacion1 <- datos$Educacion * datos$Educacion

m1 <- lmerTest::lmer(Protesta ~ 1 + Educacion1 + (1 | pais),
                    data = datos,
                    REML = TRUE,
                    verbose = 0,
                    # weights = wt, # disminuye un poco los residuales: peso del país
                    weights = weight1500, # disminuye más residuales: peso para darle el mismo tamaño a cada país
                    na.action = na.omit)
summary(m1)

## dropar NAs

df <- na.omit(datos[ , c("Protesta", # individuales
                         "Educacion")]) 

datos$Educacionlog <- log(datos$Educacion)
table(datos$Educacionlog, useNA = "ifany")


datos$Educacionlog <- datos$Educacionlog[!is.na(datos$Educacionlog)]


datos$Educacionlog[!is.finite(datos$Educacionlog)] <- 0
998+438
998+438+94
table(datos$Educacionlog)

m2 <- lmerTest::lmer(Protesta ~ 1 + Educacionlog + (1 | pais),
                     data = datos,
                     REML = TRUE,
                     verbose = 0,
                     # weights = wt, # disminuye un poco los residuales: peso del país
                     weights = weight1500, # disminuye más residuales: peso para darle el mismo tamaño a cada país
                     na.action = na.omit)
summary(m2)


anova(m, m1)

# Ingreso

unique(datos$Ingreso)
table(datos$Ingreso, useNA = "ifany")

m <- lmerTest::lmer(Protesta ~ 1 + Ingreso + (1 | pais),
                    data = datos,
                    REML = TRUE,
                    verbose = 0,
                    # weights = wt, # disminuye un poco los residuales: peso del país
                    weights = weight1500, # disminuye más residuales: peso para darle el mismo tamaño a cada país
                    na.action = na.omit)
summary(m)

datos$Ingreso1 <- datos$Ingreso * datos$Ingreso

m1 <- lmerTest::lmer(Protesta ~ 1 + Ingreso1 + (1 | pais),
                     data = datos,
                     REML = TRUE,
                     verbose = 0,
                     # weights = wt, # disminuye un poco los residuales: peso del país
                     weights = weight1500, # disminuye más residuales: peso para darle el mismo tamaño a cada país
                     na.action = na.omit)
summary(m1)

anova(m, m1)




# Sexo
unique(datos$Sexo)
table(datos$Sexo)
datos$Sexo[datos$Sexo == 2] <- 0 # Mujer asume el valor de 0
unique(datos$Sexo)
table(datos$Sexo)


