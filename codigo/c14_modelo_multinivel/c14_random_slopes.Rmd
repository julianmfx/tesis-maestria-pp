---
title: "c14_multinivel"
author: "jmf"
date: "`r Sys.Date()`"
output:   
  html_document:
    toc: TRUE
    toc_float: TRUE
    theme: cosmo
    highlight: zenburn
---

Este código tiene como función encontrar el mejor modelo multinivel que represente los datos.

Necesito reorganizar el código según los apuntes de la reunión de hoy.

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)
```

# Librerías

```{r, message = FALSE}
library(dplyr)
library(tidyverse) # to use ggplot
library(labelled) # to deal with "haven_labelled" variables (it is automatical);
library(nlme)
library(lme4) # paquete original
library(lmerTest) # paquete con pvalues
library(fitdistrplus) # to use fitdist()
library(multilevel) # to try new functions
library(r2mlm) # to calculate r-squared. No funciona muy bien
library(sjPlot) # to use tab_model()
library(lattice) # to use qqmath()
```


# Importando base de datos: modelo final ponderado y policlórico

```{r}
datos <- readr::read_rds(
  "/home/julian/Documents/tesis-maestria_participacion-politica/bd_LAPOP/datos-procesados/multinivel_final.Rds")
```

# Base con indicador de confianza

```{r ind_conf}
datos <- readr::read_rds(
  "/home/julian/Documents/tesis-maestria_participacion-politica/bd_LAPOP/datos-procesados/multinivel_pmm.Rds")
```

```{r}
glimpse(datos)
```

# Limpieza

Quitar NAs de la base. Dependiendo de la función utilizada es necesario tener un conjunto de datos sin valores faltantes.

La idea de crear un nuevo objeto permite eliminar los valores faltantes solamente de las variables que voy a utilizar

## Simpatizar partido

Como podemos ver, simpatizar partido está codificado de una manera distinta a lo que quiero:
   
   * 1 = Sí
   * 2 = No

```{r}
unique(datos$Simpatizar_partido)
table(datos$Simpatizar_partido, useNA = "ifany")
```

Iré cambiar eso para NO == 0

```{r}
datos$Simpatizar_partido[datos$Simpatizar_partido == 2] <- 0
table(datos$Simpatizar_partido, useNA = "ifany")
```

## Quitar NAs

Depende de cuáles variables voy a utilizar

```{r}
df <- na.omit(datos[ , c("Voz", "Comunitaria", "Partidista", "Protesta", # individuales
                         "Sexo", "Edad", "Educacion", "Ingreso", 
                         "Etnia", "Religion", "Votar", "Simpatizar_partido", 
                         "Corrupción", "Tamano", 
                         "gdp_growth", "gdp_pc_ppp", "log_pob", "ele_nac_2011", # nacionales
                         "ele_reg_2011", "federalista", "cpi", "tas_hom", 
                         "indg_pob_2010", "pais")]) 
```

# Creación

## Variable de confianza institucional = OK

Código `mice_imputation.R`
Cargar base con indicador de confianza

```{r, ref.label = ind_conf)}
```

  
## Juntar elecciones nacionales y religionales = OK

```{r}
datos <- datos |>
  dplyr::mutate(ele_2011 = (ele_nac_2011 + ele_reg_2011))
datos$ele_2011[datos$ele_2011 == 2] <- 1
```

0 = 18242
1 = 7056
```{r}
table(datos$ele_2011)
```

```{r}
glimpse(datos)
```


## Porcentaje de participación en la elección presidencial anterior


```{r}
table(datos$pais)
datos$pp_electoral <- 0
datos$pp_electoral[datos$pais == "AR"] <- 79.39
datos$pp_electoral[datos$pais == "BO"] <- 94.54
datos$pp_electoral[datos$pais == "BR"] <- 80.19
datos$pp_electoral[datos$pais == "CL"] <- 87.3
datos$pp_electoral[datos$pais == "CO"] <- 46.825
datos$pp_electoral[datos$pais == "CR"] <- 69.14
datos$pp_electoral[datos$pais == "EC"] <- 75.3
datos$pp_electoral[datos$pais == "SV"] <- 62.92
datos$pp_electoral[datos$pais == "GT"] <- 65.105
datos$pp_electoral[datos$pais == "HN"] <- 49.88
datos$pp_electoral[datos$pais == "MX"] <- 58.55
datos$pp_electoral[datos$pais == "NI"] <- 73.9
datos$pp_electoral[datos$pais == "PA"] <- 74.00
datos$pp_electoral[datos$pais == "PY"] <- 65.43
datos$pp_electoral[datos$pais == "PE"] <- 83.125
datos$pp_electoral[datos$pais == "DO"] <- 71.44
datos$pp_electoral[datos$pais == "UY"] <- 89.49
table(datos$pp_electoral)
```


## Voto obligatorio

Artículo de Chávez (2020)

### Sin sanción

```{r}
table(datos$pais)
datos$v_oblg <- 0
datos$v_oblg[datos$pais == "AR"] <- 1
datos$v_oblg[datos$pais == "BR"] <- 1
datos$v_oblg[datos$pais == "BO"] <- 1
datos$v_oblg[datos$pais == "EC"] <- 1
datos$v_oblg[datos$pais == "PE"] <- 1
datos$v_oblg[datos$pais == "UY"] <- 1
datos$v_oblg[datos$pais == "CR"] <- 1
datos$v_oblg[datos$pais == "HN"] <- 1
datos$v_oblg[datos$pais == "MX"] <- 1
datos$v_oblg[datos$pais == "PA"] <- 1
datos$v_oblg[datos$pais == "PY"] <- 1
datos$v_oblg[datos$pais == "DO"] <- 1
table(datos$v_oblg)
1398+2650+1434+1233+1350+1438+1373+1393+1490+1465+1409+1469 # 18102 = OK
```


### Con sanción

```{r}
table(datos$pais)
datos$v_oblg_san <- 0
datos$v_oblg_san[datos$pais == "AR"] <- 1
datos$v_oblg_san[datos$pais == "BO"] <- 1
datos$v_oblg_san[datos$pais == "BR"] <- 1
datos$v_oblg_san[datos$pais == "EC"] <- 1
datos$v_oblg_san[datos$pais == "PE"] <- 1
datos$v_oblg_san[datos$pais == "UY"] <- 1
table(datos$v_oblg_san)
1398+2650+1434+1233+1350+1438 # 9503 = OK
```

## Porcentaje de población urbana

Datos de CepalStat

```{r}
table(datos$pais)
datos$pp_urb_2010 <- 0
datos$pp_urb_2010[datos$pais == "AR"] <- 90.97405387
datos$pp_urb_2010[datos$pais == "BO"] <- 66.77470767
datos$pp_urb_2010[datos$pais == "BR"] <- 84.34865784
datos$pp_urb_2010[datos$pais == "CL"] <- 88.10295775
datos$pp_urb_2010[datos$pais == "CO"] <- 77.65474907
datos$pp_urb_2010[datos$pais == "CR"] <- 71.59210525
datos$pp_urb_2010[datos$pais == "EC"] <- 62.68486483
datos$pp_urb_2010[datos$pais == "SV"] <- 65.23294843
datos$pp_urb_2010[datos$pais == "GT"] <- 52.0239225
datos$pp_urb_2010[datos$pais == "HN"] <- 50.77136833
datos$pp_urb_2010[datos$pais == "MX"] <- 76.48389912
datos$pp_urb_2010[datos$pais == "NI"] <- 56.82951244
datos$pp_urb_2010[datos$pais == "PA"] <- 65.16724178
datos$pp_urb_2010[datos$pais == "PY"] <- 63.35992274
datos$pp_urb_2010[datos$pais == "PE"] <- 76.91950084
datos$pp_urb_2010[datos$pais == "DO"] <- 73.69199453
datos$pp_urb_2010[datos$pais == "UY"] <- 94.19087211
table(datos$pp_urb_2010)
```


```{r}
glimpse(datos)
```

# Principales preguntas

## Modelo con random slopes a veces se tarda mucho en calcular
  
 * Por qué?

## Problemas con random slopes

  * Cuándo usar random slopes?
  * Qué variables usar?
    * Vi que sacar tamaño cambia mucho

## Singular fit

Read answers on the question made by [kjetil on StackExchange](https://stats.stackexchange.com/questions/378939/dealing-with-singular-fit-in-mixed-models)

1. Are there theoretical reasons to establish random slopes?
2. The theory of how data was generated suggest this?

Answer by Stupid Wolf on [StackOverFlow](https://stats.stackexchange.com/questions/378939/dealing-with-singular-fit-in-mixed-models) is also interesting.

# Coeficientes aleatorios

## País como coeficiente aleatorio

** boundary (singluar) fit
** Model failed to converge with 56 negative eigenvalues


```{r null-pais}
modelo <- lmerTest::lmer(Voz ~ 1 + (1 + pais | pais), data = datos, na.action = na.omit)
```
```{r}
summary(modelo)
```

```{r}
ranef(modelo)
```

## Por país

tarda mucho en calcular (1 hora más o menos)

 * Failed to converge with 29 negatives eigenvalue
 * boundary (singular) fit

```{r random1}
modelo <- lmerTest::lmer(Voz ~ Sexo + Edad + Educacion + Ingreso + Etnia + Religion + Votar + Simpatizar_partido + Corrupción + scale(gdp_growth) + scale(gdp_pc_ppp) + log_pob + ele_nac_2011 + ele_reg_2011 + federalista + scale(cpi) + scale(tas_hom) + scale(indg_pob_2010) + (1 + pais | pais), data = datos, na.action = na.omit)
```

## Por federalista

* Casi

```{r}
# modelo <- lmerTest::lmer(Voz ~ Sexo + Edad + Educacion + Ingreso + Etnia + Religion + Votar + Simpatizar_partido + Corrupción + scale(gdp_growth) + scale(gdp_pc_ppp) + log_pob + ele_nac_2011 + ele_reg_2011 + scale(cpi) + scale(tas_hom) + scale(indg_pob_2010) + (1 + federalista | pais), data = datos, na.action = na.omit)
```

## Por federalista + elecciones: no
 * Failed to converge with 2 negatives eigenvalues
 * boundary (singular) fit

```{r}
# modelo <- lmerTest::lmer(Voz ~ Sexo + Edad + Educacion + Ingreso + Etnia + Religion + Votar + Simpatizar_partido + Corrupción + scale(gdp_growth) + scale(gdp_pc_ppp) + log_pob + scale(cpi) + scale(tas_hom) + scale(indg_pob_2010) + (1 + federalista + ele_nac_2011 + ele_reg_2011 | pais), data = datos, na.action = na.omit)
```

## Total sem país
 * Failed to converge with 1 negative eigenvalue
 * boundary (singular) fit

```{r}
modelo <- lmerTest::lmer(Voz ~ Sexo + Edad + Educacion + Ingreso + Etnia + Religion + Votar + Simpatizar_partido + Corrupción + (1 + scale(gdp_growth) + scale(gdp_pc_ppp) + log_pob + ele_nac_2011 + ele_reg_2011 + federalista + scale(cpi) + scale(tas_hom) + scale(indg_pob_2010) | pais), data = datos, na.action = na.omit)
```

```{r}
summary(modelo)
ranef(modelo)
```

## Total sin país, gdp_growth fijo

```{r}
modelo <- lmerTest::lmer(Voz ~ Sexo + Edad + Educacion + Ingreso + Etnia + Religion + Votar + Simpatizar_partido + Corrupción + scale(gdp_growth) + (1 + scale(gdp_pc_ppp) + log_pob + ele_nac_2011 + ele_reg_2011 + federalista + scale(cpi) + scale(tas_hom) + scale(indg_pob_2010) | pais), data = datos, na.action = na.omit)
```

```{r}
summary(modelo)
```

```{r}
ranef(modelo)
```

## Total sin país, gdp_growth y log_pob fijos

* boundary (singular) fit
* Model failed tro converge with 2 negative eigenvalues

```{r}
modelo <- lmerTest::lmer(Voz ~ Sexo + Edad + Educacion + Ingreso + Etnia + Religion + Votar + Simpatizar_partido + Corrupción + scale(gdp_growth) + log_pob + (1 + scale(gdp_pc_ppp) + ele_nac_2011 + ele_reg_2011 + federalista + scale(cpi) + scale(tas_hom) + scale(indg_pob_2010) | pais), data = datos, na.action = na.omit)
```

```{r}
summary(modelo)
```