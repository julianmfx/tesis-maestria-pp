head(datos_compl)

nrow(datos_compl)
rm(df)

random_test <- function(data, prop) {
  df <- data |> 
    slice_sample(prop = prop) # generates a random sample base on the 
  random <- principal(df, nfactors = 4, rotate = "oblimin", scores = TRUE, cor = "poly", method = "regression") # calculate the components
 return(print.psych(random, cut = 0.3, sort = TRUE)) # print de componentes
  # a <- round(random$values,1)
  # a <- round(random$rms, 3)
}


random_test(data = datos_compl, prop = 0.5)

sims <- replicate(1, random_test(data = datos_compl, prop = 0.5))
sims

cfa_random <- function(df, prop) {
  df <- df |> 
    slice_sample(prop = prop)
  modelo <- sem(model = CFA, data = df)
  # summary(modelo, fit.measures = TRUE, standardized = TRUE)
  # semPaths(modelo, "std", edge.label.cex = 1.2, fade = FALSE)
  return(fitMeasures(modelo, c("tli")))
}

cfa_random(df = lapop12, prop = 0.5)


sims <- replicate(1000, cfa_random(df = lapop12, prop = 0.5))
prop.table(table(round(sims, 2)))

######
Cálculo del ACP realizado en submuestras de datos señalan:
  100 cálculos por proporción
(20% - en algunos casos la replicación del ACP en muestras del conjunto de datos generadas de manera aleatoria apuntaban a una correlación baja entre protesta y asociaciones laborales, comunitaria y partidos políticos, organización comunitaria y protesta, negativo en escuela con protesta, cabildo con partidista, cabildo sin efecto)
(33% - protesta y asociaciones laborales, comunitaria y partidos)
(50% - protesta y asociaciones laborales, baja correlación y baja probabilidad, sin cabildo)
(80% - sin cabildo)



El modelo confirmatorio pudo ser replicado en mitad de la muestra generada de manera aleatoria
CFA – pudo ser replicada en mitad de la muestra
(10% => 30% del CFI fue de 0.91)
(50% =>  0.9  0.91  0.92  0.93 
  0.008 0.397 0.576 0.019), 58% fue de 0.92

TLI
(50% =>  0.88  0.89   0.9  0.91 
  0.034 0.470 0.468 0.028) 47% 0.89 y 47% 0.9

Otro punto importante es el cálculo a partir de correlaciones policlóricas para componentes con menos de 4 ítems. Todos los componentes se mantuvieron en el análisis e, incluso, sus coeficientes aumentaron, lo que facilita determinar la correspondencia entre  variables y componentes.
   reuniones de partidos está con comunitaria pero es más bien ubicado en partidista debidoa  su alto valor allá 

   
   ----
     
     elo con la muestra completa, con algunas observaciones interesantes.
   El cálculo del ACP realizado en submuestras señala que, en algunos casos, hay coeficientes de patrón bajos, pero relevantes respecto a (i) reuniones laborales y el tipo de protesta, (ii) asistir a reuniones de partidos políticos y el tipo de participación comunitaria, (iii) organización comunitaria y el tipo de protesta, (iv) reuniones escolares y religiosas con coeficiente negativo en protesta, (v) asistir a cabil con el tipo partidista y (iv) cabildo con un coeficiente de patrón abajo de 0.3. Es importante decir que según el tamaño de la muestra incremente, esos coeficientes pierden relevancia. El único que se mantiene es la variación del coeficiente de asistir a cabildo, debido a que varía entre el valor límite de 0.3.
   Otro punto importante es el cálculo a partir de correlaciones policlóricas para componentes con menos de 4 ítems. Todos los componentes se mantuvieron en el análisis e, incluso, sus coeficientes aumentaron, sobre todo los identificados por correlaciones de pearson. Eso ayuda a determinar la correspondencia entre  variables y componentes, puesto que la existencia de coeficientes un arriba de 0.3 en dos tipos es sobrepasada por ese mismo coeficiente con un promedio de 0.2 puntos más en otro tipo.


