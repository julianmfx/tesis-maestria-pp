---
  title: "c16_voz_logit"
author: "jmf"
date: "`r Sys.Date()`"
output:   
  html_document:
  toc: TRUE
toc_float: TRUE
theme: cosmo
highlight: zenburn
---
  

# Librerías

```{r, message = F}
library(dfoptim)
library(minqa)
library(optimx)
library(corrplot)
library(sjstats)
library(performance)
library(extrafont)
font_import(prompt = F, pattern = "Arial") # fc-list : family | sort | uniq
loadfonts(device = "postscript")
```

## Seleccionando variables 

```{r}
df_logit <-  na.omit(
  df_logit[ , c("Voz", "Sexo", "Sexo_fact",
                "Edad", "scale_Edad", 
                "Educacion", "scale_Educacion", "Ed_fact",
                "Ingreso", "scale_Ingreso", "Ingreso_fact",
                "Catolico", "Protestante", "Sin_relig", "Evan_y_Pent", "Otra_relig",
                "Religion_fact", 
                "Blanca", "Mestiza", "Indigena", "Negra", "Mulata", "Otra_etn",
                "Etnia_fact",
                "Simpatizar_partido", "SP_fact",
                "ind_confianza", "scale_ind_confianza",
                "Votar", "Votar_fact",
                "gdp_growth", "scale_gdp_growth",
                "gdp_pc_ppp", "scale_gdp_pc_ppp",
                "log_pob", "scale_log_pob",
                "pp_urb_2010", "scale_pp_urb_2010",
                "pp_electoral", "scale_pp_electoral",
                "cpi", "scale_cpi",
                "tas_hom", "scale_tas_hom",
                "indg_pob_2010", "scale_indg_pob_2010",
                "ele_2011", "ele_2011_fact",
                "v_oblg", "v_oblg_fact",
                "v_oblg_san", "v_oblg_san_fact", 
                "federalista", "federalista_fact",
                "pais", "weight1500")])
```

```{r}
glimpse(df_logit)
```

```{r}
table(is.na(df_logit))
```

# Modelo nulo

```{r}
null_voz <- lme4::glmer(
  formula = Voz ~ 1 + (1|pais),
  family = binomial(link = "logit"),
  control = glmerControl(optimizer = "bobyqa",
                         optCtrl = list(maxfun = 4e5)),
  data = df_logit,
  verbose = 1,
  weights = weight1500,
  na.action = na.omit)
```

```{r}
summary(null_voz)
```

```{r}
VarCorr(null_voz)
```

```{r}
# intercept_variance / intercerpt_variance + ((pi^2) / 3)
0.2775 / (0.2775 + (pi^2)/3)
```

```{r}
quantile(residuals(null_voz, "pearson", scaled = TRUE))
```


```{r}
var(residuals(null_voz, type = "pearson"))
sd(residuals(null_voz, type = "pearson"))
```

```{r}
performance::icc(null_voz)
```

```{r}
exp(-1.0422)
tab_model(null_voz)
```


# Modelo completo

bobyqa - Convergió con maxfun = 3e5

## Testando indicadoras (dummies)

Sobre valores de ajuste: no cambia nada

```{r}
m <- lme4::glmer(
  formula = Voz ~ Sexo + scale_Edad + scale_Educacion + scale_Ingreso +
    Catolico + Protestante + Sin_relig + Evan_y_Pent + Otra_relig + 
    Blanca + Mestiza + Indigena + Negra + Mulata + Otra_etn +
    Simpatizar_partido + scale_ind_confianza + Votar +
    scale_gdp_growth +  scale_gdp_pc_ppp + scale_log_pob + scale_pp_urb_2010 +
    scale_pp_electoral + scale_cpi + scale_tas_hom + scale_indg_pob_2010 +
    ele_2011 + v_oblg + v_oblg_san + federalista + (1|pais),
  family = binomial(link = "logit"),
  control = glmerControl(optimizer = "bobyqa",
                         optCtrl = list(maxfun = 4e5)),
  data = df_logit,
  verbose = 1,
  weights = weight1500,
  na.action = na.omit)

m_oblg <- lme4::glmer(
  formula = Voz ~ Sexo + scale_Edad + scale_Educacion + scale_Ingreso +
    Catolico + Protestante + Sin_relig + Evan_y_Pent + Otra_relig + 
    Blanca + Mestiza + Indigena + Negra + Mulata + Otra_etn +
    Simpatizar_partido + scale_ind_confianza + Votar +
    scale_gdp_growth +  scale_gdp_pc_ppp + scale_log_pob + scale_pp_urb_2010 +
    scale_pp_electoral + scale_cpi + scale_tas_hom + scale_indg_pob_2010 +
    v_oblg + (1|pais),
  family = binomial(link = "logit"),
  control = glmerControl(optimizer = "bobyqa",
                         optCtrl = list(maxfun = 4e5)),
  data = df_logit,
  verbose = 1,
  weights = weight1500,
  na.action = na.omit)

m_oblg_san <- lme4::glmer(
  formula = Voz ~ Sexo + scale_Edad + scale_Educacion + scale_Ingreso +
    Catolico + Protestante + Sin_relig + Evan_y_Pent + Otra_relig + 
    Blanca + Mestiza + Indigena + Negra + Mulata + Otra_etn +
    Simpatizar_partido + scale_ind_confianza + Votar +
    scale_gdp_growth +  scale_gdp_pc_ppp + scale_log_pob + scale_pp_urb_2010 +
    scale_pp_electoral + scale_cpi + scale_tas_hom + scale_indg_pob_2010 +
    v_oblg_san + (1|pais),
  family = binomial(link = "logit"),
  control = glmerControl(optimizer = "bobyqa",
                         optCtrl = list(maxfun = 4e5)),
  data = df_logit,
  verbose = 1,
  weights = weight1500,
  na.action = na.omit)

m_ele_2011 <- lme4::glmer(
  formula = Voz ~ Sexo + scale_Edad + scale_Educacion + scale_Ingreso +
    Catolico + Protestante + Sin_relig + Evan_y_Pent + Otra_relig + 
    Blanca + Mestiza + Indigena + Negra + Mulata + Otra_etn +
    Simpatizar_partido + scale_ind_confianza + Votar +
    scale_gdp_growth +  scale_gdp_pc_ppp + scale_log_pob + scale_pp_urb_2010 +
    scale_pp_electoral + scale_cpi + scale_tas_hom + scale_indg_pob_2010 +
    ele_2011 + (1|pais),
  family = binomial(link = "logit"),
  control = glmerControl(optimizer = "bobyqa",
                         optCtrl = list(maxfun = 4e5)),
  data = df_logit,
  verbose = 1,
  weights = weight1500,
  na.action = na.omit)

m_federalista <- lme4::glmer(
  formula = Voz ~ Sexo + scale_Edad + scale_Educacion + scale_Ingreso +
    Catolico + Protestante + Sin_relig + Evan_y_Pent + Otra_relig + 
    Blanca + Mestiza + Indigena + Negra + Mulata + Otra_etn +
    Simpatizar_partido + scale_ind_confianza + Votar +
    scale_gdp_growth +  scale_gdp_pc_ppp + scale_log_pob + scale_pp_urb_2010 +
    scale_pp_electoral + scale_cpi + scale_tas_hom + scale_indg_pob_2010 +
    federalista + (1|pais),
  family = binomial(link = "logit"),
  control = glmerControl(optimizer = "bobyqa",
                         optCtrl = list(maxfun = 4e5)),
  data = df_logit,
  verbose = 1,
  weights = weight1500,
  na.action = na.omit)

anova(m, m_oblg, m_oblg_san, m_ele_2011, m_federalista)
```

### Resultado

* ele_211 y v_oblg son los mejores para voz

```{r}
summary(m)
summary(m_oblg)
summary(m_oblg_san)
summary(m_ele_2011)
summary(m_federalista)
```


```{r}
summary(m_bobyqa)
```


```{r}
VarCorr(m_bobyqa)
icc(m_bobyqa)
```


## Testando variables individuales

* Educación como factor, religión e indígena son importantes

```{r}
m_sr <- lme4::glmer(
  formula = Voz ~ Sexo + scale_Edad + scale_Educacion + scale_Ingreso +
    Blanca + Mestiza + Indigena + Negra + Mulata + Otra_etn +
    Simpatizar_partido + scale_ind_confianza + Votar +
    scale_gdp_growth +  scale_gdp_pc_ppp + scale_log_pob + scale_pp_urb_2010 +
    scale_pp_electoral + scale_cpi + scale_tas_hom + scale_indg_pob_2010 +
    v_oblg_san + (1|pais),
  family = binomial(link = "logit"),
  control = glmerControl(optimizer = "bobyqa",
                         optCtrl = list(maxfun = 4e5)),
  data = df_logit,
  verbose = 1,
  weights = weight1500,
  na.action = na.omit)

m_se <- lme4::glmer(
  formula = Voz ~ Sexo + scale_Edad + scale_Educacion + scale_Ingreso +
    Catolico + Protestante + Sin_relig + Evan_y_Pent + Otra_relig + 
    Simpatizar_partido + scale_ind_confianza + Votar +
    scale_gdp_growth +  scale_gdp_pc_ppp + scale_log_pob + scale_pp_urb_2010 +
    scale_pp_electoral + scale_cpi + scale_tas_hom + scale_indg_pob_2010 +
    v_oblg_san + (1|pais),
  family = binomial(link = "logit"),
  control = glmerControl(optimizer = "bobyqa",
                         optCtrl = list(maxfun = 4e5)),
  data = df_logit,
  verbose = 1,
  weights = weight1500,
  na.action = na.omit)

m_indg <- lme4::glmer(
  formula = Voz ~ Sexo + scale_Edad + scale_Educacion + scale_Ingreso +
    Indigena +
    Simpatizar_partido + scale_ind_confianza + Votar +
    scale_gdp_growth +  scale_gdp_pc_ppp + scale_log_pob + scale_pp_urb_2010 +
    scale_pp_electoral + scale_cpi + scale_tas_hom + scale_indg_pob_2010 +
    v_oblg_san + (1|pais),
  family = binomial(link = "logit"),
  control = glmerControl(optimizer = "bobyqa",
                         optCtrl = list(maxfun = 4e5)),
  data = df_logit,
  verbose = 1,
  weights = weight1500,
  na.action = na.omit)

m_indg.mas <- lme4::glmer(
  formula = Voz ~ Sexo + scale_Edad + scale_Educacion + scale_Ingreso +
   Mestiza + Indigena + Negra + Mulata +
    Simpatizar_partido + scale_ind_confianza + Votar +
    scale_gdp_growth +  scale_gdp_pc_ppp + scale_log_pob + scale_pp_urb_2010 +
    scale_pp_electoral + scale_cpi + scale_tas_hom + scale_indg_pob_2010 +
    v_oblg_san + (1|pais),
  family = binomial(link = "logit"),
  control = glmerControl(optimizer = "bobyqa",
                         optCtrl = list(maxfun = 4e5)),
  data = df_logit,
  verbose = 1,
  weights = weight1500,
  na.action = na.omit)

m_re_indg <- lme4::glmer(
  formula = Voz ~ Sexo + scale_Edad + scale_Educacion + scale_Ingreso +
    Catolico + Protestante + Sin_relig + Evan_y_Pent + Otra_relig + 
   Indigena +
    Simpatizar_partido + scale_ind_confianza + Votar +
    scale_gdp_growth +  scale_gdp_pc_ppp + scale_log_pob + scale_pp_urb_2010 +
    scale_pp_electoral + scale_cpi + scale_tas_hom + scale_indg_pob_2010 +
    v_oblg_san + (1|pais),
  family = binomial(link = "logit"),
  control = glmerControl(optimizer = "bobyqa",
                         optCtrl = list(maxfun = 4e5)),
  data = df_logit,
  verbose = 1,
  weights = weight1500,
  na.action = na.omit)

m_M.ed_f <- lme4::glmer(
  formula = Voz ~ Sexo + scale_Edad + Ed_fact + scale_Ingreso +
    Catolico + Protestante + Sin_relig + Evan_y_Pent + Otra_relig + 
    Blanca + Mestiza + Indigena + Negra + Mulata + Otra_etn +
    Simpatizar_partido + scale_ind_confianza + Votar +
    scale_gdp_growth +  scale_gdp_pc_ppp + 
    scale_tas_hom +
    v_oblg_san + (1|pais),
  family = binomial(link = "logit"),
  control = glmerControl(optimizer = "bobyqa",
                         optCtrl = list(maxfun = 4e5)),
  data = df_logit,
  verbose = 1,
  weights = weight1500,
  na.action = na.omit)

m_M.ing_f <- lme4::glmer(
  formula = Voz ~ Sexo + scale_Edad + scale_Educacion + Ingreso_fact +
    Catolico + Protestante + Sin_relig + Evan_y_Pent + Otra_relig + 
    Blanca + Mestiza + Indigena + Negra + Mulata + Otra_etn +
    Simpatizar_partido + scale_ind_confianza + Votar +
    scale_gdp_growth +  scale_gdp_pc_ppp + 
    scale_tas_hom +
    v_oblg_san + (1|pais),
  family = binomial(link = "logit"),
  control = glmerControl(optimizer = "bobyqa",
                         optCtrl = list(maxfun = 4e5)),
  data = df_logit,
  verbose = 1,
  weights = weight1500,
  na.action = na.omit)

anova(m_sr, m_se, m_indg, m_indg.mas, m_re_indg, m_M.ed_f, m_M.ing_f)
```

### Resultado

* Religión, indígena y educación como factor son importantes

```{r}
summary(m_se)
summary(m_sr)
summary(m_re_indg)
summary(m_indg)
summary(m_indg.mas)
summary(m_M.ed_f)
summary(m_M.ing_f)
```

## Testando variables nacionales continuas

* Tamaño y población indígena parecen los mejores

```{r}
m_S.logpob <- lme4::glmer(
  formula = Voz ~ Sexo + scale_Edad + scale_Educacion + scale_Ingreso +
    Catolico + Protestante + Sin_relig + Evan_y_Pent + Otra_relig + 
    Blanca + Mestiza + Indigena + Negra + Mulata + Otra_etn +
    Simpatizar_partido + scale_ind_confianza + Votar +
    scale_gdp_growth +  scale_gdp_pc_ppp + scale_pp_urb_2010 +
    scale_pp_electoral + scale_cpi + scale_tas_hom + scale_indg_pob_2010 +
    v_oblg_san + (1|pais),
  family = binomial(link = "logit"),
  control = glmerControl(optimizer = "bobyqa",
                         optCtrl = list(maxfun = 4e5)),
  data = df_logit,
  verbose = 1,
  weights = weight1500,
  na.action = na.omit)

m_S.ppurb <- lme4::glmer(
  formula = Voz ~ Sexo + scale_Edad + scale_Educacion + scale_Ingreso +
    Catolico + Protestante + Sin_relig + Evan_y_Pent + Otra_relig + 
    Blanca + Mestiza + Indigena + Negra + Mulata + Otra_etn +
    Simpatizar_partido + scale_ind_confianza + Votar +
    scale_gdp_growth +  scale_gdp_pc_ppp + scale_log_pob +
    scale_pp_electoral + scale_cpi + scale_tas_hom + scale_indg_pob_2010 +
    v_oblg_san + (1|pais),
  family = binomial(link = "logit"),
  control = glmerControl(optimizer = "bobyqa",
                         optCtrl = list(maxfun = 4e5)),
  data = df_logit,
  verbose = 1,
  weights = weight1500,
  na.action = na.omit)

m_S.ppelectoral <- lme4::glmer(
  formula = Voz ~ Sexo + scale_Edad + scale_Educacion + scale_Ingreso +
    Catolico + Protestante + Sin_relig + Evan_y_Pent + Otra_relig + 
    Blanca + Mestiza + Indigena + Negra + Mulata + Otra_etn +
    Simpatizar_partido + scale_ind_confianza + Votar +
    scale_gdp_growth +  scale_gdp_pc_ppp + scale_log_pob + scale_pp_urb_2010 +
    scale_cpi + scale_tas_hom + scale_indg_pob_2010 +
    v_oblg_san + (1|pais),
  family = binomial(link = "logit"),
  control = glmerControl(optimizer = "bobyqa",
                         optCtrl = list(maxfun = 4e5)),
  data = df_logit,
  verbose = 1,
  weights = weight1500,
  na.action = na.omit)

m_S.cpi <- lme4::glmer(
  formula = Voz ~ Sexo + scale_Edad + scale_Educacion + scale_Ingreso +
    Catolico + Protestante + Sin_relig + Evan_y_Pent + Otra_relig + 
    Blanca + Mestiza + Indigena + Negra + Mulata + Otra_etn +
    Simpatizar_partido + scale_ind_confianza + Votar +
    scale_gdp_growth +  scale_gdp_pc_ppp + scale_log_pob + scale_pp_urb_2010 +
    scale_pp_electoral + scale_tas_hom + scale_indg_pob_2010 +
    v_oblg_san + (1|pais),
  family = binomial(link = "logit"),
  control = glmerControl(optimizer = "bobyqa",
                         optCtrl = list(maxfun = 4e5)),
  data = df_logit,
  verbose = 1,
  weights = weight1500,
  na.action = na.omit)

m_S <- lme4::glmer(
  formula = Voz ~ Sexo + scale_Edad + scale_Educacion + scale_Ingreso +
    Catolico + Protestante + Sin_relig + Evan_y_Pent + Otra_relig + 
    Blanca + Mestiza + Indigena + Negra + Mulata + Otra_etn +
    Simpatizar_partido + scale_ind_confianza + Votar +
    scale_gdp_growth +  scale_gdp_pc_ppp + 
    scale_tas_hom + scale_indg_pob_2010 +
    v_oblg_san + (1|pais),
  family = binomial(link = "logit"),
  control = glmerControl(optimizer = "bobyqa",
                         optCtrl = list(maxfun = 4e5)),
  data = df_logit,
  verbose = 1,
  weights = weight1500,
  na.action = na.omit)

m_M.tamano <- lme4::glmer(
  formula = Voz ~ Sexo + scale_Edad + scale_Educacion + scale_Ingreso +
    Catolico + Protestante + Sin_relig + Evan_y_Pent + Otra_relig + 
    Blanca + Mestiza + Indigena + Negra + Mulata + Otra_etn +
    Simpatizar_partido + scale_ind_confianza + Votar +
    Tamano + 
    scale_gdp_growth +  scale_gdp_pc_ppp + 
    scale_tas_hom + scale_indg_pob_2010 +
    v_oblg_san + (1|pais),
  family = binomial(link = "logit"),
  control = glmerControl(optimizer = "bobyqa",
                         optCtrl = list(maxfun = 4e5)),
  data = df_logit,
  verbose = 1,
  weights = weight1500,
  na.action = na.omit)

m_M.tamano_F <- lme4::glmer(
  formula = Voz ~ Sexo + scale_Edad + scale_Educacion + scale_Ingreso +
    Catolico + Protestante + Sin_relig + Evan_y_Pent + Otra_relig + 
    Blanca + Mestiza + Indigena + Negra + Mulata + Otra_etn +
    Simpatizar_partido + scale_ind_confianza + Votar +
    Tamano_fact + 
    scale_gdp_growth +  scale_gdp_pc_ppp + 
    scale_tas_hom + scale_indg_pob_2010 +
    v_oblg_san + (1|pais),
  family = binomial(link = "logit"),
  control = glmerControl(optimizer = "bobyqa",
                         optCtrl = list(maxfun = 4e5)),
  data = df_logit,
  verbose = 1,
  weights = weight1500,
  na.action = na.omit)

m_S.indg_pob <- lme4::glmer(
  formula = Voz ~ Sexo + scale_Edad + scale_Educacion + scale_Ingreso +
    Catolico + Protestante + Sin_relig + Evan_y_Pent + Otra_relig + 
    Blanca + Mestiza + Indigena + Negra + Mulata + Otra_etn +
    Simpatizar_partido + scale_ind_confianza + Votar +
    scale_gdp_growth +  scale_gdp_pc_ppp + 
    scale_tas_hom +
    v_oblg_san + (1|pais),
  family = binomial(link = "logit"),
  control = glmerControl(optimizer = "bobyqa",
                         optCtrl = list(maxfun = 4e5)),
  data = df_logit,
  verbose = 1,
  weights = weight1500,
  na.action = na.omit)

anova(m_S.logpob, m_S.ppurb, m_S.ppelectoral, m_S.cpi, m_M.tamano, m_M.tamano_F, m_S.indg_pob, m_S)
```


### Resultado

* cpi, ppurb, ppelectoral parecen buenos
* tamano es mejor que no sea factor

```{r}
summary(m_S)
summary(m_S.logpob)
summary(m_S.ppurb)
summary(m_S.ppelectoral)
summary(m_S.cpi)
summary(m_S.indg_pob)
summary(m_M.tamano)
summary(m_M.tamano_F)
```

# Modelo seleccionado

```{r}
m <- lme4::glmer(
  formula = Voz ~ Sexo_fact + scale_Edad + Ed_fact + Ingreso_fact +
    Religion_fact + Etnia_fact +
    SP_fact + scale_ind_confianza + Votar_fact +
    scale_gdp_growth +  scale_gdp_pc_ppp + scale_log_pob + scale_pp_urb_2010 +
    scale_pp_electoral + scale_cpi + scale_tas_hom + scale_indg_pob_2010 +
    v_oblg_san_fact + (1|pais),
  family = binomial(link = "logit"),
  control = glmerControl(optimizer = "bobyqa",
                         optCtrl = list(maxfun = 4e5)),
  data = df_logit,
  verbose = 1,
  weights = weight1500,
  na.action = na.omit)
```

## PCP

```{r}
pred <- predict(object = m, type = "response")
pred[pred < 0.5] <- 0
pred[pred >= 0.5] <- 1
table(pred)
table(df_logit$Voz)
table(pred, df_logit$Voz)
```

```{r}
20103 + 105
14939 + 5269
(14879 + 45) / 20208 # 0.7385194
(5224 + 60) / 20208
45 / 20208
20103 / 20208
```

```{r}
summary(m)
```

```{r}
# tab_model(m, file = "voz_logit.html") # exportar tabla del modelo
```

# Plotar

## Predicciones

Al hacer la predicción con ggpredicto se generaban muchos valores de la variable dependiente (el tipo de participación política). Esto ocurre porque para cada nivel de las variables `educación`, `simpatizar por partido política`, `etnia`, `votar`.

Al utilizar esos valores para hacer la predicción la variable dependiente (**y**) tenía muchos valores y el gráfico se veía mal. Eso ocurría porque intentaba utilizar muchas variables para hacer un gráfico, siendo que el método que estaba utilizando servía para utilizar pocas variables (1 o 2).

Al cambiar el método de generación de datos a través de `summarise` y `mean`, pude obtener los valores que quería y, así, tener el gráfico que quiero presentar en el tesis.

```{r}
tab_model(m)
```

```{r}
ggeffects::ggpredict(model = m, terms = c("Ed_fact [all]",
                                          "SP_fact [all]",
                                          "Etnia_fact [all]",
                                          "Votar_fact [all]"))|>
  
  plot()
```

```{r}
pred.val <- ggpredict(m, terms = c("Ed_fact [all]",
                                   "SP_fact [all]",
                                   "Etnia_fact [all]",
                                   "Votar_fact [all]"), 
                      type = "fixed") |> 
  as_tibble() |> 
  dplyr::rename(Ed_fact = x, Voz = predicted, SP_fact = group, Etnia_fact = facet, Votar_fact = panel)

pred.val
```

```{r}
levels(pred.val$Etnia_fact)
levels(pred.val$Etnia_fact) <- list("Blanca" = "Blanca",
                                    "Mestiza" = "Mestiza",
                                    "Indigena" =  "Indigena",
                                    "Negra" = "Negra",
                                    "Mulata" = "Mulata",
                                    "Otra" = "Otra_etn")
levels(pred.val$Etnia_fact)
```


```{r}
pred.val$SP_fact <- factor(pred.val$SP_fact, levels = c("Sí", "No"))
pred.val$Etnia_fact <- factor(pred.val$Etnia_fact, levels = c("Blanca", "Mestiza", "Mulata", "Negra", "Otra", "Indigena"))
```

```{r}
prob_voz_educacion <- pred.val |> 
  summarise(Voz = mean(Voz), .by = c(Ed_fact, Votar_fact, SP_fact)) |>  # generación de puntos de datos de interés
  # dplyr::rename(Ed_fact = , Voz = predicted, SP_fact = group, Etnia_fact = facet, Votar_fact = panel)
  ggplot() + 
  geom_point(aes(y = Voz, x = Ed_fact, color = SP_fact, shape = SP_fact),
             size = 3,
             alpha = 0.66,
             stroke = 0.9) +
  facet_grid(~ Votar_fact) + 
  labs(title = "Probabilidad de participar del tipo Voz") +
  labs(subtitle = "¿Votó en las últimas elecciones?") + 
  xlab("Nivel Educativo") +
  ylab("Probabilidad de participar del tipo voz") + 
  labs(shape = "¿Simpatiza por \npartido político?") +
  labs(color = "¿Simpatiza por \npartido político?") +
  theme(
    strip.background = element_blank(),
    panel.background = element_blank(),
    panel.grid = element_blank(),
    plot.title = element_text(family = "Arial", size = (12)),
    plot.subtitle = element_text(family = "Arial", size = (11), hjust = 0.5),
    legend.title = element_text(family = "Arial", size = (11), hjust = 0.5),
    legend.position = "right",
    axis.ticks.y = element_blank(),
    axis.ticks.x = element_blank(),
    axis.text.x = element_text(family = "Arial", size = (9), hjust = 0.5),
    axis.text.y = element_text(family = "Arial", size = (9), hjust = 0.5))
```

```{r}
prob_voz_etnia <- pred.val |> 
  summarise(Voz = mean(Voz), .by = c(Etnia_fact, Votar_fact, SP_fact)) |>  # generación de puntos de datos de interés
  # dplyr::rename(Ed_fact = , Voz = predicted, SP_fact = group, Etnia_fact = facet, Votar_fact = panel)
  ggplot() + 
  geom_point(aes(y = Voz, x = Etnia_fact, color = SP_fact, shape = SP_fact),
             size = 3,
             alpha = 0.66,
             stroke = 0.9) +
  facet_grid(~ Votar_fact) + 
  labs(title = "Probabilidad de participar del tipo Voz") +
  labs(subtitle = "¿Votó en las últimas elecciones?") + 
  xlab("Etnia") +
  ylab("Probabilidad de participar del tipo voz") + 
  labs(shape = "¿Simpatiza por \npartido político?") +
  labs(color = "¿Simpatiza por \npartido político?") +
  theme(
    strip.background = element_blank(),
    panel.background = element_blank(),
    panel.grid = element_blank(),
    plot.title = element_text(family = "Arial", size = (12)),
    plot.subtitle = element_text(family = "Arial", size = (11), hjust = 0.5),
    legend.title = element_text(family = "Arial", size = (11), hjust = 0.5),
    legend.position = "right",
    axis.ticks.y = element_blank(),
    axis.ticks.x = element_blank(),
    axis.text.x = element_text(family = "Arial", size = (9), hjust = 0.5),
    axis.text.y = element_text(family = "Arial", size = (9), hjust = 0.5))
```

### Exportación

```{r}
plot <- cowplot::plot_grid(prob_voz_educacion, prob_voz_etnia, align = "hv", nrow = 2, ncol = 1)
ggsave(filename = "/home/julian/Documents/tesis-maestria_participacion-politica/illustrations/multinivel/probs/voz_ed_etnia.png", plot = plot, device = "png", dpi = 300, width = 20, height = 20, units = "cm")
# ggpubr::ggexport(plot, filename = "/home/julian/Documents/tesis-maestria_participacion-politica/illustrations/multinivel/probs/voz_educacion.jpeg", width = 2800, height = 2800, res = 300)
# ggpubr::ggexport(prob_voz_etnia, filename = "/home/julian/Documents/tesis-maestria_participacion-politica/illustrations/multinivel/probs/voz_etnia.jpeg", width = 2800, height = 2800, res = 300)
```

```{r}
# Plot these predicted effects
ggplot(pred.val) + 
  geom_line(aes(y = Voz, x = Ed_fact, group = SP_fact, color = Etnia_fact)) +
  facet_grid(~ Votar_fact) + 
  theme(legend.position="bottom")
```

```{r}
ggplot(pred.val, 
       aes(y = Voz, x = Ed_fact, color = SP_fact)) + 
  geom_point() +
  facet_grid(~ Votar_fact) + 
  # geom_errorbar(
    # aes(ymin = conf.low, ymax = conf.high),
    # position = position_dodge()
  # ) +
  # geom_ribbon(aes(ymin = conf.low, ymax = conf.high, alpha = 0.01)) +
  theme(legend.position="bottom")
```


```{r}
# v = c(0, 1)
ggeffects::ggpredict(model = m, terms = "v_oblg_san [n = 100]") |>   plot()
```


## Efectos aleatorios

```{r}
qqmath(ranef(m, condVar = TRUE))
```

  
## Correlaciones

```{r}
df2corr <- df_logit[ , !names(df_logit) %in% c("pais", "weight1500")]
corr1 <- cor(df2corr, method = "spearman")
corrplot(corr1, method = "number")
```
  

## Otras variables indicadoras

```{r}
m <- lme4::glmer(
  formula = Voz ~ Sexo_fact + scale_Edad + Ed_fact + Ingreso_fact +
    Religion_fact + Etnia_fact +
    SP_fact + scale_ind_confianza + Votar_fact +
    scale_gdp_growth +  scale_gdp_pc_ppp + scale_log_pob + scale_pp_urb_2010 +
    scale_pp_electoral + scale_cpi + scale_tas_hom + scale_indg_pob_2010 +
    v_oblg_san_fact + (1|pais),
  family = binomial(link = "logit"),
  control = glmerControl(optimizer = "bobyqa",
                         optCtrl = list(maxfun = 4e5)),
  data = df_logit,
  verbose = 1,
  weights = weight1500,
  na.action = na.omit)

m_v_oblg <- lme4::glmer(
  formula = Voz ~ Sexo_fact + scale_Edad + Ed_fact + Ingreso_fact +
    Religion_fact + Etnia_fact +
    SP_fact + scale_ind_confianza + Votar_fact +
    scale_gdp_growth +  scale_gdp_pc_ppp + scale_log_pob + scale_pp_urb_2010 +
    scale_pp_electoral + scale_cpi + scale_tas_hom + scale_indg_pob_2010 +
    v_oblg_fact + (1|pais),
  family = binomial(link = "logit"),
  control = glmerControl(optimizer = "bobyqa",
                         optCtrl = list(maxfun = 4e5)),
  data = df_logit,
  verbose = 1,
  weights = weight1500,
  na.action = na.omit)

m_ele_2011 <- lme4::glmer(
  formula = Voz ~ Sexo_fact + scale_Edad + Ed_fact + Ingreso_fact +
    Religion_fact + Etnia_fact +
    SP_fact + scale_ind_confianza + Votar_fact +
    scale_gdp_growth +  scale_gdp_pc_ppp + scale_log_pob + scale_pp_urb_2010 +
    scale_pp_electoral + scale_cpi + scale_tas_hom + scale_indg_pob_2010 +
    ele_2011_fact + (1|pais),
  family = binomial(link = "logit"),
  control = glmerControl(optimizer = "bobyqa",
                         optCtrl = list(maxfun = 4e5)),
  data = df_logit,
  verbose = 1,
  weights = weight1500,
  na.action = na.omit)
```

```{r}
summary(m)
summary(m_v_oblg)
summary(m_ele_2011)
```


```{r}
tab_model(m)
tab_model(m_v_oblg)
tab_model(m_ele_2011)
```



# Optimizadores

```{r}
# diff_optims <- allFit(m_bobyqa, maxfun = 3e5)
# diff_optims_OK <- diff_optims[sapply(diff_optims, is, "merMod")]
# lapply(diff_optims_OK, function(x) x@optinfo$conv$lme4$messages)
```


## Resultado

bobyqa y nlminbwrap SÍ convergieron

"
"$bobyqa
NULL

$Nelder_Mead
[1] "Model failed to converge with max|grad| = 0.00904414 (tol = 0.002, component 1)"

$nlminbwrap
NULL

$nmkbw
[1] "Model failed to converge with max|grad| = 0.0100247 (tol = 0.002, component 1)"

$`optimx.L-BFGS-B`
[1] "Model failed to converge with max|grad| = 0.0256062 (tol = 0.002, component 1)"

$nloptwrap.NLOPT_LN_NELDERMEAD
[1] "Model failed to converge with max|grad| = 0.00265416 (tol = 0.002, component 1)"

$nloptwrap.NLOPT_LN_BOBYQA
[1] "Model failed to converge with max|grad| = 0.0458471 (tol = 0.002, component 1)""
"

### Pequeño problema

1: In eval(expr, envir, enclos) : non-integer #successes in a binomial glm!

"To figure out what's going on here, as @aosmith comments above, y <- NS1*egg_total must be close to an integer (see binomial()$initialize for the test, which is (any(abs(y - round(y)) > 0.001)). If NS1 is supposed to be a proportion of eggs surviving, then this should work unless you made some kind of typo/rounding error somewhere.

This is something you want to figure out in order to make sure that the model actually makes sense; it's also possible that lme4 will behave badly with non-integer response variables for a distribution that's supposed to be integral."

### No funcionaron

Quizás por el bajo número de optimizaciones

#### nloptwrap

```{r}
m_nloptwrap <- lme4::glmer(
  formula = Voz ~ 1 + Sexo + Edad + Educacion + Ingreso +
    Catolico + Protestante + Sin_relig + Evan_y_Pent + Otra_relig + 
    Blanca + Mestiza + Indigena + Negra + Mulata + Otra_etn +
    Simpatizar_partido + ind_confianza + Votar +
    gdp_growth + scale(gdp_pc_ppp) + log_pob + pp_urb_2010 +
    pp_electoral + cpi + tas_hom + indg_pob_2010 +
    ele_2011 + v_oblg + v_oblg_san + federalista + (1|pais),
  family = binomial(link = "logit"),
  control = glmerControl(optimizer = "nloptwrap",
                         optCtrl = list(maxfun = 2e5)),
  data = df_logit,
  verbose = 0,
  weights = weight1500,
  na.action = na.omit)

summary(m_nloptwrap)
```


#### Nelder_Mead

```{r}
m_nelder_mead <- lme4::glmer(
  formula = Voz ~ 1 + Sexo + Edad + Educacion + Ingreso +
    Catolico + Protestante + Sin_relig + Evan_y_Pent + Otra_relig + 
    Blanca + Mestiza + Indigena + Negra + Mulata + Otra_etn +
    Simpatizar_partido + ind_confianza + Votar +
    scale_gdp_growth +  scale_gdp_pc_ppp + scale_log_pob + scale_pp_urb_2010 +
    scale_pp_electoral + scale_cpi + scale_tas_hom + scale_indg_pob_2010 +
    ele_2011 + v_oblg + v_oblg_san + federalista + (1|pais),
  family = binomial(link = "logit"),
  control = glmerControl(optimizer = "Nelder_Mead",
                         optCtrl = list(maxfun = 2e5)),
  data = df_logit,
  verbose = 0,
  weights = weight1500,
  na.action = na.omit)

summary(m_nelder_mead)
```


#### L-BFGS_b

```{r}
m_LBFGSB<- lme4::glmer(
  formula = Voz ~ 1 + Sexo + scale_Edad + scale_Educacion + scale_Ingreso +
    Catolico + Protestante + Sin_relig + Evan_y_Pent + Otra_relig + 
    Blanca + Mestiza + Indigena + Negra + Mulata + Otra_etn +
    Simpatizar_partido + scale_ind_confianza + Votar +
    scale_gdp_growth +  scale_gdp_pc_ppp + scale_log_pob + scale_pp_urb_2010 +
    scale_pp_electoral + scale_cpi + scale_tas_hom + scale_indg_pob_2010 +
    ele_2011 + v_oblg + v_oblg_san + federalista + (1|pais),
  family = binomial(link = "logit"),
  control = glmerControl(optimizer ='optimx', 
                         optCtrl = list(method = 'L-BFGS-B')),
  data = df_logit,
  verbose = 0,
  weights = weight1500,
  na.action = na.omit)

summary(m_LBFGSB)
```



#### nlminb

```{r}
m_nlminb <- lme4::glmer(
  formula = Voz ~ 1 + Sexo + scale_Edad + scale_Educacion + scale_Ingreso +
    Catolico + Protestante + Sin_relig + Evan_y_Pent + Otra_relig + 
    Blanca + Mestiza + Indigena + Negra + Mulata + Otra_etn +
    Simpatizar_partido + scale_ind_confianza + Votar +
    scale_gdp_growth +  scale_gdp_pc_ppp + scale_log_pob + scale_pp_urb_2010 +
    scale_pp_electoral + scale_cpi + scale_tas_hom + scale_indg_pob_2010 +
    ele_2011 + v_oblg + v_oblg_san + federalista + (1|pais),
  family = binomial(link = "logit"),
  control = glmerControl(optimizer ='optimx', 
                         optCtrl = list(method = 'nlminb')),
  data = df_logit,
  verbose = 0,
  weights = weight1500,
  na.action = na.omit)

summary(m_nlminb)
```





# Otros modelos

## Modelo sin escalar todas las variables: no converge

```{r}
m <- lme4::glmer(
  formula = Voz ~ Sexo + Edad + Educacion + Ingreso +
    Catolico + Protestante + Sin_relig + Evan_y_Pent + Otra_relig + 
    Blanca + Mestiza + Indigena + Negra + Mulata + Otra_etn +
    Simpatizar_partido + ind_confianza + Votar +
    gdp_growth +  scale_gdp_pc_ppp + log_pob + pp_urb_2010 +
    pp_electoral + cpi + tas_hom + indg_pob_2010 +
    ele_2011 + v_oblg + v_oblg_san + federalista + (1|pais),
  family = binomial(link = "logit"),
  control = glmerControl(optimizer = "bobyqa",
                         optCtrl = list(maxfun = 3e5)),
  data = df_logit,
  verbose = 0,
  weights = weight1500,
  na.action = na.omit)
summary(m)
```

## Extras

```{r}
m2_logit <- lme4::glmer(formula = Voz ~ 1 + Sexo + Edad + Educacion + Ingreso +
                          Catolico + Protestante + Sin_relig + Evan_y_Pent + Otra_relig + 
                          Blanca + Mestiza + Indigena + Negra + Mulata + Otra_etn +
                          Simpatizar_partido + ind_confianza + Votar +
                          scale(gdp_growth) + scale(gdp_pc_ppp) + log_pob + pp_urb_2010 +
                          pp_electoral + scale(cpi) + scale(tas_hom) + indg_pob_2010 +
                          ele_2011 + v_oblg + v_oblg_san + federalista + (1|pais),
                        family = binomial(link = "logit"),
                        data = df,
                        verbose = 0,
                        weights = weight1500, # Es esta. disminuye más residuales: peso para darle el mismo tamaño a cada país
                        na.action = na.omit)
```


```{r}
m3_logit <- lme4::glmer(formula = Voz ~ 1 + Sexo + Edad + Educacion + Ingreso +
                          Catolico + Protestante + Sin_relig + Evan_y_Pent + Otra_relig + 
                          Blanca + Mestiza + Indigena + Negra + Mulata + Otra_etn +
                          Simpatizar_partido + ind_confianza + Votar +
                          gdp_growth + gdp_pc_ppp + log_pob + pp_urb_2010 +
                          cpi + tas_hom + indg_pob_2010 +
                          v_oblg + federalista + (1|pais),
                        family = binomial(link = "logit"),
                        data = df_logit,
                        verbose = 0,
                        weights = weight1500, # Es esta. disminuye más residuales: peso para darle el mismo tamaño a cada país
                        na.action = na.omit)
```


```{r}
m3_probit <- lme4::glmer(Voz ~ 1 + Sexo + Edad + Educacion + Ingreso +
                          Catolico + Protestante + Sin_relig + Evan_y_Pent + Otra_relig + 
                          Blanca + Mestiza + Indigena + Negra + Mulata + Otra_etn +
                          Simpatizar_partido + ind_confianza + Votar +
                          gdp_growth + gdp_pc_ppp + log_pob + pp_urb_2010 +
                          cpi + tas_hom + indg_pob_2010 +
                          v_oblg + federalista + (1|pais),
                        family = binomial(link = "probit"),
                        data = df_logit,
                        verbose = 0,
                        weights = weight1500, # Es esta. disminuye más residuales: peso para darle el mismo tamaño a cada país
                        na.action = na.omit)
```


```{r}
m4_logit <- lme4::glmer(formula = Voz ~ 1 + Sexo + Edad + Educacion + Ingreso +
                          Catolico + Protestante + Sin_relig + Evan_y_Pent + Otra_relig + 
                          Blanca + Mestiza + Indigena + Negra + Mulata + Otra_etn +
                          Simpatizar_partido + ind_confianza + Votar +
                          scale(gdp_growth) + scale(gdp_pc_ppp) + log_pob + pp_urb_2010 +
                          pp_electoral + scale(cpi) + scale(tas_hom) + indg_pob_2010 +
                          ele_2011 + v_oblg + v_oblg_san + federalista + (1|pais),
                        family = binomial(link = "logit"),
                        control = glmerControl(optimizer = "bobyqa",
                                               optCtrl = list(maxfun = 2e5)),
                        data = df,
                        verbose = 0,
                        weights = weight1500, # Es esta. disminuye más residuales: peso para darle el mismo tamaño a cada país
                        na.action = na.omit)
```


```{r}
m5_logit <- lme4::glmer(formula = Voz ~ 1 + Sexo + Edad + Educacion + Ingreso +
                          Catolico + Protestante + Sin_relig + Evan_y_Pent + Otra_relig + 
                          Blanca + Mestiza + Indigena + Negra + Mulata + Otra_etn +
                          Simpatizar_partido + ind_confianza + Votar +
                          scale(gdp_growth) + scale(gdp_pc_ppp) + log_pob + scale(pp_urb_2010) +
                          scale(pp_electoral) + scale(cpi) + scale(tas_hom) + scale(indg_pob_2010) + 
                          (1|pais),
                        family = binomial(link = "logit"),
                        control = glmerControl(optimizer = "bobyqa",
                                               optCtrl = list(maxfun = 2e5)),
                        data = df,
                        verbose = 0,
                        weights = weight1500, # Es esta. disminuye más residuales: peso para darle el mismo tamaño a cada país
                        na.action = na.omit)
```

```{r}
anova(m1_logit, m2_logit, m3_logit, m4_logit, m5_logit)
```




