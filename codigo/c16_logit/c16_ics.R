library(ggplot2)
library(dplyr)
library(extrafont)

font_import()
loadfonts(device = "postscript")

ics <- read.csv(file = "illustrations/multinivel/ics2.csv")
ics <- as.data.frame(ics)

ics$variable[ics$variable == "Evangélica Pentecostal"] <- "Evangélica y Pentecostal"
ics$variable[ics$variable == "Población"] <- "Log de la población"

ics <- ics |> transform(variable = factor(
  variable, 
  levels = c("Hombre", "Edad", 
             "Educación Primaria", "Educación Secundaria",
             "Educación Universitaria", 
             "Ingreso Bajo/Mediano", "Ingreso Mediano/Alto",
             "Ingreso Alto", 
             "Protestante", "Sin religión",
             "Evangélica y Pentecostal", "Otra religión",
             "Etnia Mestiza", "Etnia Indigena",
             "Etnia Negra", "Etnia Mulata",
             "Otra Etnia", "Simpatizar por partido",
             "Indicador de confianza", "Votar",
             "PIB", "PIB ppc", "Log de la población",
             "Población urbana", "Participación electoral",
             "Corrupción", "Tasa de homicidios",
             "Población indígena", "Voto obligatorio c/ sanción")))
ics$variable <- forcats::fct_rev(ics$variable) # reverse order of factor levels

ics$variable


voz <- ics |> 
  dplyr::select(variable, voz, voz.min, voz.max) |> 
  ggplot(aes(x = voz, y = variable)) +
  geom_point() + 
  # geom_errorbar(aes(xmin = voz.min, xmax = voz.max)) +
  geom_linerange(aes(xmin = voz.min, xmax = voz.max), 
                 alpha = 1, linetype = "solid",
                 linewidth =  0.8) + 
  geom_vline(xintercept = 1, linetype = "dotted") +
  labs(title = "IC [95%] para el modelo Voz",
       x = "",
       y = "Variables") +
  theme(
    strip.background = element_blank(),
    panel.background = element_blank(),
    panel.grid = element_blank(),
    plot.title = element_text(family = "Arial", size = (8)),
    legend.title = element_blank(),
    legend.text = element_blank(),
    axis.title = element_text(family = "Arial", size = (8)),
    axis.text.y = element_text(family = "Arial", size = (8)))

comu <- ics |> 
  dplyr::select(variable, comu, comu.min, comu.max) |> 
  ggplot(aes(x = comu, y = variable)) +
  geom_point() + 
  # geom_errorbar(aes(xmin = comu_min, xmax = comu_max)) +
  geom_linerange(aes(xmin = comu.min, xmax = comu.max), 
                 alpha = 1, linetype = "solid",
                 linewidth = 0.8) + 
  geom_vline(xintercept = 1, linetype = "dotted") +
  labs(title = "IC [95%] para el modelo Comunitaria",
       x = "",
       y = "") +
  theme(
    strip.background = element_blank(),
    panel.background = element_blank(),
    panel.grid = element_blank(),
    plot.title = element_text(family = "Arial", size = (8)),
    legend.title = element_blank(),
    legend.text = element_blank(),
    axis.title = element_blank(),
    axis.text.y = element_blank(),
    axis.ticks.y = element_blank())


partd <- ics |> 
  dplyr::select(variable, partd, partd.min, partd.max) |> 
  ggplot(aes(x = partd, y = variable)) +
  geom_point() + 
  # geom_errorbar(aes(xmin = partd_min, xmax = partd_max)) +
  geom_linerange(aes(xmin = partd.min, xmax = partd.max), 
                 alpha = 1, linetype = "solid",
                 linewidth = 0.8) + 
  geom_vline(xintercept = 1, linetype = "dotted") +
  labs(title = "IC [95%] para el modelo Partidista",
       x = "",
       y = "Variables") +
  theme(
    strip.background = element_blank(),
    panel.background = element_blank(),
    panel.grid = element_blank(),
    plot.title = element_text(family = "Arial", size = (8)),
    legend.title = element_blank(),
    legend.text = element_blank(),
    axis.title = element_text(family = "Arial", size = (8)),
    axis.text.y = element_text(family = "Arial", size = (8)))

prot <- ics |>
  dplyr::select(variable, prot, prot.min, prot.max) |> 
  ggplot(aes(x = prot, y = variable)) +
  geom_point() + 
  # geom_errorbar(aes(xmin = prot_min, xmax = prot_max)) +
  geom_linerange(aes(xmin = prot.min, xmax = prot.max), 
                 alpha = 1, linetype = "solid",
                 linewidth = 0.8) + 
  geom_vline(xintercept = 1, linetype = "dotted") +
  labs(title = "IC [95%] para el modelo Protesta",
       x = "",
       y = "") +
  theme(
    strip.background = element_blank(),
    panel.background = element_blank(),
    panel.grid = element_blank(),
    plot.title = element_text(family = "Arial", size = (8)),
    legend.title = element_blank(),
    legend.text = element_blank(),
    axis.title = element_blank(),
    axis.text.y = element_blank(),
    axis.ticks.y = element_blank())
plot <- cowplot::plot_grid(voz, comu, partd, prot, align = "h")
plot1 <- cowplot::plot_grid(voz, comu, align = "h")
plot2 <- cowplot::plot_grid(partd, prot, align = "h")
ggsave(filename = "illustrations/multinivel/ics_plot.png", plot = plot, device = "png", dpi = 300, width = 17.25, height = 24, units = "cm")
ggsave(filename = "illustrations/multinivel/ics_plot1.png", plot = plot1, device = "png", dpi = 300, width = 17.25, height = 24, units = "cm")
ggsave(filename = "illustrations/multinivel/ics_plot2.png", plot = plot2, device = "png", dpi = 300, width = 17.25, height = 24, units = "cm")
