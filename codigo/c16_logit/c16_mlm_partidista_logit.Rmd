---
  title: "c16_partidista_logit"
author: "jmf"
date: "`r Sys.Date()`"
output:   
  html_document:
  toc: TRUE
toc_float: TRUE
theme: cosmo
highlight: zenburn
---
  

# Librerías

```{r, message = F}
library(dfoptim)
library(minqa)
library(optimx)
library(corrplot)
library(sjstats)
library(performance)
```

## Seleccionando variables

```{r}
df_logit <-  na.omit(
  df_logit[ , c("Partidista", "Sexo", "Sexo_fact",
                "Edad", "scale_Edad", 
                "Educacion", "scale_Educacion", "Ed_fact",
                "Ingreso", "scale_Ingreso", "Ingreso_fact",
                "Catolico", "Protestante", "Sin_relig", "Evan_y_Pent", "Otra_relig",
                "Religion_fact", 
                "Blanca", "Mestiza", "Indigena", "Negra", "Mulata", "Otra_etn",
                "Etnia_fact",
                "Simpatizar_partido", "SP_fact",
                "ind_confianza", "scale_ind_confianza",
                "Votar", "Votar_fact",
                "gdp_growth", "scale_gdp_growth",
                "gdp_pc_ppp", "scale_gdp_pc_ppp",
                "log_pob", "scale_log_pob",
                "pp_urb_2010", "scale_pp_urb_2010",
                "pp_electoral", "scale_pp_electoral",
                "cpi", "scale_cpi",
                "tas_hom", "scale_tas_hom",
                "indg_pob_2010", "scale_indg_pob_2010",
                "ele_2011", "ele_2011_fact",
                "v_oblg", "v_oblg_fact",
                "v_oblg_san", "v_oblg_san_fact", 
                "federalista", "federalista_fact",
                "pais", "weight1500")])
```

# Modelo nulo

```{r}
null_Partidista <- lme4::glmer(
  formula = Partidista ~ 1 + (1|pais),
  family = binomial(link = "logit"),
  control = glmerControl(optimizer = "bobyqa",
                         optCtrl = list(maxfun = 4e5)),
  data = df_logit,
  verbose = 1,
  weights = weight1500,
  na.action = na.omit)
```

```{r}
summary(null_Partidista)
```

```{r}
VarCorr(null_Partidista)
```

```{r}
# intercept_variance / intercerpt_variance + ((pi^2) / 3)
```

```{r}
quantile(residuals(null_Partidista, "pearson", scaled = TRUE))
```


```{r}
var(residuals(null_Partidista, type = "pearson"))
sd(residuals(null_Partidista, type = "pearson"))
```

```{r}
performance::icc(null_Partidista)
```

```{r}
exp(-1.23736)
tab_model(null_Partidista)
```



# Modelos

bobyqa - Convergió con maxfun = 3e5

## Testando variables nacionales de control (dummies)

Sobre valores de ajuste: no cambia casis nada.
* federalista es un poquito mejor

```{r}
m <- lme4::glmer(
  formula = Partidista ~ Sexo + scale_Edad + scale_Educacion + scale_Ingreso +
    Catolico + Protestante + Sin_relig + Evan_y_Pent + Otra_relig + 
    Blanca + Mestiza + Indigena + Negra + Mulata + Otra_etn +
    Simpatizar_partido + scale_ind_confianza + Votar +
    scale_gdp_growth +  scale_gdp_pc_ppp + scale_log_pob + scale_pp_urb_2010 +
    scale_pp_electoral + scale_cpi + scale_tas_hom + scale_indg_pob_2010 +
    ele_2011 + v_oblg + v_oblg_san + federalista + (1|pais),
  family = binomial(link = "logit"),
  control = glmerControl(optimizer = "bobyqa",
                         optCtrl = list(maxfun = 4e5)),
  data = df_logit,
  verbose = 1,
  weights = weight1500,
  na.action = na.omit)

m_oblg <- lme4::glmer(
  formula = Partidista ~ Sexo + scale_Edad + scale_Educacion + scale_Ingreso +
    Catolico + Protestante + Sin_relig + Evan_y_Pent + Otra_relig + 
    Blanca + Mestiza + Indigena + Negra + Mulata + Otra_etn +
    Simpatizar_partido + scale_ind_confianza + Votar +
    scale_gdp_growth +  scale_gdp_pc_ppp + scale_log_pob + scale_pp_urb_2010 +
    scale_pp_electoral + scale_cpi + scale_tas_hom + scale_indg_pob_2010 +
    v_oblg + (1|pais),
  family = binomial(link = "logit"),
  control = glmerControl(optimizer = "bobyqa",
                         optCtrl = list(maxfun = 4e5)),
  data = df_logit,
  verbose = 1,
  weights = weight1500,
  na.action = na.omit)

m_oblg_san <- lme4::glmer(
  formula = Partidista ~ Sexo + scale_Edad + scale_Educacion + scale_Ingreso +
    Catolico + Protestante + Sin_relig + Evan_y_Pent + Otra_relig + 
    Blanca + Mestiza + Indigena + Negra + Mulata + Otra_etn +
    Simpatizar_partido + scale_ind_confianza + Votar +
    scale_gdp_growth +  scale_gdp_pc_ppp + scale_log_pob + scale_pp_urb_2010 +
    scale_pp_electoral + scale_cpi + scale_tas_hom + scale_indg_pob_2010 +
    v_oblg_san + (1|pais),
  family = binomial(link = "logit"),
  control = glmerControl(optimizer = "bobyqa",
                         optCtrl = list(maxfun = 4e5)),
  data = df_logit,
  verbose = 1,
  weights = weight1500,
  na.action = na.omit)

m_ele_2011 <- lme4::glmer(
  formula = Partidista ~ Sexo + scale_Edad + scale_Educacion + scale_Ingreso +
    Catolico + Protestante + Sin_relig + Evan_y_Pent + Otra_relig + 
    Blanca + Mestiza + Indigena + Negra + Mulata + Otra_etn +
    Simpatizar_partido + scale_ind_confianza + Votar +
    scale_gdp_growth +  scale_gdp_pc_ppp + scale_log_pob + scale_pp_urb_2010 +
    scale_pp_electoral + scale_cpi + scale_tas_hom + scale_indg_pob_2010 +
    ele_2011 + (1|pais),
  family = binomial(link = "logit"),
  control = glmerControl(optimizer = "bobyqa",
                         optCtrl = list(maxfun = 4e5)),
  data = df_logit,
  verbose = 1,
  weights = weight1500,
  na.action = na.omit)

m_federalista <- lme4::glmer(
  formula = Partidista ~ Sexo + scale_Edad + scale_Educacion + scale_Ingreso +
    Catolico + Protestante + Sin_relig + Evan_y_Pent + Otra_relig + 
    Blanca + Mestiza + Indigena + Negra + Mulata + Otra_etn +
    Simpatizar_partido + scale_ind_confianza + Votar +
    scale_gdp_growth +  scale_gdp_pc_ppp + scale_log_pob + scale_pp_urb_2010 +
    scale_pp_electoral + scale_cpi + scale_tas_hom + scale_indg_pob_2010 +
    federalista + (1|pais),
  family = binomial(link = "logit"),
  control = glmerControl(optimizer = "bobyqa",
                         optCtrl = list(maxfun = 4e5)),
  data = df_logit,
  verbose = 1,
  weights = weight1500,
  na.action = na.omit)

anova(m, m_oblg, m_oblg_san, m_ele_2011, m_federalista)
```

### Resultado

Federalista funciona mejor para el tipo partidista

```{r}
summary(m)
summary(m_oblg)
summary(m_oblg_san)
summary(m_ele_2011)
summary(m_federalista)
```

```{r}
summary(m)
summary(m_federalista)
```
```{r}
margins <- ggeffects::ggpredict(model = m_federalista, terms = "federalista [all]")
plot(margins)
```


```{r}
summary(m_bobyqa)
```


```{r}
VarCorr(m_bobyqa)
icc(m_bobyqa)
```


## Testando variables individuales

Educación e Ingreso como factor mejoran el ajuste.
Hay que tener indígena

```{r}
m <- lme4::glmer(
  formula = Partidista ~ Sexo + scale_Edad + scale_Educacion + scale_Ingreso +
    Catolico + Protestante + Sin_relig + Evan_y_Pent + Otra_relig + 
    Blanca + Mestiza + Indigena + Negra + Mulata + Otra_etn +
    Simpatizar_partido + scale_ind_confianza + Votar +
    scale_gdp_growth +  scale_gdp_pc_ppp + scale_log_pob + scale_pp_urb_2010 +
    scale_pp_electoral + scale_cpi + scale_tas_hom + scale_indg_pob_2010 +
    v_oblg_san + (1|pais),
  family = binomial(link = "logit"),
  control = glmerControl(optimizer = "bobyqa",
                         optCtrl = list(maxfun = 4e5)),
  data = df_logit,
  verbose = 1,
  weights = weight1500,
  na.action = na.omit)

m_sr <- lme4::glmer(
  formula = Partidista ~ Sexo + scale_Edad + scale_Educacion + scale_Ingreso +
    Blanca + Mestiza + Indigena + Negra + Mulata + Otra_etn +
    Simpatizar_partido + scale_ind_confianza + Votar +
    scale_gdp_growth +  scale_gdp_pc_ppp + scale_log_pob + scale_pp_urb_2010 +
    scale_pp_electoral + scale_cpi + scale_tas_hom + scale_indg_pob_2010 +
    v_oblg_san + (1|pais),
  family = binomial(link = "logit"),
  control = glmerControl(optimizer = "bobyqa",
                         optCtrl = list(maxfun = 4e5)),
  data = df_logit,
  verbose = 1,
  weights = weight1500,
  na.action = na.omit)

m_se <- lme4::glmer(
  formula = Partidista ~ Sexo + scale_Edad + scale_Educacion + scale_Ingreso +
    Catolico + Protestante + Sin_relig + Evan_y_Pent + Otra_relig + 
    Simpatizar_partido + scale_ind_confianza + Votar +
    scale_gdp_growth +  scale_gdp_pc_ppp + scale_log_pob + scale_pp_urb_2010 +
    scale_pp_electoral + scale_cpi + scale_tas_hom + scale_indg_pob_2010 +
    v_oblg_san + (1|pais),
  family = binomial(link = "logit"),
  control = glmerControl(optimizer = "bobyqa",
                         optCtrl = list(maxfun = 4e5)),
  data = df_logit,
  verbose = 1,
  weights = weight1500,
  na.action = na.omit)

m_indg <- lme4::glmer(
  formula = Partidista ~ Sexo + scale_Edad + scale_Educacion + scale_Ingreso +
    Indigena +
    Simpatizar_partido + scale_ind_confianza + Votar +
    scale_gdp_growth +  scale_gdp_pc_ppp + scale_log_pob + scale_pp_urb_2010 +
    scale_pp_electoral + scale_cpi + scale_tas_hom + scale_indg_pob_2010 +
    v_oblg_san + (1|pais),
  family = binomial(link = "logit"),
  control = glmerControl(optimizer = "bobyqa",
                         optCtrl = list(maxfun = 4e5)),
  data = df_logit,
  verbose = 1,
  weights = weight1500,
  na.action = na.omit)
m_indg.mas <- lme4::glmer(
  formula = Partidista ~ Sexo + scale_Edad + scale_Educacion + scale_Ingreso +
   Mestiza + Indigena + Negra + Mulata +
    Simpatizar_partido + scale_ind_confianza + Votar +
    scale_gdp_growth +  scale_gdp_pc_ppp + scale_log_pob + scale_pp_urb_2010 +
    scale_pp_electoral + scale_cpi + scale_tas_hom + scale_indg_pob_2010 +
    v_oblg_san + (1|pais),
  family = binomial(link = "logit"),
  control = glmerControl(optimizer = "bobyqa",
                         optCtrl = list(maxfun = 4e5)),
  data = df_logit,
  verbose = 1,
  weights = weight1500,
  na.action = na.omit)

m_re_indg <- lme4::glmer(
  formula = Partidista ~ Sexo + scale_Edad + scale_Educacion + scale_Ingreso +
    Catolico + Protestante + Sin_relig + Evan_y_Pent + Otra_relig + 
   Indigena +
    Simpatizar_partido + scale_ind_confianza + Votar +
    scale_gdp_growth +  scale_gdp_pc_ppp + scale_log_pob + scale_pp_urb_2010 +
    scale_pp_electoral + scale_cpi + scale_tas_hom + scale_indg_pob_2010 +
    v_oblg_san + (1|pais),
  family = binomial(link = "logit"),
  control = glmerControl(optimizer = "bobyqa",
                         optCtrl = list(maxfun = 4e5)),
  data = df_logit,
  verbose = 1,
  weights = weight1500,
  na.action = na.omit)

m_M.ed_f <- lme4::glmer(
  formula = Partidista ~ Sexo + scale_Edad + Ed_fact + scale_Ingreso +
    Catolico + Protestante + Sin_relig + Evan_y_Pent + Otra_relig + 
    Blanca + Mestiza + Indigena + Negra + Mulata + Otra_etn +
    Simpatizar_partido + scale_ind_confianza + Votar +
    scale_gdp_growth +  scale_gdp_pc_ppp + 
    scale_tas_hom +
    v_oblg_san + (1|pais),
  family = binomial(link = "logit"),
  control = glmerControl(optimizer = "bobyqa",
                         optCtrl = list(maxfun = 4e5)),
  data = df_logit,
  verbose = 1,
  weights = weight1500,
  na.action = na.omit)

m_M.ing_f <- lme4::glmer(
  formula = Partidista ~ Sexo + scale_Edad + scale_Educacion + Ingreso_fact +
    Catolico + Protestante + Sin_relig + Evan_y_Pent + Otra_relig + 
    Blanca + Mestiza + Indigena + Negra + Mulata + Otra_etn +
    Simpatizar_partido + scale_ind_confianza + Votar +
    scale_gdp_growth +  scale_gdp_pc_ppp + 
    scale_tas_hom +
    v_oblg_san + (1|pais),
  family = binomial(link = "logit"),
  control = glmerControl(optimizer = "bobyqa",
                         optCtrl = list(maxfun = 4e5)),
  data = df_logit,
  verbose = 1,
  weights = weight1500,
  na.action = na.omit)

m_M.Tamano <- lme4::glmer(
  formula = Partidista ~ Sexo + scale_Edad + scale_Educacion + scale_Ingreso +
    Catolico + Protestante + Sin_relig + Evan_y_Pent + Otra_relig + 
    Blanca + Mestiza + Indigena + Negra + Mulata + Otra_etn +
    Simpatizar_partido + scale_ind_confianza + Votar +
    Tamano +
    scale_gdp_growth +  scale_gdp_pc_ppp + 
    scale_tas_hom +
    v_oblg_san + (1|pais),
  family = binomial(link = "logit"),
  control = glmerControl(optimizer = "bobyqa",
                         optCtrl = list(maxfun = 4e5)),
  data = df_logit,
  verbose = 1,
  weights = weight1500,
  na.action = na.omit)

m_M.Tamano_F <- lme4::glmer(
  formula = Partidista ~ Sexo + scale_Edad + scale_Educacion + scale_Ingreso +
    Catolico + Protestante + Sin_relig + Evan_y_Pent + Otra_relig + 
    Blanca + Mestiza + Indigena + Negra + Mulata + Otra_etn +
    Simpatizar_partido + scale_ind_confianza + Votar +
    Tamano_fact +
    scale_gdp_growth +  scale_gdp_pc_ppp + 
    scale_tas_hom +
    v_oblg_san + (1|pais),
  family = binomial(link = "logit"),
  control = glmerControl(optimizer = "bobyqa",
                         optCtrl = list(maxfun = 4e5)),
  data = df_logit,
  verbose = 1,
  weights = weight1500,
  na.action = na.omit)

anova(m, m_sr, m_se, m_indg, m_indg.mas, m_re_indg, m_M.ed_f, m_M.ing_f, m_M.Tamano, m_M.Tamano_F)
```


### Resultado

Mulata también es importante en el con indígenas y más algunos
Solo controlar por indigena, negra y multata parece mejor.

* Educacion e Ingreso con factor son significativos

```{r}
summary(m)
summary(m_se)
summary(m_sr)
summary(m_re_indg)
summary(m_indg)
summary(m_indg.mas)
summary(m_M.ed_f)
summary(m_M.ing_f)
```

## Testando variables nacionales continuas

Sacar cualquier una de ellas no cambia nada.
Modelos sin todas es lo mejorcito

```{r}
m_S.logpob <- lme4::glmer(
  formula = Partidista ~ Sexo + scale_Edad + scale_Educacion + scale_Ingreso +
    Catolico + Protestante + Sin_relig + Evan_y_Pent + Otra_relig + 
    Blanca + Mestiza + Indigena + Negra + Mulata + Otra_etn +
    Simpatizar_partido + scale_ind_confianza + Votar +
    scale_gdp_growth +  scale_gdp_pc_ppp + scale_pp_urb_2010 +
    scale_pp_electoral + scale_cpi + scale_tas_hom + scale_indg_pob_2010 +
    v_oblg_san + (1|pais),
  family = binomial(link = "logit"),
  control = glmerControl(optimizer = "bobyqa",
                         optCtrl = list(maxfun = 4e5)),
  data = df_logit,
  verbose = 1,
  weights = weight1500,
  na.action = na.omit)

m_S.ppurb <- lme4::glmer(
  formula = Partidista ~ Sexo + scale_Edad + scale_Educacion + scale_Ingreso +
    Catolico + Protestante + Sin_relig + Evan_y_Pent + Otra_relig + 
    Blanca + Mestiza + Indigena + Negra + Mulata + Otra_etn +
    Simpatizar_partido + scale_ind_confianza + Votar +
    scale_gdp_growth +  scale_gdp_pc_ppp + scale_log_pob +
    scale_pp_electoral + scale_cpi + scale_tas_hom + scale_indg_pob_2010 +
    v_oblg_san + (1|pais),
  family = binomial(link = "logit"),
  control = glmerControl(optimizer = "bobyqa",
                         optCtrl = list(maxfun = 4e5)),
  data = df_logit,
  verbose = 1,
  weights = weight1500,
  na.action = na.omit)

m_S.ppelectoral <- lme4::glmer(
  formula = Partidista ~ Sexo + scale_Edad + scale_Educacion + scale_Ingreso +
    Catolico + Protestante + Sin_relig + Evan_y_Pent + Otra_relig + 
    Blanca + Mestiza + Indigena + Negra + Mulata + Otra_etn +
    Simpatizar_partido + scale_ind_confianza + Votar +
    scale_gdp_growth +  scale_gdp_pc_ppp + scale_log_pob + scale_pp_urb_2010 +
    scale_cpi + scale_tas_hom + scale_indg_pob_2010 +
    v_oblg_san + (1|pais),
  family = binomial(link = "logit"),
  control = glmerControl(optimizer = "bobyqa",
                         optCtrl = list(maxfun = 4e5)),
  data = df_logit,
  verbose = 1,
  weights = weight1500,
  na.action = na.omit)

m_S.cpi <- lme4::glmer(
  formula = Partidista ~ Sexo + scale_Edad + scale_Educacion + scale_Ingreso +
    Catolico + Protestante + Sin_relig + Evan_y_Pent + Otra_relig + 
    Blanca + Mestiza + Indigena + Negra + Mulata + Otra_etn +
    Simpatizar_partido + scale_ind_confianza + Votar +
    scale_gdp_growth +  scale_gdp_pc_ppp + scale_log_pob + scale_pp_urb_2010 +
    scale_pp_electoral + scale_tas_hom + scale_indg_pob_2010 +
    v_oblg_san + (1|pais),
  family = binomial(link = "logit"),
  control = glmerControl(optimizer = "bobyqa",
                         optCtrl = list(maxfun = 4e5)),
  data = df_logit,
  verbose = 1,
  weights = weight1500,
  na.action = na.omit)

m_S <- lme4::glmer(
  formula = Partidista ~ Sexo + scale_Edad + scale_Educacion + scale_Ingreso +
    Catolico + Protestante + Sin_relig + Evan_y_Pent + Otra_relig + 
    Blanca + Mestiza + Indigena + Negra + Mulata + Otra_etn +
    Simpatizar_partido + scale_ind_confianza + Votar +
    scale_gdp_growth +  scale_gdp_pc_ppp + 
    scale_tas_hom + scale_indg_pob_2010 +
    v_oblg_san + (1|pais),
  family = binomial(link = "logit"),
  control = glmerControl(optimizer = "bobyqa",
                         optCtrl = list(maxfun = 4e5)),
  data = df_logit,
  verbose = 1,
  weights = weight1500,
  na.action = na.omit)

m_M.tamano <- lme4::glmer(
  formula = Partidista ~ Sexo + scale_Edad + scale_Educacion + scale_Ingreso +
    Catolico + Protestante + Sin_relig + Evan_y_Pent + Otra_relig + 
    Blanca + Mestiza + Indigena + Negra + Mulata + Otra_etn +
    Simpatizar_partido + scale_ind_confianza + Votar +
    Tamano + 
    scale_gdp_growth +  scale_gdp_pc_ppp + 
    scale_tas_hom + scale_indg_pob_2010 +
    v_oblg_san + (1|pais),
  family = binomial(link = "logit"),
  control = glmerControl(optimizer = "bobyqa",
                         optCtrl = list(maxfun = 4e5)),
  data = df_logit,
  verbose = 1,
  weights = weight1500,
  na.action = na.omit)

m_M.tamano_F <- lme4::glmer(
  formula = Partidista ~ Sexo + scale_Edad + scale_Educacion + scale_Ingreso +
    Catolico + Protestante + Sin_relig + Evan_y_Pent + Otra_relig + 
    Blanca + Mestiza + Indigena + Negra + Mulata + Otra_etn +
    Simpatizar_partido + scale_ind_confianza + Votar +
    Tamano_fact + 
    scale_gdp_growth +  scale_gdp_pc_ppp + 
    scale_tas_hom + scale_indg_pob_2010 +
    v_oblg_san + (1|pais),
  family = binomial(link = "logit"),
  control = glmerControl(optimizer = "bobyqa",
                         optCtrl = list(maxfun = 4e5)),
  data = df_logit,
  verbose = 1,
  weights = weight1500,
  na.action = na.omit)

m_S.indg_pob <- lme4::glmer(
  formula = Partidista ~ Sexo + scale_Edad + scale_Educacion + scale_Ingreso +
    Catolico + Protestante + Sin_relig + Evan_y_Pent + Otra_relig + 
    Blanca + Mestiza + Indigena + Negra + Mulata + Otra_etn +
    Simpatizar_partido + scale_ind_confianza + Votar +
    scale_gdp_growth +  scale_gdp_pc_ppp + 
    scale_tas_hom +
    v_oblg_san + (1|pais),
  family = binomial(link = "logit"),
  control = glmerControl(optimizer = "bobyqa",
                         optCtrl = list(maxfun = 4e5)),
  data = df_logit,
  verbose = 1,
  weights = weight1500,
  na.action = na.omit)

anova(m_S.logpob, m_S.ppurb, m_S.ppelectoral, m_S.cpi, m_M.tamano, m_M.tamano_F, m_S.indg_pob, m_S)
```




```{r}
m_S.indg_pob <- lme4::glmer(
  formula = Partidista ~ Sexo + scale_Edad + scale_Educacion + scale_Ingreso +
    Catolico + Protestante + Sin_relig + Evan_y_Pent + Otra_relig + 
    Blanca + Mestiza + Indigena + Negra + Mulata + Otra_etn +
    Simpatizar_partido + scale_ind_confianza + Votar +
    scale_gdp_growth +  scale_gdp_pc_ppp + 
    scale_tas_hom +
    v_oblg_san + (1|pais),
  family = binomial(link = "logit"),
  control = glmerControl(optimizer = "bobyqa",
                           optCtrl = list(maxfun = 4e5)),
  data = df_logit,
  verbose = 1,
  weights = weight1500,
  na.action = na.omit)
```


```{r}
m_S.indg_pob_F <- lme4::glmer(
  formula = Partidista ~ Sexo + scale_Edad + scale_Educacion + scale_Ingreso +
    Catolico + Protestante + Sin_relig + Evan_y_Pent + Otra_relig + 
    Blanca + Mestiza + Indigena + Negra + Mulata + Otra_etn +
    Simpatizar_partido + scale_ind_confianza + Votar +
    scale_gdp_growth +  scale_gdp_pc_ppp + 
    scale_tas_hom +
    federalista + (1|pais),
  family = binomial(link = "logit"),
  control = glmerControl(optimizer = "bobyqa",
                           optCtrl = list(maxfun = 4e5)),
  data = df_logit,
  verbose = 1,
  weights = weight1500,
  na.action = na.omit)
```

```{r}
anova(m_S.indg_pob, m_S.indg_pob_F)
```
```{r}
summary(m_S.indg_pob)
summary(m_S.indg_pob_F)
```

```{r}
summary(m_M.tamano_F)
```


### Resultado

Cambia muy poco algunos valores de significancia.
 * Población indígena es importante

```{r}
summary(m_S)
summary(m_S.logpob)
summary(m_S.ppurb)
summary(m_S.ppelectoral)
summary(m_S.cpi)
summary(m_S.indg_pob)
summary(m_M.tamano)
```



# Modelo seleccionado

```{r}
m <- lme4::glmer(
  formula = Partidista ~ Sexo_fact + scale_Edad + Ed_fact + Ingreso_fact +
    Religion_fact + Etnia_fact +
    SP_fact + scale_ind_confianza + Votar_fact +
    scale_gdp_growth +  scale_gdp_pc_ppp + scale_log_pob + scale_pp_urb_2010 +
    scale_pp_electoral + scale_cpi + scale_tas_hom + scale_indg_pob_2010 +
    federalista + (1|pais),
  family = binomial(link = "logit"),
  control = glmerControl(optimizer = "bobyqa",
                         optCtrl = list(maxfun = 4e5)),
  data = df_logit,
  verbose = 1,
  weights = weight1500,
  na.action = na.omit)
```

```{r}
pred <- predict(object = m, type = "response")
pred[pred < 0.5] <- 0
pred[pred >= 0.5] <- 1
table(pred)
table(df_logit$Partidista)
table(pred, df_logit$Partidista)
```

```{r}
19082 + 1126
15562 + 4646
(15092 + 656) / 20208 # 0.7792953
(3990 + 470) / 20208
656 / 20208
1126/19082
```

```{r}
summary(m)
```
```{r}
exp(0.032074)
```


```{r}
tab_model(m, file = "partidista_logit.html")
```


## Comparar con federalista: no cambia nada en la predicción

```{r}
m_federalista <- lme4::glmer(
  formula = Partidista ~ Sexo_fact + scale_Edad + Ed_fact + Ingreso_fact +
    Religion_fact + Etnia_fact +
    SP_fact + scale_ind_confianza + Votar_fact +
    scale_gdp_growth +  scale_gdp_pc_ppp + scale_log_pob + scale_pp_urb_2010 +
    scale_pp_electoral + scale_cpi + scale_tas_hom + scale_indg_pob_2010 +
    federalista + (1|pais),
  family = binomial(link = "logit"),
  control = glmerControl(optimizer = "bobyqa",
                         optCtrl = list(maxfun = 4e5)),
  data = df_logit,
  verbose = 1,
  weights = weight1500,
  na.action = na.omit)
```

```{r}
pred_fed <- predict(object = m_federalista, type = "response")
pred_fed[pred_fed < 0.5] <- 0
pred_fed[pred_fed >= 0.5] <- 1
table(pred_fed)
table(df_logit$Partidista)
table(pred_fed, df_logit$Partidista)
```

```{r}
19085 + 1123
15562 + 4646
(15093 + 654) / 20208 # 0.7792953
(3992 + 469) / 20208
```

```{r}
ggeffects::ggpredict(model = m, terms = "scale_Educacion [all]") |> 
  plot()
```
```{r}
ggeffects::ggpredict(model = m, terms = "Ed_fact [all]") |> 
  plot()
```

```{r}
ggeffects::ggpredict(model = m, terms = "scale_Ingreso [all]") |> 
  plot()
```

```{r}
ggeffects::ggpredict(model = m, terms = "Ingreso_fact [all]") |> 
  plot()
```

## Otras variables indicadoras

```{r}
m <- lme4::glmer(
  formula = Partidista ~ Sexo_fact + scale_Edad + Ed_fact + Ingreso_fact +
    Religion_fact + Etnia_fact +
    SP_fact + scale_ind_confianza + Votar_fact +
    scale_gdp_growth +  scale_gdp_pc_ppp + scale_log_pob + scale_pp_urb_2010 +
    scale_pp_electoral + scale_cpi + scale_tas_hom + scale_indg_pob_2010 +
    v_oblg_san_fact + (1|pais),
  family = binomial(link = "logit"),
  control = glmerControl(optimizer = "bobyqa",
                         optCtrl = list(maxfun = 4e5)),
  data = df_logit,
  verbose = 1,
  weights = weight1500,
  na.action = na.omit)

m_federalista <- lme4::glmer(
  formula = Partidista ~ Sexo_fact + scale_Edad + Ed_fact + Ingreso_fact +
    Religion_fact + Etnia_fact +
    SP_fact + scale_ind_confianza + Votar_fact +
    scale_gdp_growth +  scale_gdp_pc_ppp + scale_log_pob + scale_pp_urb_2010 +
    scale_pp_electoral + scale_cpi + scale_tas_hom + scale_indg_pob_2010 +
    federalista_fact + (1|pais),
  family = binomial(link = "logit"),
  control = glmerControl(optimizer = "bobyqa",
                         optCtrl = list(maxfun = 4e5)),
  data = df_logit,
  verbose = 1,
  weights = weight1500,
  na.action = na.omit)
```

```{r}
summary(m)
summary(m_federalista)
```
```{r}
tab_model(m)
tab_model(m_federalista)
```

```{r}
ggeffects::ggpredict(model = m_federalista,
                     terms = "federalista_fact [all]",
                     ci.lvl = 0.95) |> 
  plot()
```


# Optimizadores

```{r}
# diff_optims <- allFit(m_bobyqa, maxfun = 3e5)
# diff_optims_OK <- diff_optims[sapply(diff_optims, is, "merMod")]
# lapply(diff_optims_OK, function(x) x@optinfo$conv$lme4$messages)
```


## Resultado

bobyqa y nlminbwrap SÍ convergieron

"
"$bobyqa
NULL

$Nelder_Mead
[1] "Model failed to converge with max|grad| = 0.00904414 (tol = 0.002, component 1)"

$nlminbwrap
NULL

$nmkbw
[1] "Model failed to converge with max|grad| = 0.0100247 (tol = 0.002, component 1)"

$`optimx.L-BFGS-B`
[1] "Model failed to converge with max|grad| = 0.0256062 (tol = 0.002, component 1)"

$nloptwrap.NLOPT_LN_NELDERMEAD
[1] "Model failed to converge with max|grad| = 0.00265416 (tol = 0.002, component 1)"

$nloptwrap.NLOPT_LN_BOBYQA
[1] "Model failed to converge with max|grad| = 0.0458471 (tol = 0.002, component 1)""
"

### Pequeño problema

1: In eval(expr, envir, enclos) : non-integer #successes in a binomial glm!

"To figure out what's going on here, as @aosmith comments above, y <- NS1*egg_total must be close to an integer (see binomial()$initialize for the test, which is (any(abs(y - round(y)) > 0.001)). If NS1 is supposed to be a proportion of eggs surviving, then this should work unless you made some kind of typo/rounding error somewhere.

This is something you want to figure out in order to make sure that the model actually makes sense; it's also possible that lme4 will behave badly with non-integer response variables for a distribution that's supposed to be integral."

### No funcionaron

Quizás por el bajo número de optimizaciones

#### nloptwrap

```{r}
m_nloptwrap <- lme4::glmer(
  formula = Partidista ~ 1 + Sexo + Edad + Educacion + Ingreso +
    Catolico + Protestante + Sin_relig + Evan_y_Pent + Otra_relig + 
    Blanca + Mestiza + Indigena + Negra + Mulata + Otra_etn +
    Simpatizar_partido + ind_confianza + Votar +
    gdp_growth + scale(gdp_pc_ppp) + log_pob + pp_urb_2010 +
    pp_electoral + cpi + tas_hom + indg_pob_2010 +
    ele_2011 + v_oblg + v_oblg_san + federalista + (1|pais),
  family = binomial(link = "logit"),
  control = glmerControl(optimizer = "nloptwrap",
                         optCtrl = list(maxfun = 2e5)),
  data = df_logit,
  verbose = 0,
  weights = weight1500,
  na.action = na.omit)

summary(m_nloptwrap)
```


#### Nelder_Mead

```{r}
m_nelder_mead <- lme4::glmer(
  formula = Partidista ~ 1 + Sexo + Edad + Educacion + Ingreso +
    Catolico + Protestante + Sin_relig + Evan_y_Pent + Otra_relig + 
    Blanca + Mestiza + Indigena + Negra + Mulata + Otra_etn +
    Simpatizar_partido + ind_confianza + Votar +
    scale_gdp_growth +  scale_gdp_pc_ppp + scale_log_pob + scale_pp_urb_2010 +
    scale_pp_electoral + scale_cpi + scale_tas_hom + scale_indg_pob_2010 +
    ele_2011 + v_oblg + v_oblg_san + federalista + (1|pais),
  family = binomial(link = "logit"),
  control = glmerControl(optimizer = "Nelder_Mead",
                         optCtrl = list(maxfun = 2e5)),
  data = df_logit,
  verbose = 0,
  weights = weight1500,
  na.action = na.omit)

summary(m_nelder_mead)
```


#### L-BFGS_b

```{r}
m_LBFGSB<- lme4::glmer(
  formula = Partidista ~ 1 + Sexo + scale_Edad + scale_Educacion + scale_Ingreso +
    Catolico + Protestante + Sin_relig + Evan_y_Pent + Otra_relig + 
    Blanca + Mestiza + Indigena + Negra + Mulata + Otra_etn +
    Simpatizar_partido + scale_ind_confianza + Votar +
    scale_gdp_growth +  scale_gdp_pc_ppp + scale_log_pob + scale_pp_urb_2010 +
    scale_pp_electoral + scale_cpi + scale_tas_hom + scale_indg_pob_2010 +
    ele_2011 + v_oblg + v_oblg_san + federalista + (1|pais),
  family = binomial(link = "logit"),
  control = glmerControl(optimizer ='optimx', 
                         optCtrl = list(method = 'L-BFGS-B')),
  data = df_logit,
  verbose = 0,
  weights = weight1500,
  na.action = na.omit)

summary(m_LBFGSB)
```



#### nlminb

```{r}
m_nlminb <- lme4::glmer(
  formula = Partidista ~ 1 + Sexo + scale_Edad + scale_Educacion + scale_Ingreso +
    Catolico + Protestante + Sin_relig + Evan_y_Pent + Otra_relig + 
    Blanca + Mestiza + Indigena + Negra + Mulata + Otra_etn +
    Simpatizar_partido + scale_ind_confianza + Votar +
    scale_gdp_growth +  scale_gdp_pc_ppp + scale_log_pob + scale_pp_urb_2010 +
    scale_pp_electoral + scale_cpi + scale_tas_hom + scale_indg_pob_2010 +
    ele_2011 + v_oblg + v_oblg_san + federalista + (1|pais),
  family = binomial(link = "logit"),
  control = glmerControl(optimizer ='optimx', 
                         optCtrl = list(method = 'nlminb')),
  data = df_logit,
  verbose = 0,
  weights = weight1500,
  na.action = na.omit)

summary(m_nlminb)
```




# Plotar

## Efectos aleatorios

```{r}
plot(residuals(m_S.indg_pob_F))
```


```{r}
qqmath(ranef(m2, condVar = TRUE))
```

  
## Correlaciones

```{r}
df2corr <- df_logit[ , !names(df_logit) %in% c("pais", "weight1500")]
corr1 <- cor(df2corr, method = "spearman")
corrplot(corr1, method = "number")
```
  


# Otros modelos

## Modelo sin escalar todas las variables: no converge

```{r}
m <- lme4::glmer(
  formula = Partidista ~ Sexo + Edad + Educacion + Ingreso +
    Catolico + Protestante + Sin_relig + Evan_y_Pent + Otra_relig + 
    Blanca + Mestiza + Indigena + Negra + Mulata + Otra_etn +
    Simpatizar_partido + ind_confianza + Votar +
    gdp_growth +  scale_gdp_pc_ppp + log_pob + pp_urb_2010 +
    pp_electoral + cpi + tas_hom + indg_pob_2010 +
    ele_2011 + v_oblg + v_oblg_san + federalista + (1|pais),
  family = binomial(link = "logit"),
  control = glmerControl(optimizer = "bobyqa",
                         optCtrl = list(maxfun = 3e5)),
  data = df_logit,
  verbose = 0,
  weights = weight1500,
  na.action = na.omit)
summary(m)
```

## Extras

```{r}
m_federalista <- lme4::glmer(
  formula = Partidista ~ Sexo + scale_Edad + scale_Educacion + scale_Ingreso +
    Catolico + Protestante + Sin_relig + Evan_y_Pent + Otra_relig + 
    Blanca + Mestiza + Indigena + Negra + Mulata + Otra_etn +
    Simpatizar_partido + scale_ind_confianza + Votar +
    scale_gdp_growth +  scale_gdp_pc_ppp + scale_log_pob + scale_pp_urb_2010 +
    scale_pp_electoral + scale_cpi + scale_tas_hom + scale_indg_pob_2010 +
    federalista + (1|pais),
  family = binomial(link = "logit"),
  control = glmerControl(optimizer = "bobyqa",
                         optCtrl = list(maxfun = 4e5)),
  data = df_logit,
  verbose = 1,
  weights = weight1500,
  na.action = na.omit)

m_federalista_RF <- lme4::glmer(
  formula = Partidista ~ Sexo + scale_Edad + scale_Educacion + scale_Ingreso +
    Religion_fact + 
    Blanca + Mestiza + Indigena + Negra + Mulata + Otra_etn +
    Simpatizar_partido + scale_ind_confianza + Votar +
    scale_gdp_growth +  scale_gdp_pc_ppp + scale_log_pob + scale_pp_urb_2010 +
    scale_pp_electoral + scale_cpi + scale_tas_hom + scale_indg_pob_2010 +
    federalista + (1|pais),
  family = binomial(link = "logit"),
  control = glmerControl(optimizer = "bobyqa",
                         optCtrl = list(maxfun = 4e5)),
  data = df_logit,
  verbose = 1,
  weights = weight1500,
  na.action = na.omit)

m_federalista_EF <- lme4::glmer(
  formula = Partidista ~ Sexo + scale_Edad + scale_Educacion + scale_Ingreso +
    Catolico + Protestante + Sin_relig + Evan_y_Pent + Otra_relig + 
    Etnia_fact +
    Simpatizar_partido + scale_ind_confianza + Votar +
    scale_gdp_growth +  scale_gdp_pc_ppp + scale_log_pob + scale_pp_urb_2010 +
    scale_pp_electoral + scale_cpi + scale_tas_hom + scale_indg_pob_2010 +
    federalista + (1|pais),
  family = binomial(link = "logit"),
  control = glmerControl(optimizer = "bobyqa",
                         optCtrl = list(maxfun = 4e5)),
  data = df_logit,
  verbose = 1,
  weights = weight1500,
  na.action = na.omit)

m_federalista_fact <- lme4::glmer(
  formula = Partidista ~ Sexo + scale_Edad + Ed_fact + Ingreso_fact +
    Religion_fact +  
    Etnia_fact +
    Simpatizar_partido + scale_ind_confianza + Votar +
    scale_gdp_growth +  scale_gdp_pc_ppp + scale_log_pob + scale_pp_urb_2010 +
    scale_pp_electoral + scale_cpi + scale_tas_hom + scale_indg_pob_2010 +
    federalista + (1|pais),
  family = binomial(link = "logit"),
  control = glmerControl(optimizer = "bobyqa",
                         optCtrl = list(maxfun = 4e5)),
  data = df_logit,
  verbose = 1,
  weights = weight1500,
  na.action = na.omit)

m_oblg_san_fact <- lme4::glmer(
  formula = Partidista ~ Sexo + scale_Edad + Ed_fact + Ingreso_fact +
    Religion_fact +  
    Etnia_fact +
    Simpatizar_partido + scale_ind_confianza + Votar +
    scale_gdp_growth +  scale_gdp_pc_ppp + scale_log_pob + scale_pp_urb_2010 +
    scale_pp_electoral + scale_cpi + scale_tas_hom + scale_indg_pob_2010 +
    v_oblg_san + (1|pais),
  family = binomial(link = "logit"),
  control = glmerControl(optimizer = "bobyqa",
                         optCtrl = list(maxfun = 4e5)),
  data = df_logit,
  verbose = 1,
  weights = weight1500,
  na.action = na.omit)

anova(m_federalista, m_federalista_RF, m_federalista_EF,m_federalista_fact, m_oblg_san_fact)
```

```{r}
summary(m_federalista)
summary(m_federalista_RF)
summary(m_federalista_EF)
summary(m_federalista_fact)
summary(m_oblg_san_fact)
```

