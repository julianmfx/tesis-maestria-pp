---
title: "Estadística descriptiva - prof. Nieto"
author: "jmf"
date: "`r Sys.Date()`"
output:
  html_document:
    df_print: paged
    toc: TRUE
    toc_float: TRUE
    theme: cosmo
    highlight: zenburn
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)
```

# Introducción

El objetivo de este código es realizar el análisis estadístico de la encuesta de 2012 de LAPOP.

# Librerías 


```{r librerias, results = "hide", message = FALSE, warning = FALSE}
library(dplyr) # to use pipe operators
library(haven) # to import database
library(tidyverse) # to use ggplot()
library(purrr) # to use map()
```


# Importación de la base de datos
Utilización de la última versión de la base de datos: "lapop12_dic.Rds"
```{r import}
lapop12 <- readr::read_rds(
  "/home/julian/Documents/tesis-maestria_participacion-politica/bd_LAPOP/datos-procesados/lapop12_dic.Rds")
```  

# Visualización de la base de datos
```{r}
str(lapop12)
```

```{r}
glimpse(lapop12)
```

```{r, echo = FALSE}
print(paste("Son 27756 líneas y 25 columnas agrupadas a través de 17 países"))
print(paste("Hay un total de", 25*27756, "observaciones."))
print(paste("Observaciones sin respuesta totalizan el número de", sum(is.na(lapop12))))
print(paste("El número de observaciones medidas sin contar las faltantes es de", 693900-51081)) # justo se incrementaron en 166 el número de NA's después de la correción de q2
```

```{r}
summary(lapop12)
```

```{r}
summary(lapop12, maxsum = 17) # para ver todos los países
```


```{r}
a <- as.data.frame(summary(lapop12, maxsum = 17))
```

```{r}
a
```
```{r}
glimpse(lapop12)
```


```{r}
map(lapop12, levels)
```

