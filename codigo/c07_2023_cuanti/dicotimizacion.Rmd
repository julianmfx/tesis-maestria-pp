---
title: "dicotomización de las variables"
author: "jmf"
date: "`r Sys.Date()`"
output:   
  html_document:
    toc: TRUE
    toc_float: TRUE
    theme: cosmo
    highlight: zenburn
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)
```

# Introducción

El objetivo de este código es dicotomizar las variables de la base de datos de análisis a fin de avanzar para las próximas etapas de la tesis.

# Librerías 


```{r librerias, results = "hide", message=FALSE, warning=FALSE}

library(dplyr) # to use pipe operators
library(haven) # to import database
library(tidyverse) # to use ggplot()
library(purrr) # to use map()
library(janitor) # to use compare_df_cols()
library(vetr) # to use alike()
library(diffdf) # to use diffdf()
```


# Importación de la base de datos

```{r import}
lapop12 <- readr::read_rds(
  "/home/julian/Documents/tesis-maestria_participacion-politica/bd_LAPOP/datos-procesados/lapop12_A.Rds")
```  

```{r}
str(lapop12)
map(lapop12, levels)
```

```{r}
head(lapop12)
```

# Descripción de las variables

Las variables son:

| Variables 2012 |
|----------------|
| pais           |
| cp2            |
| cp4a           |
| cp4            |
| cp5            |
| cp6            |
| cp7            |
| cp8            |
| cp9            |
| cp13           |
| vb20           |
| cp21           |
| np1            |
| np2            |
| pp1            |
| pp2            |
| prot3          |
| prot6          |
| prot7          |
| q1             |
| q2             |
| vb2            |
| vb20           |
| vic44          |


Todas las variables son factores, a excepción de la variable q2 (edad) que es numérica.

# Dicotomización de las variables

La dicotomización de las variables sirve para:

* Disminuir la complejidad;
* Incrementar la comparabilidad;
* Permitir mayor uso herramientas;
* Garantizar ventajas paramétricas;

## Observaciones de la dicotomización

Algunas variables originalmente son dicotómicas porque son respuestas a perguntas de sí o no.

Esas variables son:

| Variables binarias en la encuesta de 2012 |
|----------------|
| q1             |
| cp2            |
| cp4a           |
| cp4            |
| np1            |
| np2            |
| prot3          |
| prot7          |
| prot6          |
| prot8          |
| vic44          |
| vb2            |
| pp2            |

Las que no son binarias son:

| Variables no binarias en la encuesta de 2012 |
|----------------|
| cp5            |
| cp6            |
| cp7            |
| cp8            |
| cp9            |
| cp13           |
| cp20           |
| cp21           |
| vb20           |
| pp1            |
| q2             |


Todas las no binarias serán dicotomizadas, excepto q2 que es un variable númerica.

La función map demuestra los niveles de cada variable de la base de datos:

```{r}
map(lapop12, levels)
```

La dicotomización fue realizada según la siguiente clasificación:

1. Las personas que respondieron "Una vez a la semana" y "Una o dos veces al mes" fueron clasificadas como "Constantemente o ocasionalmente";
2. Las personas que respondieron "Una o dos veces al año" y "Nunca" fueron clasificadas como "Nunca o raramente";
3. Para la variable "pp1", las personas que respondieron "Frecuentemente" y "De vez en cuando" fueron clasificadas como "Constantemente o ocasionalmente";
4. Para la variable "pp1", las personas que respondieron "Rara vez" y "Nunca" fueron clasificadas como "Nunca o raramente";
5. Para la varialbe "vb20", las personas que respondieron "Votaría por situación", Votaria por oposición" y "Votaría Blanco/Nulo" son clasificadas como "Votaría";
6. Para la varialbe "vb20", las personas que respondieron "No votaría", son clasificadas como "No votaría";
7. Las personas que no saben, no respondieron y a las cuales no se aplica fueron clasificadas como valores faltantes (NAs).


## Variable cp5

```{r}
unique(lapop12$cp5)
table(lapop12$cp5)
table(paste(lapop12$cp5))
```

```{r}
levels(lapop12$cp5) <- list("Constantemente o ocasionalmente" = c("Una vez a la semana", "Una o dos veces al mes"),
                            "Nunca o raramente" = c("Nunca", "Una o dos veces al año"),
                            NA)
```

```{r}
1160+3336
4980+17893
387
```

```{r}
unique(lapop12$cp5)
table(lapop12$cp5)
table(paste(lapop12$cp5))
```

## Variable cp6

```{r}
unique(lapop12$cp6)
table(lapop12$cp6)
table(paste(lapop12$cp6))
```

```{r}
levels(lapop12$cp6) <- list("Constantemente o ocasionalmente" = c("Una vez a la semana", "Una o dos veces al mes"),
                            "Nunca o raramente" = c("Nunca", "Una o dos veces al año"),
                            NA)
```

```{r}
8387+4751
2799+11710
109
```


```{r}
unique(lapop12$cp6)
table(lapop12$cp6)
table(paste(lapop12$cp6))
```

## Variable cp7

```{r}
unique(lapop12$cp7)
table(lapop12$cp7)
table(paste(lapop12$cp7))
```

```{r}
levels(lapop12$cp7) <-  list("Constantemente o ocasionalmente" = c("Una vez a la semana", "Una o dos veces al mes"),
                            "Nunca o raramente" = c("Nunca", "Una o dos veces al año"),
                            NA)
```

```{r}
1037+6532
3149+16881
157
```


```{r}
unique(lapop12$cp7)
table(lapop12$cp7)
table(paste(lapop12$cp7))
```

## Variable cp8

```{r}
unique(lapop12$cp8)
table(lapop12$cp8)
table(paste(lapop12$cp8))
```


```{r}
levels(lapop12$cp8) <- list("Constantemente o ocasionalmente" = c("Una vez a la semana", "Una o dos veces al mes"),
                            "Nunca o raramente" = c("Nunca", "Una o dos veces al año"),
                            NA)
```

```{r}
935+3226
2800+20638
157
```


```{r}
unique(lapop12$cp8)
table(lapop12$cp8)
table(paste(lapop12$cp8))
```



## Variable cp9

```{r}
unique(lapop12$cp9)
table(lapop12$cp9)
table(paste(lapop12$cp9))
```

```{r}
levels(lapop12$cp9) <- list("Constantemente o ocasionalmente" = c("Una vez a la semana", "Una o dos veces al mes"),
                            "Nunca o raramente" = c("Nunca", "Una o dos veces al año"),
                            NA)
```

```{r}
434+1456
1334+24339
193
```

```{r}
unique(lapop12$cp9)
table(lapop12$cp9)
table(paste(lapop12$cp9))
```

## Variable cp13

```{r}
unique(lapop12$cp13)
table(lapop12$cp13)
table(paste(lapop12$cp13))
```

```{r}
levels(lapop12$cp13) <- list("Constantemente o ocasionalmente" = c("Una vez a la semana", "Una o dos veces al mes"),
                            "Nunca o raramente" = c("Nunca", "Una o dos veces al año"),
                            NA)
```

```{r}
473+1150
1561+24283
289
```

```{r}
unique(lapop12$cp13)
table(lapop12$cp13)
table(paste(lapop12$cp13))
```



# Variable cp20
```{r}
unique(lapop12$cp20)
table(lapop12$cp20)
table(paste(lapop12$cp20))
```

```{r}
levels(lapop12$cp20) <- list("Constantemente o ocasionalmente" = c("Una vez a la semana", "Una o dos veces al mes"),
                            "Nunca o raramente" = c("Nunca", "Una o dos veces al año"),
                            NA)
```

```{r}
395+857
521+12179
13804
```

```{r}
unique(lapop12$cp20)
table(lapop12$cp20)
table(paste(lapop12$cp20))
```

## Variable cp21

```{r}
unique(lapop12$cp21)
table(lapop12$cp21)
table(paste(lapop12$cp21))
```

```{r}
levels(lapop12$cp21) <- list("Constantemente o ocasionalmente" = c("Una vez a la semana", "Una o dos veces al mes"),
                            "Nunca o raramente" = c("Nunca", "Una o dos veces al año"),
                            NA)
```

```{r}
2499+2384
1428+21228
217
```

```{r}
unique(lapop12$cp21)
table(lapop12$cp21)
table(paste(lapop12$cp21))
```

## Variable pp1

```{r}
unique(lapop12$pp1)
table(lapop12$pp1)
table(paste(lapop12$pp1))
```

```{r}
levels(lapop12$pp1) <- list("Constantemente o ocasionalmente" = c("Frecuentemente", "De vez en cuando"),
                            "Nunca o raramente" = c("Rara vez", "Nunca"),
                            NA)
```

```{r}
1453+2662
3526+19744
371
```

```{r}
unique(lapop12$pp1)
table(lapop12$pp1)
table(paste(lapop12$pp1))
```

## Variable vb20

```{r}
unique(lapop12$vb20)
table(lapop12$vb20)
table(paste(lapop12$vb20))
```

```{r}
levels(lapop12$vb20) <- list("Votaría" = c("Votaría por situación", "Votaría por oposición", "Votaría Blanco/Nulo"),
                             "No votaría" = "No votaría",
                             NA)
```

```{r}
7974+7880+2729
4735
4438
```

```{r}
unique(lapop12$vb20)
table(lapop12$vb20)
table(paste(lapop12$vb20))
```

## Reordenación de los niveles de la variable vic44

También se observó que la variable vic44 tienes los niveles en un orden distinto a las otras variables.
* Eso debe ser hecho antes de la limpieza de niveles porque este código que elaboré vuelve a incluir en la variable un "tercer nivel" que antes existía

```{r}
unique(lapop12$vic44)
table(lapop12$vic44)
table(paste(lapop12$vic44))
```

```{r}
levels(lapop12$vic44) <- list("Sí" = "Sí", 
                              "No" = "No",
                              NA)
```


```{r}
unique(lapop12$vic44)
table(lapop12$vic44)
table(paste(lapop12$vic44))
```

# Limpieza

La manipulación de las variables mencionadas hace con que NA sea un nivel entre las variables factorizadas, lo que no representa los datos y, por lo tanto, debe ser removido.

```{r}
map(lapop12, levels)
```

## Remoción de los niveles desnecesarios

Todas las variables que son factores tendrán sus niveles desnecesarios removidos.

Sobre el uso de "[]" en la función, [stefan](https://stackoverflow.com/questions/72793782/purrr-map-a-function-to-all-columns-except-for-one) afirma que, al usar map, tenemos como retorno una lista y la utilización de [] preservará la estructura del objeto, es decir, terminamos con el mismo objeto que iniciamos que, en este caso, es un dataframe. Referencias en el [guía avanzado de R](https://adv-r.hadley.nz/subsetting.html#subassignment).

Este código aplicará la función droplevels a todas variables que son factores en la base de datos. Después, se asigna el resultado a la própria base.

```{r}
lapop12[] <- map_if(lapop12, is.factor, droplevels)
```

Verificación:

```{r}
map(lapop12, levels)
```

## Recodificación de valores faltantes en la variable numérica

Algo de lo que solo me percaté en el análisis estadístico (~/codigo/c06_analisis-estadistico) ayer (17.11.22) es que la variable q2 tiene valores faltantes (NA's) codificados como 888888 y 988888. Eso será corregido en la exportación de este código.

### Valores faltantes de q2

Los valores NA's fueron descubiertos cuando apliqué la función summary, aunque ya se podría verlos durante los códigos de limpieza (c02), pero como los hice con mucha prisa no me di cuenta de eso.

```{r}
summary(lapop12)
```

Este código busca la variable q2 en la base de datos lapop12 y reemplaza los valores en que q2 es igual a 888888 o 988888 por valores faltantes.

Las funciones tables y summary antes y después sirven para verificar la transformación.
```{r}
table(paste(lapop12$q2))
summary(lapop12)
lapop12 <- lapop12 %>%
  mutate(q2 = replace(q2, q2 == 888888 | q2 == 988888, NA))

table(paste(lapop12$q2))
summary(lapop12)
29+137
```



# Exportación y verificación

Así como lo hice en los otros código de generación de una nueva base de datos, esta será exportada y posteriormente verificada.

La base de datos dicotomizada es exportada para la carpeta de datos procesados.

```{r}
# readr::write_rds(lapop12, file = "/home/julian/Documents/tesis-maestria_participacion-politica/bd_LAPOP/datos-procesados/lapop12_dic.Rds")
```

Se carga la base de datos exportada como um objeto de teste para verificar que no hubo problemas de exportación.
```{r}
lapop12_dic_test <- readr::read_rds(
  "/home/julian/Documents/tesis-maestria_participacion-politica/bd_LAPOP/datos-procesados/lapop12_dic.Rds")
```

Las pruebas de exportación permiten concluir que no hubo error de exportación.
```{r}
all_equal(lapop12, lapop12_dic_test)
compare_df_cols(lapop12, lapop12_dic_test, return = "mismatch", bind_method = "rbind")
alike(lapop12, lapop12_dic_test)
diffdf(lapop12, lapop12_dic_test)
```

--------
### Otra manera de hacer la remoción
Separación de las columnas objeto de la función droplevels para excluir la columna q2, puesto que es numérica y no permite aplicar todas las columnas.

```{r}
# col_names <- colnames(lapop12)[1:24]
```

```{r}
# lapop12[] <- map_at(lapop12, col_names, droplevels)
```

```{r}
# map(lapop12, levels)
```

