---
title: "Untitled"
author: "jmf"
date: "2024-03-10"
output: html_document
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)
```

# Otras posibles variables


N1. ¿Hasta qué punto diría que el gobierno actual combate la pobreza?
N3. ¿Hasta qué punto diría que el gobierno actual promueve y protege los principios
democráticos?
N9. ¿Hasta qué punto diría que el gobierno actual combate la corrupción en el
gobierno?
N11. ¿Hasta qué punto diría que el gobierno actual mejora la seguridad ciudadana?
N15. ¿Hasta qué punto diría que el gobierno actual está manejando bien la economía?
