---
  title: "c14_multinivel"
author: "jmf"
date: "`r Sys.Date()`"
output:   
  html_document:
  toc: TRUE
toc_float: TRUE
theme: cosmo
highlight: zenburn
---
  

# Librerías

```{r, message = F}
library(dplyr)
library(labelled)
library(dfoptim)
library(minqa)
library(optimx)
library(corrplot)
library(sjstats)
library(sjPlot)
library(performance)
library(lme4)
```

# Base de datos

```{r}
# datos <- readRDS(file = "/home/julian/Documents/tesis-maestria-pp/bd_LAPOP/datos-procesados/logit/df_vars_democracia/df_logit_final.rds")
datos <- readRDS(file = "C:/Users/L03529479/Documents/Julian/tesis-maestria_participacion-politica/bd_LAPOP/datos-procesados/logit/df_vars_democracia/df_logit_final.rds")
```


```{r}
glimpse(datos)
```

## Seleccionando variables

```{r}
df_logit <-  na.omit(
  datos[ , c("Comunitaria", "Sexo", "Sexo_fact",
                "Edad", "scale_Edad", 
                "Educacion", "scale_Educacion", "Ed_fact",
                "Ingreso", "scale_Ingreso", "Ingreso_fact",
                "Catolico", "Protestante", "Sin_relig", "Evan_y_Pent", "Otra_relig",
                "Religion_fact", 
                "Blanca", "Mestiza", "Indigena", "Negra", "Mulata", "Otra_etn",
                "Etnia_fact",
                "Simpatizar_partido", "SP_fact",
                "ind_confianza", "scale_ind_confianza",
                "Votar", "Votar_fact",
                #
                "Satisfaccion_democracia",
                #
                "gdp_growth", "scale_gdp_growth",
                "gdp_pc_ppp", "scale_gdp_pc_ppp",
                "log_pob", "scale_log_pob",
                "pp_urb_2010", "scale_pp_urb_2010",
                "pp_electoral", "scale_pp_electoral",
                "cpi", "scale_cpi",
                "tas_hom", "scale_tas_hom",
                "indg_pob_2010", "scale_indg_pob_2010",
                "ele_2011", "ele_2011_fact",
                "v_oblg", "v_oblg_fact",
                "v_oblg_san", "v_oblg_san_fact", 
                "federalista", "federalista_fact",
                "pais", "weight1500")])
```

```{r}
glimpse(df_logit)
```

```{r}
table(is.na(df_logit))
```

# Modelo nulo

```{r}
null_comu <- lme4::glmer(
  formula = Comunitaria ~ 1 + (1|pais),
  family = binomial(link = "logit"),
  control = glmerControl(optimizer = "bobyqa",
                         optCtrl = list(maxfun = 4e5)),
  data = df_logit,
  verbose = 1,
  weights = weight1500,
  na.action = na.omit)
```

```{r}
summary(null_comu)
```

```{r}
VarCorr(null_comu)
```

```{r}
# intercept_variance / intercerpt_variance + ((pi^2) / 3)
0.2775 / (0.2775 + (pi^2)/3)
```

```{r}
quantile(residuals(null_comu, "pearson", scaled = TRUE))
```


```{r}
var(residuals(null_comu, type = "pearson"))
sd(residuals(null_comu, type = "pearson"))
```

```{r}
performance::icc(null_comu)
```

```{r}
exp(-0.4500)
tab_model(null_comu)
```


# Modelo completo

bobyqa - Convergió con maxfun = 3e5

# Modelo seleccionado

```{r}
m <- lme4::glmer(
  formula = Comunitaria ~ Sexo_fact + scale_Edad + Ed_fact + Ingreso_fact +
    Religion_fact + Etnia_fact +
    SP_fact + scale_ind_confianza + Votar_fact +
    scale_gdp_growth +  scale_gdp_pc_ppp + scale_log_pob + scale_pp_urb_2010 +
    scale_pp_electoral + scale_cpi + scale_tas_hom + scale_indg_pob_2010 +
    v_oblg_san_fact + (1|pais),
  family = binomial(link = "logit"),
  control = glmerControl(optimizer = "bobyqa",
                         optCtrl = list(maxfun = 4e5)),
  data = df_logit,
  verbose = 1,
  weights = weight1500,
  na.action = na.omit)
```

```{r}
summary(m)
```

<!-- ## Exportar modelo seleccionado -->

<!-- Agregué esta parte para incluir el modelo completo en el anexo de la tesis -->

<!-- ```{r} -->
<!-- tab_model(m, file = "C:/Users/L03529479/Documents/Julian/tesis-maestria_participacion-politica/illustrations/multinivel/modelos_completos/comunitario.html") -->
<!-- ``` -->


## Seguir com comparación con la variable de democracia

```{r}
summary(m_democracia)
```

```{r}
summary(m_democracia_binaria)
```


```{r}
tab_model(m_democracia)
```

```{r}
tab_model(m_democracia_binaria, file = "/home/julian/Documents/tesis-maestria-pp/illustrations/multinivel/extra/satisfaccion_democracia.html")
```


## Agregar Satisfaccion democracia

No hay efecto y cambia mucho

PN4. Cambiando de tema, en general, ¿usted diría que está muy satisfecho(a), satisfecho(a),
insatisfecho(a) o muy insatisfecho(a) con la forma en que la democracia funciona en (país)?
(1) Muy satisfecho(a) 
(2) Satisfecho(a)
(3) Insatisfecho(a) 
(4) Muy insatisfecho(a)
(88) NS
(98) NR
(99) INAP

```{r}
table(df_logit$Satisfaccion_democracia)
```

```{r}
df_logit <- df_logit |> 
  mutate(Satisfaccion_democracia = case_when(
    Satisfaccion_democracia %in% c(1,2) ~ 1, # satisfecho
    Satisfaccion_democracia %in% c(3,4) ~ 0, # insatisfecho
    TRUE ~ 99
  ))
```

```{r}
table(df_logit$Satisfaccion_democracia)
4348+546
609+5914
```


```{r}
m_democracia_binaria <- lme4::glmer(
  formula = Comunitaria ~ Sexo_fact + scale_Edad + Ed_fact + Ingreso_fact +
    Religion_fact + Etnia_fact +
    SP_fact + scale_ind_confianza + Votar_fact +
    Satisfaccion_democracia +
    scale_gdp_growth +  scale_gdp_pc_ppp + scale_log_pob + scale_pp_urb_2010 +
    scale_pp_electoral + scale_cpi + scale_tas_hom + scale_indg_pob_2010 +
    v_oblg_san_fact + (1|pais),
  family = binomial(link = "logit"),
  control = glmerControl(optimizer = "bobyqa",
                         optCtrl = list(maxfun = 4e5)),
  data = df_logit,
  verbose = 1,
  weights = weight1500,
  na.action = na.omit)
```

```{r}
summary(m_democracia)
```


## Agregar Aficion democracia

Aquí sí hay un efecto. Personas afectas a la democracia, tienden a participar menos comunitariamente.

```{r}
table(df_logit$Aficion_democracia)
```

```{r}
df_logit <- df_logit |> 
  mutate(Aficion_democracia = case_when(
    Aficion_democracia %in% c(1,3) ~ 0,
    Aficion_democracia == 2 ~ 1,
    TRUE ~ 99
  ))
```

```{r}
table(df_logit$Aficion_democracia)
2206+2361
```


```{r}
m_democracia <- lme4::glmer(
  formula = Comunitaria ~ Sexo_fact + scale_Edad + Ed_fact + Ingreso_fact +
    Religion_fact + Etnia_fact +
    SP_fact + scale_ind_confianza + Votar_fact +
    Aficion_democracia +
    scale_gdp_growth +  scale_gdp_pc_ppp + scale_log_pob + scale_pp_urb_2010 +
    scale_pp_electoral + scale_cpi + scale_tas_hom + scale_indg_pob_2010 +
    v_oblg_san_fact + (1|pais),
  family = binomial(link = "logit"),
  control = glmerControl(optimizer = "bobyqa",
                         optCtrl = list(maxfun = 4e5)),
  data = df_logit,
  verbose = 1,
  weights = weight1500,
  na.action = na.omit)
```

```{r}
summary(m_democracia)
```

## Agregar problemas democracia

# Modelo seleccionado

```{r}
table(df_logit$Problemas_democracia)
```

```{r}
df_logit$scale_Problemas_democracia <- scale(df_logit$Problemas_democracia)
```


```{r}
m_democracia <- lme4::glmer(
  formula = Comunitaria ~ Sexo_fact + scale_Edad + Ed_fact + Ingreso_fact +
    Religion_fact + Etnia_fact +
    SP_fact + scale_ind_confianza + Votar_fact +
    scale_Problemas_democracia +
    scale_gdp_growth +  scale_gdp_pc_ppp + scale_log_pob + scale_pp_urb_2010 +
    scale_pp_electoral + scale_cpi + scale_tas_hom + scale_indg_pob_2010 +
    v_oblg_san_fact + (1|pais),
  family = binomial(link = "logit"),
  control = glmerControl(optimizer = "bobyqa",
                         optCtrl = list(maxfun = 4e5)),
  data = df_logit,
  verbose = 1,
  weights = weight1500,
  na.action = na.omit)
```

```{r}
summary(m_democracia)
```

```{r}
pred <- predict(object = m, type = "response")
pred[pred < 0.5] <- 0
pred[pred > 0.5] <- 1
table(pred)
table(df_logit$Comunitaria)
table(pred, df_logit$Comunitaria)
```

```{r}
14197+6011
11892+8316
(9521 + 3640) / 20208 # 65% 0.6512767
(4676 + 2371) / 20208
3640 / 20208
9521 / 20208
0.1801267 + 0.47115
```


```{r}
tab_model(m, file = "comunitaria_logit.html")
```


## Predecir valores del modelo

```{r}
ggeffects::ggpredict(model = m,
                     terms = c("Etnia_fact [all]")) |> 
  plot()
```

```{r}
unique(df_logit$Etnia_fact
       )
```


```{r}
ggeffects::ggpredict(model = m,
                     terms = c("scale_gdp_pc_ppp [all]",
                               # "Etnia_fact [all]",
                               "Ed_fact [all]",
                               "Votar_fact [all]"),
                     ci.lvl = NA) |> 
  plot()
```



## Valores predichos para todas las variables

```{r}
summary(m)
```


```{r}
ggpredict(model = m,
          terms = c("scale_gdp_pc_ppp [all]")) |> 
  plot()
```


## Otras variables indicadoras

```{r}
m <- lme4::glmer(
  formula = Voz ~ Sexo_fact + scale_Edad + Ed_fact + Ingreso_fact +
    Religion_fact + Etnia_fact +
    SP_fact + scale_ind_confianza + Votar_fact +
    scale_gdp_growth +  scale_gdp_pc_ppp + scale_log_pob + scale_pp_urb_2010 +
    scale_pp_electoral + scale_cpi + scale_tas_hom + scale_indg_pob_2010 +
    v_oblg_san_fact + (1|pais),
  family = binomial(link = "logit"),
  control = glmerControl(optimizer = "bobyqa",
                         optCtrl = list(maxfun = 4e5)),
  data = df_logit,
  verbose = 1,
  weights = weight1500,
  na.action = na.omit)

m_v_oblg <- lme4::glmer(
  formula = Voz ~ Sexo_fact + scale_Edad + Ed_fact + Ingreso_fact +
    Religion_fact + Etnia_fact +
    SP_fact + scale_ind_confianza + Votar_fact +
    scale_gdp_growth +  scale_gdp_pc_ppp + scale_log_pob + scale_pp_urb_2010 +
    scale_pp_electoral + scale_cpi + scale_tas_hom + scale_indg_pob_2010 +
    v_oblg_fact + (1|pais),
  family = binomial(link = "logit"),
  control = glmerControl(optimizer = "bobyqa",
                         optCtrl = list(maxfun = 4e5)),
  data = df_logit,
  verbose = 1,
  weights = weight1500,
  na.action = na.omit)

m_ele_2011 <- lme4::glmer(
  formula = Voz ~ Sexo_fact + scale_Edad + Ed_fact + Ingreso_fact +
    Religion_fact + Etnia_fact +
    SP_fact + scale_ind_confianza + Votar_fact +
    scale_gdp_growth +  scale_gdp_pc_ppp + scale_log_pob + scale_pp_urb_2010 +
    scale_pp_electoral + scale_cpi + scale_tas_hom + scale_indg_pob_2010 +
    ele_2011_fact + (1|pais),
  family = binomial(link = "logit"),
  control = glmerControl(optimizer = "bobyqa",
                         optCtrl = list(maxfun = 4e5)),
  data = df_logit,
  verbose = 1,
  weights = weight1500,
  na.action = na.omit)
```



# Predecir

```{r}
df <- summary(m)
df <- as.data.frame(df$coefficients)
df <- as.data.frame(t(df))
df
names(df)
```

```{r}
pred.val <- ggpredict(m, terms = c("scale_gdp_pc_ppp [all]", "v_oblg_san_fact [all]", "Votar_fact [all]"), type = "fixed") %>%
  as_tibble() %>%
  dplyr::rename(scale_gdp_pc_ppp = x, Comunitaria = predicted, v_oblg_san_fact = group, Votar_fact = facet)

pred.val
```

```{r}
# Plot predicted effects
ggplot(pred.val, 
       aes(y = Comunitaria, x = scale_gdp_pc_ppp, group = Votar_fact, color = Votar_fact)) + 
  # Lines of mathach by ses.dev, grouped/colored by sector (Public vs Catholic)
  geom_smooth() +
  # Different panels for different mean.ses values
  facet_grid(~ v_oblg_san_fact) +
  # geom_errorbar(
  # aes(ymin = conf.low, ymax = conf.high),
  # position = position_dodge()
  # ) +
  # geom_ribbon(aes(ymin = conf.low, ymax = conf.high, alpha = 0.001)) +
  theme(legend.position="bottom")
```

```{r}
# Plot predicted effects
ggplot(pred.val, 
       aes(y = Comunitaria, x = scale_gdp_pc_ppp, group = Ed_fact, color = Ed_fact)) + 
  # Lines of mathach by ses.dev, grouped/colored by sector (Public vs Catholic)
  geom_smooth() +
  # Different panels for different mean.ses values
  # facet_grid(~ Votar_fact) + 
  geom_errorbar(
    aes(ymin = conf.low, ymax = conf.high),
    # position = position_dodge()
  ) +
  # geom_ribbon(aes(ymin = conf.low, ymax = conf.high, alpha = 0.01)) +
  theme(legend.position="bottom")
```


### Efectos pais

```{r}
pais.effects <- ranef(m5)$pais %>%
  as_tibble(rownames = "pais") %>%
    # These are called u_j in certain textbooks.
    rename(u_j = `(Intercept)`)
```

```{r}
pais.effects <- pais.effects |> 
  mutate(pais = factor(pais))
pais.effects
```


```{r}
# predict the scores based on the model
df_logit$m2 <- predict(m2, type = "response")

# graph with predicted Comunitaria score for ingreso
df_logit %>% 
  ggplot(aes(x = scale_Ingreso, y = m2, color = Indigena, group = Indigena)) + 
  geom_smooth(se = T, method = lm) +
  theme_bw() +
  theme_bw() +
  labs(x = "scale_Ingreso", y = "m2", color = "Indigena")
```

```{r}
qqmath(ranef(m2, condVar = T))
```


```{r}
coefs_m2 <- coef(m2)
# print random effects and best line
coefs_m2$pais %>%
  mutate(pais = rownames(coefs_m2$pais))  %>% 
  ggplot(aes(scale_Ingreso, `(Intercept)`, label = pais)) + 
  geom_point() + 
  geom_smooth(se = F, method = lm) +
  geom_label(nudge_y = 0.15, alpha = 0.5) +
  theme_bw() +
  labs(x = "Slope", y = "Intercept")
```




# Optimizadores

```{r}
diff_optims <- allFit(m_bobyqa, maxfun = 3e5)
diff_optims_OK <- diff_optims[sapply(diff_optims, is, "merMod")]
lapply(diff_optims_OK, function(x) x@optinfo$conv$lme4$messages)
```


## Resultado

bobyqa y nlminbwrap SÍ convergieron

"
"$bobyqa
NULL

$Nelder_Mead
[1] "Model failed to converge with max|grad| = 0.00904414 (tol = 0.002, component 1)"

$nlminbwrap
NULL

$nmkbw
[1] "Model failed to converge with max|grad| = 0.0100247 (tol = 0.002, component 1)"

$`optimx.L-BFGS-B`
[1] "Model failed to converge with max|grad| = 0.0256062 (tol = 0.002, component 1)"

$nloptwrap.NLOPT_LN_NELDERMEAD
[1] "Model failed to converge with max|grad| = 0.00265416 (tol = 0.002, component 1)"

$nloptwrap.NLOPT_LN_BOBYQA
[1] "Model failed to converge with max|grad| = 0.0458471 (tol = 0.002, component 1)""
"

### Pequeño problema

1: In eval(expr, envir, enclos) : non-integer #successes in a binomial glm!

"To figure out what's going on here, as @aosmith comments above, y <- NS1*egg_total must be close to an integer (see binomial()$initialize for the test, which is (any(abs(y - round(y)) > 0.001)). If NS1 is supposed to be a proportion of eggs surviving, then this should work unless you made some kind of typo/rounding error somewhere.

This is something you want to figure out in order to make sure that the model actually makes sense; it's also possible that lme4 will behave badly with non-integer response variables for a distribution that's supposed to be integral."

### No funcionaron

Quizás por el bajo número de optimizaciones

#### nloptwrap

```{r}
m_nloptwrap <- lme4::glmer(
  formula = Comunitaria ~ 1 + Sexo + Edad + Educacion + Ingreso +
    Catolico + Protestante + Sin_relig + Evan_y_Pent + Otra_relig + 
    Blanca + Mestiza + Indigena + Negra + Mulata + Otra_etn +
    Simpatizar_partido + ind_confianza + Votar +
    gdp_growth + scale(gdp_pc_ppp) + log_pob + pp_urb_2010 +
    pp_electoral + cpi + tas_hom + indg_pob_2010 +
    ele_2011 + v_oblg + v_oblg_san + federalista + (1|pais),
  family = binomial(link = "logit"),
  control = glmerControl(optimizer = "nloptwrap",
                         optCtrl = list(maxfun = 2e5)),
  data = df_logit,
  verbose = 0,
  weights = weight1500,
  na.action = na.omit)

summary(m_nloptwrap)
```


#### Nelder_Mead

```{r}
m_nelder_mead <- lme4::glmer(
  formula = Comunitaria ~ 1 + Sexo + Edad + Educacion + Ingreso +
    Catolico + Protestante + Sin_relig + Evan_y_Pent + Otra_relig + 
    Blanca + Mestiza + Indigena + Negra + Mulata + Otra_etn +
    Simpatizar_partido + ind_confianza + Votar +
    scale_gdp_growth +  scale_gdp_pc_ppp + scale_log_pob + scale_pp_urb_2010 +
    scale_pp_electoral + scale_cpi + scale_tas_hom + scale_indg_pob_2010 +
    ele_2011 + v_oblg + v_oblg_san + federalista + (1|pais),
  family = binomial(link = "logit"),
  control = glmerControl(optimizer = "Nelder_Mead",
                         optCtrl = list(maxfun = 2e5)),
  data = df_logit,
  verbose = 0,
  weights = weight1500,
  na.action = na.omit)

summary(m_nelder_mead)
```


#### L-BFGS_b

```{r}
m_LBFGSB<- lme4::glmer(
  formula = Comunitaria ~ 1 + Sexo + scale_Edad + scale_Educacion + scale_Ingreso +
    Catolico + Protestante + Sin_relig + Evan_y_Pent + Otra_relig + 
    Blanca + Mestiza + Indigena + Negra + Mulata + Otra_etn +
    Simpatizar_partido + scale_ind_confianza + Votar +
    scale_gdp_growth +  scale_gdp_pc_ppp + scale_log_pob + scale_pp_urb_2010 +
    scale_pp_electoral + scale_cpi + scale_tas_hom + scale_indg_pob_2010 +
    ele_2011 + v_oblg + v_oblg_san + federalista + (1|pais),
  family = binomial(link = "logit"),
  control = glmerControl(optimizer ='optimx', 
                         optCtrl = list(method = 'L-BFGS-B')),
  data = df_logit,
  verbose = 0,
  weights = weight1500,
  na.action = na.omit)

summary(m_LBFGSB)
```



#### nlminb

```{r}
m_nlminb <- lme4::glmer(
  formula = Comunitaria ~ 1 + Sexo + scale_Edad + scale_Educacion + scale_Ingreso +
    Catolico + Protestante + Sin_relig + Evan_y_Pent + Otra_relig + 
    Blanca + Mestiza + Indigena + Negra + Mulata + Otra_etn +
    Simpatizar_partido + scale_ind_confianza + Votar +
    scale_gdp_growth +  scale_gdp_pc_ppp + scale_log_pob + scale_pp_urb_2010 +
    scale_pp_electoral + scale_cpi + scale_tas_hom + scale_indg_pob_2010 +
    ele_2011 + v_oblg + v_oblg_san + federalista + (1|pais),
  family = binomial(link = "logit"),
  control = glmerControl(optimizer ='optimx', 
                         optCtrl = list(method = 'nlminb')),
  data = df_logit,
  verbose = 0,
  weights = weight1500,
  na.action = na.omit)

summary(m_nlminb)
```



# Plotar

## Efectos aleatorios

```{r}
qqmath(ranef(m2, condVar = TRUE))
```

  
## Correlaciones

```{r}
df2corr <- df_logit[ , !names(df_logit) %in% c("pais", "weight1500")]
corr1 <- cor(df2corr, method = "spearman")
corrplot(corr1, method = "number")
```
  


# Otros modelos

## Modelo sin escalar todas las variables: no converge

```{r}
m <- lme4::glmer(
  formula = Comunitaria ~ Sexo + Edad + Educacion + Ingreso +
    Catolico + Protestante + Sin_relig + Evan_y_Pent + Otra_relig + 
    Blanca + Mestiza + Indigena + Negra + Mulata + Otra_etn +
    Simpatizar_partido + ind_confianza + Votar +
    gdp_growth +  scale_gdp_pc_ppp + log_pob + pp_urb_2010 +
    pp_electoral + cpi + tas_hom + indg_pob_2010 +
    ele_2011 + v_oblg + v_oblg_san + federalista + (1|pais),
  family = binomial(link = "logit"),
  control = glmerControl(optimizer = "bobyqa",
                         optCtrl = list(maxfun = 3e5)),
  data = df_logit,
  verbose = 0,
  weights = weight1500,
  na.action = na.omit)
summary(m)
```

## Extras

```{r}
m2_logit <- lme4::glmer(formula = Comunitaria ~ 1 + Sexo + Edad + Educacion + Ingreso +
                          Catolico + Protestante + Sin_relig + Evan_y_Pent + Otra_relig + 
                          Blanca + Mestiza + Indigena + Negra + Mulata + Otra_etn +
                          Simpatizar_partido + ind_confianza + Votar +
                          scale(gdp_growth) + scale(gdp_pc_ppp) + log_pob + pp_urb_2010 +
                          pp_electoral + scale(cpi) + scale(tas_hom) + indg_pob_2010 +
                          ele_2011 + v_oblg + v_oblg_san + federalista + (1|pais),
                        family = binomial(link = "logit"),
                        data = df,
                        verbose = 0,
                        weights = weight1500, # Es esta. disminuye más residuales: peso para darle el mismo tamaño a cada país
                        na.action = na.omit)
```


```{r}
m3_logit <- lme4::glmer(formula = Comunitaria ~ 1 + Sexo + Edad + Educacion + Ingreso +
                          Catolico + Protestante + Sin_relig + Evan_y_Pent + Otra_relig + 
                          Blanca + Mestiza + Indigena + Negra + Mulata + Otra_etn +
                          Simpatizar_partido + ind_confianza + Votar +
                          gdp_growth + gdp_pc_ppp + log_pob + pp_urb_2010 +
                          cpi + tas_hom + indg_pob_2010 +
                          v_oblg + federalista + (1|pais),
                        family = binomial(link = "logit"),
                        data = df_logit,
                        verbose = 0,
                        weights = weight1500, # Es esta. disminuye más residuales: peso para darle el mismo tamaño a cada país
                        na.action = na.omit)
```


```{r}
m3_probit <- lme4::glmer(Comunitaria ~ 1 + Sexo + Edad + Educacion + Ingreso +
                          Catolico + Protestante + Sin_relig + Evan_y_Pent + Otra_relig + 
                          Blanca + Mestiza + Indigena + Negra + Mulata + Otra_etn +
                          Simpatizar_partido + ind_confianza + Votar +
                          gdp_growth + gdp_pc_ppp + log_pob + pp_urb_2010 +
                          cpi + tas_hom + indg_pob_2010 +
                          v_oblg + federalista + (1|pais),
                        family = binomial(link = "probit"),
                        data = df_logit,
                        verbose = 0,
                        weights = weight1500, # Es esta. disminuye más residuales: peso para darle el mismo tamaño a cada país
                        na.action = na.omit)
```


```{r}
m4_logit <- lme4::glmer(formula = Comunitaria ~ 1 + Sexo + Edad + Educacion + Ingreso +
                          Catolico + Protestante + Sin_relig + Evan_y_Pent + Otra_relig + 
                          Blanca + Mestiza + Indigena + Negra + Mulata + Otra_etn +
                          Simpatizar_partido + ind_confianza + Votar +
                          scale(gdp_growth) + scale(gdp_pc_ppp) + log_pob + pp_urb_2010 +
                          pp_electoral + scale(cpi) + scale(tas_hom) + indg_pob_2010 +
                          ele_2011 + v_oblg + v_oblg_san + federalista + (1|pais),
                        family = binomial(link = "logit"),
                        control = glmerControl(optimizer = "bobyqa",
                                               optCtrl = list(maxfun = 2e5)),
                        data = df,
                        verbose = 0,
                        weights = weight1500, # Es esta. disminuye más residuales: peso para darle el mismo tamaño a cada país
                        na.action = na.omit)
```


```{r}
m5_logit <- lme4::glmer(formula = Comunitaria ~ 1 + Sexo + Edad + Educacion + Ingreso +
                          Catolico + Protestante + Sin_relig + Evan_y_Pent + Otra_relig + 
                          Blanca + Mestiza + Indigena + Negra + Mulata + Otra_etn +
                          Simpatizar_partido + ind_confianza + Votar +
                          scale(gdp_growth) + scale(gdp_pc_ppp) + log_pob + scale(pp_urb_2010) +
                          scale(pp_electoral) + scale(cpi) + scale(tas_hom) + scale(indg_pob_2010) + 
                          (1|pais),
                        family = binomial(link = "logit"),
                        control = glmerControl(optimizer = "bobyqa",
                                               optCtrl = list(maxfun = 2e5)),
                        data = df,
                        verbose = 0,
                        weights = weight1500, # Es esta. disminuye más residuales: peso para darle el mismo tamaño a cada país
                        na.action = na.omit)
```

```{r}
anova(m1_logit, m2_logit, m3_logit, m4_logit, m5_logit)
```





