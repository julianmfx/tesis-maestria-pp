---
title: "c14_multinivel"
author: "jmf"
date: "`r Sys.Date()`"
output:   
  html_document:
    toc: TRUE
    toc_float: TRUE
    theme: cosmo
    highlight: zenburn
---

Este código tiene como función encontrar el mejor modelo multinivel que represente los datos.

Necesito reorganizar el código según los apuntes de la reunión de hoy.

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)
```

# Librerías

```{r, message = FALSE}
library(dplyr)
library(ggplot2)
library(broom) # to use tidy()
# library(tidyverse) # to use ggplot. cuando actualizé R, no conseguí instalar tidyverse
library(labelled) # to deal with "haven_labelled" variables (it is automatical);
library(ggeffects) # to use ggpredict()
library(nlme)
library(lme4) # paquete original
library(lmerTest) # paquete con pvalues
library(fitdistrplus) # to use fitdist()
library(multilevel) # to try new functions
library(r2mlm) # to calculate r-squared. No funciona muy bien
library(performance) # to use icc()
library(sjPlot) # to use tab_model()
library(lattice) # to use qqmath()
library(see) # warning message from ggpredict
# library(merTools) # to use predictInterval()
library(patchwork) # to plot graphs with ggpredict
```


# Importando base de datos: modelo final ponderado y policlórico

```{r}
# datos <- readr::read_rds(
#   "C:/Users/L03529479/Documents/Julian/tesis-maestria_participacion-politica/bd_LAPOP/datos-procesados/logit/df_vars_democracia/multinivel_final.Rds")
```

# Base con indicador de confianza

```{r ind_conf}
datos <- readr::read_rds(
  "C:/Users/L03529479/Documents/Julian/tesis-maestria_participacion-politica/bd_LAPOP/datos-procesados/logit/df_vars_democracia/multinivel_final_pmm.Rds")
```


```{r}
glimpse(datos)
```

# Limpieza

Quitar NAs de la base. Dependiendo de la función utilizada es necesario tener un conjunto de datos sin valores faltantes.

La idea de crear un nuevo objeto permite eliminar los valores faltantes solamente de las variables que voy a utilizar

## Sexo

Como podemos ver, sexo está configurado de la siguiente manera:

  * 1 = Hombre
  * 2 = Mujer

```{r}
unique(datos$Sexo)
table(datos$Sexo, useNA = "ifany")
```

Lo voy a cambiar para:
  * 1 =  Hombre
  * 0 = Mujer

"0" ahora debe tener 12794 observaciones

```{r}
datos$Sexo[datos$Sexo == 2] <- 0
table(datos$Sexo, useNA = "ifany")
```

```{r}
glimpse(datos)
```

## Edad

```{r}
table(datos$Edad, useNA = "ifany")
```


## Simpatizar partido

Como podemos ver, simpatizar partido está codificado de una manera distinta a lo que quiero:
   
   * 1 = Sí
   * 2 = No

```{r}
unique(datos$Simpatizar_partido)
table(datos$Simpatizar_partido, useNA = "ifany")
```

Iré cambiar eso para NO == 0
0 = 17140
```{r}
datos$Simpatizar_partido[datos$Simpatizar_partido == 2] <- 0
table(datos$Simpatizar_partido, useNA = "ifany")
```


## Corrupción

```{r}
unique(datos$Corrupción)
table(datos$Corrupción, useNA = "ifany")
```
1 = 996
2 = 3873
3 = 9232
4 = 9721
NA = 1476

```{r}
datos$Corrupción[datos$Corrupción == 1] <- 10
datos$Corrupción[datos$Corrupción == 2] <- 9
datos$Corrupción[datos$Corrupción == 3] <- 8
datos$Corrupción[datos$Corrupción == 4] <- 7
datos$Corrupción[datos$Corrupción == 7] <- 1
datos$Corrupción[datos$Corrupción == 8] <- 2
datos$Corrupción[datos$Corrupción == 9] <- 3
datos$Corrupción[datos$Corrupción == 10] <- 4
table(datos$Corrupción, useNA = "ifany")
```



## Religion

Religion son diversas categorías:


1 Católico
2 Protestante
3                          Religiones Orientales no Cristianas
4 Ninguna
5 Evangélica y Pentecostal
6                    Iglesia de los Santos de los Últimos Días
7                                     Religiones Tradicionales
10                                                        Judío
11                                             Agnóstico o ateo
12                                           Testigos de Jehová
15                                                         Otro

```{r}
unique(datos$Religion)
table(datos$Religion, useNA = "ifany")
```

Se perden valores de ajuste al agregar categorías, creo que es normal porque se pierde información

### Católicos

```{r}
datos$Catolico <- 0
table(datos$Catolico) 
datos$Catolico[datos$Religion == 1] <- 1
table(datos$Catolico)
16320
```

### Protestantes

```{r}
datos$Protestante <- 0
table(datos$Protestante) 
datos$Protestante[datos$Religion == 2] <- 1
table(datos$Protestante)
1387
```

### Sin Religion
```{r}
datos$Sin_relig <- 0
table(datos$Sin_relig) 
datos$Sin_relig[datos$Religion == 4] <- 1
table(datos$Sin_relig)
2325
```

### Evangélicos y pentecostales

```{r}
datos$Evan_y_Pent <- 0
table(datos$Evan_y_Pent) 
datos$Evan_y_Pent[datos$Religion == 5] <- 1
table(datos$Evan_y_Pent)
3791
```

### Otra Religion
Otra Religion agrega:
* Religiones Orientales o no Cristinas
* Iglesia de los Santos de los Últimos Días
* Religiones tradicionales
* Judíos
* Agnósticos o ateos
* Testigos de Jehová
* Otro

```{r}
datos$Otra_relig <- 0
table(datos$Otra_relig) 
datos$Otra_relig[datos$Religion == 3 | datos$Religion == 6 | datos$Religion == 7 | datos$Religion == 10 | datos$Religion == 11 | datos$Religion == 12 | datos$Religion == 15] <- 1
table(datos$Otra_relig)
28 + 95 + 103 + 18 + 401 + 220 + 1
```


## Etnia

```{r}
unique(datos$Etnia)
table(datos$Etnia, useNA = "ifany")
```


### Blanca

```{r}
datos$Blanca <- 0
table(datos$Blanca) 
datos$Blanca[datos$Etnia == 1] <- 1
table(datos$Blanca)
7171
```


### Mestiza

```{r}
datos$Mestiza <- 0
table(datos$Mestiza) 
datos$Mestiza[datos$Etnia == 2] <- 1
table(datos$Mestiza) 
13043
```


### Indígena

```{r}
datos$Indigena <- 0
table(datos$Indigena) 
datos$Indigena[datos$Etnia == 3] <- 1
table(datos$Indigena) 
1599
```


### Negra

```{r}
datos$Negra <- 0
table(datos$Negra) 
datos$Negra[datos$Etnia == 4] <- 1
table(datos$Negra) 
1116
```


### Mulata

```{r}
datos$Mulata <- 0
table(datos$Mulata) 
datos$Mulata[datos$Etnia == 5] <- 1
table(datos$Mulata)
1143
```

### Otra
Otra representa otra (7) y amarela (1506)

```{r}
datos$Otra_etn <- 0
table(datos$Otra_etn) 
datos$Otra_etn[datos$Etnia == 7 | datos$Etnia == 1506] <- 1
table(datos$Otra_etn) 
311+49
```


1 - Blanca
2 - Mestiza
3 - Indigena
4 - Negra
5 - Mulata
7 - Otra
1506 - Amarela
NA

## Educación = OK
```{r}
unique(datos$Educacion)
table(datos$Educacion, useNA = "ifany")
```

## Ingreso = tiene muchos NAs
```{r}
unique(datos$Ingreso)
table(datos$Ingreso, useNA = "ifany")
```


## Quitar NAs

Depende de cuáles variables voy a utilizar

```{r}
# df <- na.omit(datos[ , c("Voz", "Comunitaria", "Partidista", "Protesta", # individuales
#                          "Sexo", "Edad", "Educacion", "Ingreso", 
#                          "Etnia", "Religion", "Votar", "Simpatizar_partido", 
#                          "Corrupción", "Tamano", 
#                          "gdp_growth", "gdp_pc_ppp", "log_pob", "ele_nac_2011", # nacionales
#                          "ele_reg_2011", "federalista", "cpi", "tas_hom", 
#                          "indg_pob_2010", "pais")]) 
```


# Creación

## Variable de confianza institucional = OK

Código `mice_imputation.R`
```{r ref.label = ind_conf}
```

  
## ele_2011

Juntar elecciones nacionales y Religionales = OK

```{r}
datos <- datos |>
  dplyr::mutate(ele_2011 = (ele_nac_2011 + ele_reg_2011))
datos$ele_2011[datos$ele_2011 == 2] <- 1
```

0 = 18242
1 = 7056
```{r}
table(datos$ele_2011)
```

```{r}
glimpse(datos)
```


## pp_electoral

Porcentaje de participación en las elección presidencial anterior


```{r}
table(datos$pais)
datos$pp_electoral <- 0
datos$pp_electoral[datos$pais == "AR"] <- 79.39
datos$pp_electoral[datos$pais == "BO"] <- 94.54
datos$pp_electoral[datos$pais == "BR"] <- 80.19
datos$pp_electoral[datos$pais == "CL"] <- 87.3
datos$pp_electoral[datos$pais == "CO"] <- 46.825
datos$pp_electoral[datos$pais == "CR"] <- 69.14
datos$pp_electoral[datos$pais == "EC"] <- 75.3
datos$pp_electoral[datos$pais == "SV"] <- 62.92
datos$pp_electoral[datos$pais == "GT"] <- 65.105
datos$pp_electoral[datos$pais == "HN"] <- 49.88
datos$pp_electoral[datos$pais == "MX"] <- 58.55
datos$pp_electoral[datos$pais == "NI"] <- 73.9
datos$pp_electoral[datos$pais == "PA"] <- 74.00
datos$pp_electoral[datos$pais == "PY"] <- 65.43
datos$pp_electoral[datos$pais == "PE"] <- 83.125
datos$pp_electoral[datos$pais == "DO"] <- 71.44
datos$pp_electoral[datos$pais == "UY"] <- 89.49
table(datos$pp_electoral)
```


## Voto obligatorio

Artículo de Chávez (2020)

### v_oblg: sin sanción

```{r}
table(datos$pais)
datos$v_oblg <- 0
datos$v_oblg[datos$pais == "AR"] <- 1
datos$v_oblg[datos$pais == "BR"] <- 1
datos$v_oblg[datos$pais == "BO"] <- 1
datos$v_oblg[datos$pais == "EC"] <- 1
datos$v_oblg[datos$pais == "PE"] <- 1
datos$v_oblg[datos$pais == "UY"] <- 1
datos$v_oblg[datos$pais == "CR"] <- 1
datos$v_oblg[datos$pais == "HN"] <- 1
datos$v_oblg[datos$pais == "MX"] <- 1
datos$v_oblg[datos$pais == "PA"] <- 1
datos$v_oblg[datos$pais == "PY"] <- 1
datos$v_oblg[datos$pais == "DO"] <- 1
table(datos$v_oblg)
1398+2650+1434+1233+1350+1438+1373+1393+1490+1465+1409+1469 # 18102 = OK
```


### v_oblg_san: con sanción

```{r}
table(datos$pais)
datos$v_oblg_san <- 0
datos$v_oblg_san[datos$pais == "AR"] <- 1
datos$v_oblg_san[datos$pais == "BO"] <- 1
datos$v_oblg_san[datos$pais == "BR"] <- 1
datos$v_oblg_san[datos$pais == "EC"] <- 1
datos$v_oblg_san[datos$pais == "PE"] <- 1
datos$v_oblg_san[datos$pais == "UY"] <- 1
table(datos$v_oblg_san)
1398+2650+1434+1233+1350+1438 # 9503 = OK
```

## pp_urb_2010: porcentaje de población urbana

Datos de CepalStat

```{r}
table(datos$pais)
datos$pp_urb_2010 <- 0
datos$pp_urb_2010[datos$pais == "AR"] <- 90.97405387
datos$pp_urb_2010[datos$pais == "BO"] <- 66.77470767
datos$pp_urb_2010[datos$pais == "BR"] <- 84.34865784
datos$pp_urb_2010[datos$pais == "CL"] <- 88.10295775
datos$pp_urb_2010[datos$pais == "CO"] <- 77.65474907
datos$pp_urb_2010[datos$pais == "CR"] <- 71.59210525
datos$pp_urb_2010[datos$pais == "EC"] <- 62.68486483
datos$pp_urb_2010[datos$pais == "SV"] <- 65.23294843
datos$pp_urb_2010[datos$pais == "GT"] <- 52.0239225
datos$pp_urb_2010[datos$pais == "HN"] <- 50.77136833
datos$pp_urb_2010[datos$pais == "MX"] <- 76.48389912
datos$pp_urb_2010[datos$pais == "NI"] <- 56.82951244
datos$pp_urb_2010[datos$pais == "PA"] <- 65.16724178
datos$pp_urb_2010[datos$pais == "PY"] <- 63.35992274
datos$pp_urb_2010[datos$pais == "PE"] <- 76.91950084
datos$pp_urb_2010[datos$pais == "DO"] <- 73.69199453
datos$pp_urb_2010[datos$pais == "UY"] <- 94.19087211
table(datos$pp_urb_2010)
```

```{r}
glimpse(datos)
```


# Modelo logit

```{r}
dplyr::glimpse(datos)
df_logit <- datos
```


## Voz

```{r}
df_logit$Voz[df_logit$Voz <= 0] <- 0
df_logit$Voz[df_logit$Voz > 0] <- 1

table(datos$Voz <= 0) # Voz <= 0 son 18905
table(datos$Voz > 0) # Voz > 0 son 6393

# 18905 + 6393 # n

table(df_logit$Voz)
```


## Comunitaria

```{r}
df_logit$Comunitaria[df_logit$Comunitaria <= 0] <- 0
df_logit$Comunitaria[df_logit$Comunitaria > 0] <- 1

table(datos$Comunitaria <= 0) # Comunitaria <= 0 son 15164
table(datos$Comunitaria > 0) # Comunitaria > 0 son 10134

# 15164 + 10134 # n

table(df_logit$Comunitaria)
```


## Partidista

```{r}
df_logit$Partidista[df_logit$Partidista <= 0] <- 0
df_logit$Partidista[df_logit$Partidista > 0] <- 1

table(datos$Partidista <= 0) # Partidista <= 0 son 19669
table(datos$Partidista > 0) # Partidista > 0 son 5629

# 19669 + 5629 # n

table(df_logit$Partidista)
```


## Protesta

```{r}
df_logit$Protesta[df_logit$Protesta <= 0] <- 0
df_logit$Protesta[df_logit$Protesta > 0] <- 1

table(datos$Protesta <= 0) # Protesta <= 0 son 18780
table(datos$Protesta > 0) # Protesta > 0 son 6518

# 18780 + 6518 # n

table(df_logit$Protesta)
```

## Transformar variables del modelo logit

### Escalar variables (centering?)

```{r}
df_logit$scale_Edad <- scale(df_logit$Edad)
df_logit$scale_Educacion <- scale(df_logit$Educacion)
df_logit$scale_Ingreso <- scale(df_logit$Ingreso)
df_logit$scale_ind_confianza <- scale(df_logit$ind_confianza)
df_logit$scale_gdp_growth <- scale(df_logit$gdp_growth)
df_logit$scale_gdp_pc_ppp <- scale(df_logit$gdp_pc_ppp)
df_logit$scale_log_pob <- scale(df_logit$log_pob)
df_logit$scale_pp_urb_2010 <- scale(df_logit$pp_urb_2010)
df_logit$scale_pp_electoral <- scale(df_logit$pp_electoral)
df_logit$scale_cpi <- scale(df_logit$cpi)
df_logit$scale_tas_hom <- scale(df_logit$tas_hom)
df_logit$scale_indg_pob_2010 <- scale(df_logit$indg_pob_2010)
```


### Sexo como factor



```{r}
levels(df_logit$Sexo)
table(df_logit$Sexo)
unique(df_logit$Sexo)

10133 # Mujer = 0
10075 # Hombre = 1

df_logit$Sexo_fact <- as.factor(df_logit$Sexo)
levels(df_logit$Sexo_fact)
table(df_logit$Sexo_fact)

levels(df_logit$Sexo_fact) <- list("Mujer" = "0",
                                   "Hombre" = "1",
                                   NA)

df_logit$Sexo_fact <- droplevels(df_logit$Sexo_fact)
table(df_logit$Sexo_fact)
```


### Educación como factor


Ninguno = 835
Primaria = 6022 # 365+610+950+767+824+2506
Secundaria = 10328 # 782+1095+1752+1070+2328+3301
Universitaria_M = 4145 # 615+739+838+818+558+577

```{r}
levels(df_logit$Educacion)
table(df_logit$Educacion)

df_logit$Ed_fact <- as.factor(df_logit$Educacion)
levels(df_logit$Ed_fact)
table(df_logit$Ed_fact)

  
levels(df_logit$Ed_fact) <- list("Ninguno" = "0",
                             "Primaria" = c("1", "2", "3", "4", "5", "6"),
                             "Secundaria" = c("7", "8", "9", "10", "11", "12"),
                             "Universitaria" = c("13", "14", "15", "16", "17", "18"),
                             NA)

835 # Ninguno
365+610+950+767+824+2506 # Primaria = 6022
782+1095+1752+1070+2328+3301 # Secundaria = 10328
615+739+838+818+558+577 # Universitaria_M = 4145

df_logit$Ed_fact <- droplevels(df_logit$Ed_fact)

table(df_logit$Ed_fact)
835+6022+10328+4145
```


### Ingreso como factor

Bajo = 388 + 361 + 861 + 1045 + 1539
Bajo/Mediano = 1832 + 1849 + 1860 + 1706
Mediano/Alto = 1864 + 1629 + 1398 + 1544
Alto = 1176 + 759 + 516 + 1003

```{r}
levels(df_logit$Ingreso)
table(df_logit$Ingreso)

df_logit$Ingreso_fact <- as.factor(df_logit$Ingreso)

levels(df_logit$Ingreso_fact)
table(df_logit$Ingreso_fact)

levels(df_logit$Ingreso_fact) <- list("Bajo" = c("0", "1", "2", "3", "4"), 
                               "Bajo/Mediano" = c("5", "6", "7", "8"),
                               "Mediano/Alto" = c("9", "10", "11", "12"),
                               "Alto" =  c("13", "14", "15", "16"),
                               NA)

df_logit$Ingreso_fact <- droplevels(df_logit$Ingreso_fact)
levels(df_logit$Ingreso_fact)

388 + 361 + 861 + 1045 + 1539 # Bajo
1832 + 1849 + 1860 + 1706 # Bajo/Mediano
1864 + 1629 + 1398 + 1544 # Mediano/Alto
1176 + 759 + 516 + 1003 # Alto

table(df_logit$Ingreso_fact)
```
### Simpatizar por partido como factor

```{r}
levels(df_logit$Simpatizar_partido)
table(df_logit$Simpatizar_partido)
unique(df_logit$Simpatizar_partido)

17140 # No = 0
7789 # Sí = 1

df_logit$SP_fact <- as.factor(df_logit$Simpatizar_partido)
levels(df_logit$SP_fact)
table(df_logit$SP_fact)

levels(df_logit$SP_fact) <- list("No" = "0",
                                 "Sí" = "1",
                                 NA)

df_logit$SP_fact <- droplevels(df_logit$SP_fact)
table(df_logit$SP_fact)
```

### Votar como factor

```{r}
levels(df_logit$Votar)
table(df_logit$Votar)
unique(df_logit$Votar)

4590 # No = 0
15618 # Sí = 1

df_logit$Votar_fact <- as.factor(df_logit$Votar)
levels(df_logit$Votar_fact)
table(df_logit$Votar_fact)

levels(df_logit$Votar_fact) <- list("No" = "0",
                                   "Sí" = "1",
                                   NA)

df_logit$Votar_fact <- droplevels(df_logit$Votar_fact)
table(df_logit$Votar_fact)
```
### Voto_oblg_san como factor

```{r}
levels(df_logit$v_oblg_san)
table(df_logit$v_oblg_san)
unique(df_logit$v_oblg_san)

12463 # No = 0
7745 # Sí = 1

df_logit$v_oblg_san_fact <- as.factor(df_logit$v_oblg_san)
levels(df_logit$v_oblg_san_fact)
table(df_logit$v_oblg_san_fact)

levels(df_logit$v_oblg_san_fact) <- list("No" = "0",
                                   "Sí" = "1",
                                   NA)

df_logit$v_oblg_san_fact <- droplevels(df_logit$v_oblg_san_fact)
table(df_logit$v_oblg_san_fact)
```
### v_oblg como factor

```{r}
levels(df_logit$v_oblg)
table(df_logit$v_oblg)
unique(df_logit$v_oblg)

7196 # No = 0
18102 # Sí = 1

df_logit$v_oblg_fact <- as.factor(df_logit$v_oblg)
levels(df_logit$v_oblg_fact)
table(df_logit$v_oblg_fact)

levels(df_logit$v_oblg_fact) <- list("No" = "0",
                                     "Sí" = "1",
                                     NA)

df_logit$v_oblg_fact <- droplevels(df_logit$v_oblg_fact)
table(df_logit$v_oblg_fact)
```

### ele_2011 como factor

```{r}
levels(df_logit$ele_2011)
table(df_logit$ele_2011)
unique(df_logit$ele_2011)

18242 # No = 0
7056 # Sí = 1

df_logit$ele_2011_fact <- as.factor(df_logit$ele_2011)
levels(df_logit$ele_2011_fact)
table(df_logit$ele_2011_fact)

levels(df_logit$ele_2011_fact) <- list("No" = "0",
                                       "Sí" = "1",
                                       NA)

df_logit$ele_2011_fact <- droplevels(df_logit$ele_2011_fact)
table(df_logit$ele_2011_fact)
```

### federalista como factor

```{r}
levels(df_logit$federalista)
table(df_logit$federalista)
unique(df_logit$federalista)

20976 # No = 0
4322 # Sí = 1

df_logit$federalista_fact <- as.factor(df_logit$federalista)
levels(df_logit$federalista_fact)
table(df_logit$federalista_fact)

levels(df_logit$federalista_fact) <- list("No" = "0",
                                          "Sí" = "1",
                                          NA)

df_logit$federalista_fact <- droplevels(df_logit$federalista_fact)
table(df_logit$federalista_fact)
```

### Tamaño como factor

```{r}
levels(df_logit$Tamano)
table(df_logit$Tamano)
unique(df_logit$Tamano)

df_logit$Tamano_fact <- as.factor(df_logit$Tamano)

table(df_logit$Tamano_fact)
levels(df_logit$Tamano_fact)

levels(df_logit$Tamano_fact) <- list("Capital_N" = "1", 
                               "C_Grande" = "2",
                               "C_Mediana" = "3",
                               "C_Pequena" =  "4",
                               "A_Rural" =  "5",
                               NA)

df_logit$Tamano_fact <- droplevels(df_logit$Tamano_fact)
levels(df_logit$Tamano_fact)

4694 # 1	Capital nacional			
3694 # 2	Ciudad grande			
3442 # 3	Ciudad mediana			
3087 # 4	Ciudad pequeña			
6413 # 5	Area rural

table(df_logit$Tamano_fact)

```

### Religión como factor

  1     2     3     4     5     6     7    10    11    12    15 
16320  1387    28  2325  3791    95   103    18   401   220     1 

```{r}
levels(df_logit$Religion)
table(df_logit$Religion)
unique(df_logit$Religion)



df_logit$Religion_fact <- as.factor(df_logit$Religion)

table(df_logit$Religion_fact)
levels(df_logit$Religion_fact)
levels(df_logit$Religion_fact) <- list("Católico" = "1", 
                               "Protestante" = "2",
                               "S_religion" = "4",
                               "Evang_Pent" =  "5",
                               "Otra_relig" =  c("3", "6", "7", "10", "11", "12", "15"),
                               NA)

df_logit$Religion_fact <- droplevels(df_logit$Religion_fact)
levels(df_logit$Religion_fact)

16320 # Católico
1387 # Protestante
2325 # Sin religión
3791 # Evangélicos y pentecostales
28 + 95 + 103 + 18 + 401 + 220 + 1 # Otra religion
table(df_logit$Religion_fact)
```


### Etnia como factor

```{r}
levels(df_logit$Etnia)
table(df_logit$Etnia)
unique(df_logit$Etnia)

df_logit$Etnia_fact <- as.factor(df_logit$Etnia)

table(df_logit$Etnia_fact)
levels(df_logit$Etnia_fact)
levels(df_logit$Etnia_fact) <- list("Blanca" = "1", 
                                    "Mestiza" = "2",
                                    "Indigena" = "3",
                                    "Negra" =  "4",
                                    "Mulata" =  "5",
                                    "Otra_etn" = c("7", "1506"),
                                    NA)

df_logit$Etnia_fact <- droplevels(df_logit$Etnia_fact)
levels(df_logit$Etnia_fact)

7171 # Blanca
13043 # Mestiza
1599 # Indigena
1116 # Negra
1143 # Mulata
311 + 49 # Otra Etnia
table(df_logit$Etnia_fact)
```

```{r}
table(is.na(df_logit))
```

```{r}
glimpse(df_logit)
```

```{r}
print("Modelos multiniveles generados!")
```

# EJECUTAR CÓDIGO

Revisando el código, hay varios puntos de confusión.
No sé si iré corregirlos, pero lo bueno que me acuerdo lo que estaba haciendo hasta aquí.

Lo que el código hace hasta aquí es tomar el conjunto de datos creados por otros códigos y generar dos modelos:
1. El primero se llama *datos*. Este es el conjunto de datos con todas las variables originales limpias y categorizadas. Además agrega algunas variables nacionales.
2. El segundo se llma *df_logit*. Este es el conjunto de datos que es generado para su uso en los modelos logit. Lo que estaba haciendo (y no se debería hacer) era generar ese conjunto de datos en este script y usarlo en los scripts de los modelos.

Para disminuir la confusión, exportaré los dos conjuntos de datos y los utilizaré para calcular algunas estadísticas descriptivas que me pidió Nieto.
1. El código *datos* será el **df_logit_base**, en que tiene todas las variables del df_logit, pero ellas no están transformadas a factor.
2. El código *df_logit* será el **df_logit_final**, en que todas las variables ya están transformadas y puede ser usadas en el modelo logit.

```{r}
glimpse(datos)
```

```{r}
glimpse(df_logit)
```


## Exportación

```{r}
saveRDS(object = datos,
file = "C:/Users/L03529479/Documents/Julian/tesis-maestria_participacion-politica/bd_LAPOP/datos-procesados/logit/df_vars_democracia/df_logit_base.rds")
saveRDS(object = df_logit,
file = "C:/Users/L03529479/Documents/Julian/tesis-maestria_participacion-politica/bd_LAPOP/datos-procesados/logit/df_vars_democracia/df_logit_final.rds")
```


# Preparación

 * Código de análisis del AIC, BIC y R²
 * Modelos nulos para los cuatro tipos
 * Modelos solo con variables individuales
 * Modelos solo con variables nacionales
 * Modelos con más o menos variables

## Ordenando la base de datos

```{r}
datos <- datos |>
  relocate(Voz, Comunitaria, Partidista, Protesta, pais, Sexo, Edad, Educacion, Ingreso, Etnia, Religion, gdp_growth, gdp_pc_ppp, log_pob, ele_nac_2011, ele_reg_2011, federalista, cpi, tas_hom, indg_pob_2010, Tamano, Urbano_Rural)
```

## Verificando NAs
Hay muchos en ingreso

```{r}
table(paste0(datos$Ingreso))
```


```{r}
sum(is.na(datos$Ingreso))
```

## Conociendo los datos

### Look at distribution by type

```{r}
datos |> 
  dplyr::select(Voz, Comunitaria, Partidista, Protesta) |> 
  tidyr::gather(key = tipo, value = Value, c(Voz, Comunitaria, Partidista, Protesta)) |> 
  transform(tipo = factor(tipo, levels = c("Voz", "Comunitaria", "Partidista", "Protesta"))) |> 
  ggplot(aes(Value)) + 
  geom_density() +
  geom_vline(xintercept = 0, linetype = "dotted") +
  facet_wrap( ~ tipo) +
  labs(x = "Puntaje de cada individuo",
       y = "Frecuencia de los puntajes") + 
  theme(
    strip.background = element_blank(),
      panel.background = element_blank(),
      panel.grid = element_blank(),
      legend.title = element_blank())
```

```{r}
df_logit |> 
  dplyr::select(Voz, Comunitaria, Partidista, Protesta) |> 
  tidyr::gather(key = tipo, value = Value, c(Comunitaria, Voz, Partidista, Protesta)) |> 
  group_by(tipo) |> 
  summarise(participa = sum(Value)) |>
  mutate(no_participa = (25298 - participa)) # |> 
  # ggplot() +
  # geom_point(aes(x = participa, y = 1:4, color = tipo)) + 
  # labs(x = "Puntaje del tipo de participación política",
  #      y = "Frecuencia del tipo de participación política") + 
  # theme(
  #   strip.background = element_blank(),
  #     panel.background = element_blank(),
  #     panel.grid = element_blank(),
  #     legend.title = element_blank())
```


### Look at distribution by country

```{r}
datos %>% 
  ggplot(aes(Voz)) + 
  geom_density() +
  facet_wrap(~pais)
```

```{r}
datos %>% 
  ggplot(aes(Comunitaria)) + 
  geom_density() +
  facet_wrap(~pais)
```

```{r}
datos %>% 
  ggplot(aes(Partidista)) + 
  geom_density() +
  facet_wrap(~pais)
```

```{r}
datos %>% 
  ggplot(aes(Protesta)) + 
  geom_density() +
  facet_wrap(~pais)
```


# Modelo nulos

```{r}
lm_voz <- gls(model = Voz ~ 1,
              data = datos,
              method = "REML",
              na.action = na.omit)
summary(lm_voz)
```

```{r}
AIC(lm_voz)
BIC(lm_voz)
AIC(null_voz)
BIC(null_voz)
```


## Voz

```{r}
null_voz <- lmerTest::lmer(Voz ~ 1 + (1 | pais),
                         data = datos,
                         REML = TRUE,
                         verbose = 0,
                         # weights = wt, # disminuye un poco los residuales: peso del país
                         # weights = weight1500, # Es esta. disminuye más residuales: peso para darle el mismo tamaño a cada país
                         na.action = na.omit,
                        )
summary(null_voz)
```
```{r}
performance::icc(null_voz)
```


```{r}
ICC <- (0.004381) / (0.004381 + 0.623681)
ICC
```

```{r}
mod1_sl <- lm(Voz ~ 1, 
              data = datos)
summary(mod1_sl)
```
El efecto país en Voz no es significativo

```{r}
anova(null_voz, mod1_sl)
```

## Partidista

```{r}
null_partd <- lmerTest::lmer(Partidista ~ 1 + (1 | pais),
                         data = datos,
                         REML = TRUE,
                         verbose = 0,
                         # weights = wt, # disminuye un poco los residuales: peso del país
                         weights = weight1500, # disminuye más residuales: peso para darle el mismo tamaño a cada país
                         na.action = na.omit,
                        )
summary(null_partd)
```

```{r}
performance::icc(null_partd)
```


```{r}
ICC <- (0.03254) / (0.03254 + 0.68382)
ICC
```

```{r}
partd_sl <- lm(Partidista ~ 1,
               data = datos)
summary(partd_sl)
```
El efecto país en partidista no es significativo

```{r}
anova(null_partd, partd_sl)
```


## Protesta

```{r}
null_prot <- lmerTest::lmer(Protesta ~ 1 + (1 | pais),
                         data = datos,
                         REML = TRUE,
                         verbose = 0,
                         # weights = wt, # disminuye un poco los residuales: peso del país
                         weights = weight1500, # disminuye más residuales: peso para darle el mismo tamaño a cada país
                         na.action = na.omit,
                        )
summary(null_prot)
```

```{r}
performance::icc(null_prot)
```


```{r}
ICC <- (0.01812) / (0.01812 + 0.75492)
ICC
```

```{r}
prot_sl <- lm(Protesta ~ 1,
              data = datos)
summary(prot_sl)
```

El efecto país no es significativo para el tipo protesta

```{r}
anova(null_prot, prot_sl)
```


```{r}
anova(null_voz, null_com, null_partd, null_prot)
```



# OLD De aquí adelante las partes serán llevadas a otros códigos

# Coeficientes fijos

## País como fijo

* Fixed-effect model matrix is rank deficient: es necesario quitar 8 países del análisis

```{r}
modelo <- lmerTest::lmer(Voz ~ Sexo + Edad + Educacion + Ingreso + Etnia + Religion + Votar + Simpatizar_partido + Corrupción + scale(gdp_growth) + log_pob + ele_nac_2011 + ele_reg_2011 + federalista + scale(cpi) + scale(tas_hom) + scale(indg_pob_2010) + pais + (1 | pais), data = datos, na.action = na.omit)
```

 
### Individuales y país
```{r}
modelo <- lmerTest::lmer(Voz ~ Sexo + Edad + Educacion + Ingreso + Etnia + Religion + Votar + Simpatizar_partido + + pais + (1 | pais), data = datos, na.action = na.omit)
summary(modelo)
```

### Nacionales y país

* Fixed-effect model matrix is rank deficient: es necesario quitar 8 países del análisis
* Solo con las variables nacionales no funciona. El problema es mezclar como efectos fijos variables nacionales y el país.

```{r}
modelo <- lmerTest::lmer(Partidista ~ pais + (1 | pais),
                         data = datos, 
                         na.action = na.omit)
summary(modelo)
```

## Remover tamaño cambia mucho del modelo

Interesante ver que al incluir algunos coeficientes aleatorios hay una correlación muy alta con el coeficiente aleatorio del país.
También que al incluir como coeficientes aleatorios ele_nac_2011, gpd_pc_ppp, el modelo cambia bastante.


# Plotando los residuales

We see that this model allows each country to have a different average support for immigration through the random effect. We could visualize these random effects in a different way, using a dotplot (here I use the lattice package for convenience, it’s possible to do this in ggplot but slightly more code). We use the qqmath() command with the random effects from the model (extracted using ranef() command).

### dotplot using lattice package

```{r}
qqmath(ranef(null_voz, condVar = TRUE))
```

# Centering

No sé cuando ni si será necesario.

```{r}
# CenteredVar <- data$var - mean(data$var)
```



# use of step(). Plot isn't working
```{r}
# s <- lmerTest::step(modelo) # elimination of non-significant effects
# s
# plot(s) #plot of post-hoc analysis of the final model
```


# Modelos factorizados

## Regresiones

### Null model or Unconditional Means Model


```{r}
modelo <- lmerTest::lmer(formula = Comunitaria ~ 1 + (1 | pais),
                         data =  datos,
                         na.action = na.omit)
summary(modelo)
```

```{r}
a <- (0.08225) / (0.08225 + 0.91215)
a
```


```{r}
modelo <- lmerTest::lmer(formula = Comunitaria ~ 1 + (1 | pais),
                         data =  datos,
                         na.action = na.omit)
summary(modelo)
```

Partidista

```{r}
modelo <- lmerTest::lmer(formula = Partidista ~ 1 + (1 | pais),
                         data =  datos,
                         na.action = na.omit)
summary(modelo)
```

Protesta

```{r}
modelo <- lmerTest::lmer(formula = Protesta ~ 1 + (1 | pais),
                         data =  datos,
                         na.action = na.omit)
summary(modelo)
```

```{r}
modelo <- nlme::lme(fixed = Voz ~ 1, random = ~ 1 | pais, data = datos, na.action = na.omit)
intervals(modelo)
VarCorr(modelo)
```



#### Es con la varianza o el desvión patrón?

 * Todo indica que es con la varianza
El libro dice que es con la varianza, pero lo hace con el desvío patrón

```{r}
a <- 0.08007667 / (0.08007667 + 0.9970633)
a
rho <- 0.006412 / (0.006412 + 0.994135)
rho
```


```{r}
modelo <- lmer(formula = Voz ~ 1 + (1|pais), data = datos)
modelo
summary(modelo)
```

### Extract the random effects

```{r}
VarCorr(modelo)
```


```{r}
VarCorr(modelo)
RE <- as.data.frame(VarCorr(modelo))
```


### Compute Intra Class Correlation

Correlation of "Voz" within the same country is 0.07
```{r}
rho <- 0.006412 / (0.006412 + 0.994135)
rho
```

## Modelo con una variable predictora

```{r}
modelo <- lmerTest::lmer(formula = Voz ~ Ingreso +  (1|pais), data = datos, na.action = na.omit)
summary(modelo)
```
```{r}
modelo <- lme(fixed = Voz ~ Ingreso, random = ~ 1 | pais, data = datos, na.action = na.omit)
summary(modelo)
intervals(modelo)
```





## Corrupción

```{r}
levels(datos$Corrupción)
datos$Corrupción <- factor(datos$Corrupción,
                           levels = c("Nada generalizada", "Poco generalizada", "Algo generalizada", "Muy generalizada"))
levels(datos$Corrupción)
```

```{r}
modelo <- lmerTest::lmer(Comunitaria ~ scale(gdp_growth) + scale(gdp_pc_ppp) + log_pob + ele_nac_2011 + ele_reg_2011 + federalista + scale(cpi) + scale(tas_hom) + scale(indg_pob_2010) + (1|pais), data = datos, na.action = na.omit)
summary(modelo)
```

```{r}
modelo <- lmerTest::lmer(Comunitaria ~ scale(gdp_growth) + scale(gdp_pc_ppp) + log_pob + ele_nac_2011 + ele_reg_2011 + federalista + scale(cpi) + scale(tas_hom) + scale(indg_pob_2010) + (1|pais), data = datos, na.action = na.omit)
summary(modelo)
```




## Prueba del indicar de confianza institucional
```{r}
# modelo2 <- lmerTest::lmer(Voz ~ 1 + Sexo + Edad + Educacion + Ingreso + Etnia + Religion + Votar + Tamano + Simpatizar_partido + Corrupción + ind_confianza + scale(gdp_growth) + scale(gdp_pc_ppp) + log_pob + ele_nac_2011 + ele_reg_2011 + federalista + scale(cpi) + scale(tas_hom) + scale(indg_pob_2010) + (1 | pais), data = datos, na.action = na.omit)
# 
# summary(modelo2)
```

Mejoró muy poquito el modelo tan poco que es posible decir que ha empeorado
```{r}
# AIC(modelo)
# BIC(modelo)
# AIC(modelo2)
# BIC(modelo2)
# anova(modelo, modelo2)
```



Tengo que hacer comparación de anovas

```{r}
modelo <- lmerTest::lmer(Comunitaria ~ 1 + Sexo + Edad + Educacion + Ingreso + Etnia + Religion + Votar  + Simpatizar_partido + ind_confianza + scale(gdp_growth) + scale(gdp_pc_ppp) + pp_urb_2010 + ele_nac_2011 + pp_electoral + federalista + scale(cpi) + scale(tas_hom) + scale(indg_pob_2010) + (1 | pais), data = datos, na.action = na.omit)

summary(modelo)
```

## Apresentación


### Voto obligatorio parece que tiene un efecto en la voz política
```{r}
modelo <- lmerTest::lmer(Voz ~ 1 + Sexo + Edad + Educacion + Ingreso + Etnia + Religion + Votar + Simpatizar_partido + ind_confianza + scale(gdp_growth) + scale(gdp_pc_ppp) + pp_urb_2010 + v_oblg + pp_electoral + federalista + scale(cpi) + scale(tas_hom) + scale(indg_pob_2010) + (1 | pais), data = datos, na.action = na.omit)

summary(modelo)
```

### Voz

```{r modelo}
modelo <- lmerTest::lmer(Voz ~ 1 + Sexo + Edad + Educacion + Ingreso + Etnia + Religion + Votar + Tamano + Simpatizar_partido + Corrupción + scale(gdp_growth) + scale(gdp_pc_ppp) + log_pob + ele_nac_2011 + ele_reg_2011 + federalista + scale(cpi) + scale(tas_hom) + scale(indg_pob_2010) + (1 | pais), data = datos, na.action = na.omit)

summary(modelo)
```

```{r}
ranef(modelo)
```

```{r}
AIC(modelo)
BIC(modelo)
logLik(modelo)
```


```{r}
confint.merMod(modelo) # funcionó bien para el modelo con varias variables
```

### Comunitaria

```{r}
modelo <- lmerTest::lmer(Comunitaria ~ Sexo + Edad + Educacion + Ingreso + Etnia + Religion + Votar + Tamano + Simpatizar_partido + Corrupción + scale(gdp_growth) + scale(gdp_pc_ppp) + log_pob + ele_nac_2011 + ele_reg_2011 + federalista + scale(cpi) + scale(tas_hom) + scale(indg_pob_2010) + (1|pais), data = datos, na.action = na.omit)
summary(modelo)
```

### Partidista

```{r}
modelo <- lmerTest::lmer(Partidista ~ Sexo + Edad + Educacion + Ingreso + Etnia + Religion + Votar + Tamano + Simpatizar_partido + Corrupción + scale(gdp_growth) + scale(gdp_pc_ppp) + log_pob + ele_nac_2011 + ele_reg_2011 + federalista + scale(cpi) + scale(tas_hom) + scale(indg_pob_2010) + (1|pais), data = datos, na.action = na.omit)
summary(modelo)
```

### Protesta

```{r}
modelo <- lmerTest::lmer(Protesta ~ Sexo + Edad + Educacion + Ingreso + Etnia + Religion + Votar + Tamano + Simpatizar_partido + Corrupción + scale(gdp_growth) + scale(gdp_pc_ppp) + log_pob + ele_nac_2011 + ele_reg_2011 + federalista + scale(cpi) + scale(tas_hom) + scale(indg_pob_2010) + (1|pais), data = datos, na.action = na.omit)
summary(modelo)
```

### Probando cambiar la tasa de población indígena de República dominicana. Cambia muy poco los resultados

```{r}
datos <- datos |> 
  mutate(indg_pob_2010 = case_when(
    indg_pob_2010 == 12.6 ~ 1,
    TRUE ~ indg_pob_2010))
```


```{r}
datos |> 
  group_by(pais) |> 
  filter(pais == "DO") #|> 
  mutate(indg_pob_2010 = 1)
```


## En regresión lineal, los valores estatales son importantes

a <- lm(Voz ~ Sexo + Edad + Educacion + Ingreso + Etnia + Religion + Votar + Tamano + scale(gdp_growth) + scale(gdp_pc_ppp) + log_pob + ele_nac_2011 + ele_reg_2011 + federalista + scale(cpi) + scale(tas_hom) + scale(indg_pob_2010) + Simpatizar_partido + pais, data = datos, na.action = na.omit)

summary(a)

a <- lm(Voz ~ scale(gdp_growth) + scale(gdp_pc_ppp) + log_pob + ele_nac_2011 + ele_reg_2011 + federalista + scale(cpi) + scale(tas_hom) + scale(indg_pob_2010) + pais, data = datos, na.action = na.omit)

summary(a)

## En la multinivel no

```{r}
modelo <- lme4::lmer(Voz ~ Sexo + Edad + Educacion + Ingreso + Etnia + Religion + Votar + Tamano + Simpatizar_partido + scale(gdp_growth) + scale(gdp_pc_ppp) + log_pob + ele_nac_2011 + ele_reg_2011 + federalista + scale(cpi) + scale(tas_hom) + (1 + scale(indg_pob_2010) + (1|pais), data = datos, na.action = na.omit)
summary(modelo)
ranef(modelo)
```



```{r}
a <- (0.02243) / (0.02243 + 0.89297)
a
```

# fitdist

```{r}
normal <- fitdist(datos$Voz, 'norm')
logis <- fitdist(datos$Voz, 'logis')
cauchy <- fitdist(datos$Voz, 'cauchy')
unif <- fitdist(datos$Voz, 'quasinorm')
descdist(datos$Voz, boot = 1000) # beta distribution
```

# To use multilevel package

Models can't have NAs

```{r}
a <- rgr.ols(xdat1 = d.new$Ingreso, xdat2 = d.new$Sexo, ydata = d.new$Protesta, grpid = d.new$pais, nreps = 100)
summary(a)


b <- rgr.waba(x = d.new$Ingreso, y = d.new$Sexo, grpid = d.new$pais, nrep = 100)
summary(b)
quantile(b,c(.025,.975))

c <- rgr.agree(x = d.new$cpi, grpid = d.new$pais, nrangrps = 100)
summary(c)
```


```{r}
1 / datos$Edad
```


