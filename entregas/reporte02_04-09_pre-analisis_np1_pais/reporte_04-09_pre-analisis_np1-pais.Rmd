---
title: "Preanalisis descriptivo de LAPOP del año de 2012 - variable NP1"
author: "Julian Mokwa Félix"
date: "2022-09-06"
output:
  html_document:
    toc: TRUE
    toc_float: TRUE
    theme: cosmo
    highlight: zenburn

---

<!-- setwd("/home/julian/Documents/tesis-maestria_participacion-politica/entregas/reporte02_04-09_pre-analisis_np1_pais") -->

<!-- print(getwd()) -->

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE, comment = NA)
```

# Introducción

Este documento es un análisis inicial de la variable "NP1" referente a la encuesta de LAPOP del año de 2012. Para el análisis, fue utilizada la base de datos regional [LAPOP](https://www.vanderbilt.edu/lapop/index.php/), la cual incluye todos los casos y observaciones del año analizado. La variable NP1 busca medir la asistencia del individuo a un espacio público municipal. El fraseo de la variable es: "¿Ha asistido a un cabildo abierto o una sesión municipal/asamblea municipal/sesión del concejo municipal durante los últimos 12 meses?".

<!-- 1. Descargar las bases de datos de lapop según el [plan de gestión de datos de la tesis](https://docutopia.tupale.co/hQ7WGtUNRjOqRQik5QlWUA?view). -->

## Librerías utilizadas

```{r librerias, results = "hide", message=FALSE, warning=FALSE}

library(dplyr) # to use pipe operators
library(haven) # to import database
library(tidyverse) # to use ggplot

```

## Importación de la base de datos

```{r import}

lapop12 <- read_sav(
  "/home/julian/Documents/tesis-maestria_participacion-politica/bd_LAPOP/datos-brutos/regional-merge-files/2012/data-set_AmericasBarometer Merged 2012 Spanish Rev1.5_W.sav") %>%
  as.data.frame()

```

# Limpieza de los datos

## Factorizando la variable "np1"

La clase de la variable no está bien definida debido a dos razones. La primera, la importación de la base de datos a través del paquete [haven](https://cran.r-project.org/web/packages/haven/haven.pdf). Segundo, la base de datos original contiene etiquetas (**labels**) asociadas a los valores de la variable y que resulta en varios tipos, como "double" y "character", los cuales no son óptimos para realizar análisis. Podemos verificar la estructura de la variable np1, la cual se repite para todas la otras variables. La única diferencia significativa entre las estructuras de las variables de la base de datos está en la codificación de las respuestas. Por ello y debido a que el dataframe es muy grande, no será mostrada la estructura entera del dataframe y solamente de la variable seleccionada.

```{r}
str(lapop12$np1)
```

Así, son verificadas las clases, los valores únicos de la variable y la tabla de frecuencia antes y después para confirmar que la transformación de la variable fue existosa.

```{r}
class(lapop12$np1)
unique(lapop12$np1)
table(lapop12$np1)
```

En ese sentido, se hace necesario factorizar la variable a fin de transformarla en un tipo de dato que es más adecuado para realizar análisis, debido a su generalidad de lectura por las librerías y por facilitar la organización de la información. Esa variable también contiene etiqueta en formas de niveles (*levels*), los cuáles serán necesarios remover para un posterior análisis.

```{r}
lapop12$np1 <- as_factor(lapop12$np1)
class(lapop12$np1)
table(lapop12$np1)
unique(lapop12$np1)
```
## Colapsando los valores de "NS", "NR" y "N/A"
Como iremos trabajar con esos tres valores juntos, se hace necesario renombrar las etiquetas y los niveles de la variable para solamente tres. Las etiquetas y niveles "Sí" y "No", siguen siendo las mismas, pero las correspondentes a "NS", "NR" y "N/A" serán sumadas y cambiadas a una denominada de "NA".
```{r}
levels(lapop12$np1) <- list("Sí" = "Sí", "No" = "No", "NA" = c("NS", "NR", "N/A"))
```
Verificación del proceso de cambio:
```{r}
class(lapop12$np1)
table(lapop12$np1)
unique(lapop12$np1)
```
Además, también se puede sumar los valores de "NS", "NR" y "N/A" y se llega al mismo resultado de 1108.

```{r}
251+107+750
```

## Limpiando la variable "país"

Lo mismo ocurre con la variable país.

```{r}
class(lapop12$pais)
unique(lapop12$pais)
table(lapop12$pais)
```

La información es verificada para ver si la transformación fue exitosa.

```{r}
lapop12$pais <- as_factor(lapop12$pais)
class(lapop12$pais)
table(lapop12$pais)
unique(lapop12$pais)
```

## Quitando los países no seleccionados la de base de datos

Según el análisis de las encuestas entre 2012 y 2018, fueron seleccionados 17 países porque eran ellos que se mantenían en las encuestas en todos esos años. Ellos son: México, Guatemala, El Salvador, Honduras, Nicaragua, Costa Rica, Panamá, Colombia, Ecuador, Perú, Bolivia, Paraguay, Chile, Uruguay, Brasil, Argentina y República Dominicana. Por lo tanto, de la encuesta de 2012, que contiene 26 países, serán quitados 9 países del análisis. Según el código que están codificados en la base da datos, los países son presentados en la tabla abajo:

| Código | País              |
|--------|-------------------|
| 3      | Belice            |
| 5      | Canadá            |
| 13     | Guyana            |
| 14     | Haití             |
| 15     | Jamaica           |
| 21     | Surinam           |
| 22     | Trinidad y Tobago |
| 24     | Estados Unidos    |
| 25     | Venezuela         |

Los países son removidos por medio de la modificación del conjunto original de datos. El conjunto de datos es reasignado mediante un filtro que **no contenga** los países de la tabla recién presentada. También, solamente retirar los países del conjunto de datos no es suficiente, puesto que ellos poseen niveles que permanecen en la base. Así, es necesario retirar los niveles de las variables a partir de la función "droplevels()".

```{r limpiando-pais}

lapop12 <- lapop12 %>% 
  group_by(pais) %>% 
  filter(!pais %in% c("Belice", "Canadá", "Guyana", "Haití", "Jamaica", "Surinam",
                   "Trinidad y Tobago", "Estados Unidos", "Venezuela"))

lapop12 <- droplevels(lapop12)
```

# Análisis por frecuencia y porcentaje

Son creadas 4 nuevos dataframes para la variable np1 según su frecuencia y porcentaje: i) el primero contiene el total de observaciones; ii) el segundo agrupa esas observaciones por país, iii) el tercero sirve para exportar la tabla que sigue adjunta a este reporte y iv) el cuarto ejemplifica el caso mexicano.

```{r base-generator}
np1_total <- lapop12 %>%
  dplyr::group_by(np1) %>%
  dplyr::summarise(Frequency = n()) %>%
  dplyr::mutate(Percent = round(Frequency/sum(Frequency)*100, 1))
  np1_total

np1_pais <- lapop12 %>%
  dplyr::group_by(pais, np1) %>%
  dplyr::summarise(Frequency = n(), .groups="drop_last") %>%
  dplyr::mutate(Percent = round(Frequency/sum(Frequency)*100, 1))
  np1_pais

np1_pais_transform <- lapop12 %>%
  dplyr::group_by(pais, np1) %>%
  dplyr::summarise(Frequency = n(), .groups="drop_last") %>%
  dplyr::mutate(Percent = round(Frequency/sum(Frequency)*100, 1)) %>% 
  pivot_wider(id_cols = pais,
              names_from = np1,
              values_from = c(Frequency, Percent))
  # Exportar la tabla que seguirá adjunta a este reporte
  write.table(np1_pais_transform, file = "np1_pais_transform.csv", sep = ",", quote = FALSE, row.names = F)
  np1_pais_transform


np1_MX <- lapop12 %>%
  dplyr::filter(pais == "México") %>% 
  dplyr::group_by(np1) %>%
  dplyr::summarise(Frequency = n()) %>%
  dplyr::mutate(Percent = round(Frequency/sum(Frequency)*100, 1))
  np1_MX
```

# Visualización

## Histogramas

Con el caso méxico, se generó un histograma.

```{r hist-mx}
np1_pais %>%
  filter(pais == "México") %>% 
  ggplot(aes(np1, Percent, fill = np1)) + 
  # el segundo argumento identifica el eje Y como porcentaje
  geom_bar(stat="identity",  position = position_dodge()) +
  geom_text(aes(y = Percent, label = Percent), nudge_y = 2, color = "black",
             size=3) +
  theme_bw() +
  theme(legend.position="none")

```

## Probando grids and wraps

Para los gráficos finales, fue utilizado facet_wrap() de ggplot.

### Frecuencia por país

```{r}
np1_frecuencia_por_pais <- np1_pais %>%
  ggplot(aes(np1, Percent,
             fill = np1,
             color = np1,
             group=1,
             )) +
  facet_wrap(~pais, 
             ncol = 6,
             ) +
  scale_y_continuous(limits = c(0, NA)) +
  geom_bar(stat="identity", position=position_dodge()) +
  labs(x = NULL, 
       y = "Número de respuestas por país",
       title = "NP1: ¿Ha asistido a un cabildo abierto o una sesión municipal/asamblea municipal/sesión del \nconcejo municipal durante los últimos 12 meses?") +
  geom_text(aes(label=Frequency), nudge_y = 8, color = "black",
             size=2.5) +
  scale_color_brewer(palette="Pastel1") + 
  scale_fill_brewer(palette="Pastel1") +
  theme(
    strip.placement = "outside",
    strip.text.x = element_text(hjust = 0.5, vjust = 0.5),
    strip.background = element_blank(),
    panel.background = element_blank(),
    panel.grid = element_blank(),
    axis.text.x = element_blank(),
    axis.text.x.bottom = element_blank(),
    axis.text.x.top = element_blank(),
    axis.title.x.bottom = element_blank(),
    axis.ticks.x = element_blank(),
    axis.text.y = element_blank(), 
    axis.ticks.y = element_blank(),
    legend.title = element_blank(),
    legend.text = element_text(size = 8),
    plot.title = element_text(size = 10)
    )
  np1_frecuencia_por_pais
```

### Porcentaje por país

Los nombres de los países eran demasiados largos para el gráfico y, por lo tanto, se decidió cambiarlos por sus siglas con códigos de 2 letras.

```{r}
levels(np1_pais$pais)
levels(np1_pais$pais) <- c("MX", "GT", "SV", "HN", "NI", "CR", "PA", "CO", "EC", 
                           "BO", "PE", "PY", "CL", "UY", "BR", "AR", "DO")
levels(np1_pais$pais)
```

Fue utilizado facet_wrap() de ggplot para construir el gráfico.

```{r}
np1_facet_wrap <- np1_pais %>%
  ggplot(aes(np1, Percent,
             fill = np1,
             color = np1,
             group=1,
             )) +
  facet_wrap(~pais,
             ncol = 17,
             ) +
  scale_y_continuous(limits = c(0, NA)) +
  geom_bar(stat="identity", position=position_dodge()) +
  labs(x = NULL,
       y = "Porcentaje de las respuestas (%)",
       title = "NP1: ¿Ha asistido a un cabildo abierto o una sesión municipal/asamblea municipal/\nsesión del concejo municipal durante los últimos 12 meses?") +
  geom_hline(aes(yintercept = 90, linetype = "90%"), size = 0.35, color = "blue") +
  geom_hline(aes(yintercept = 10, linetype = "10%"), size = 0.35, color = "red") +
  scale_linetype_manual(name = "limit",  
                        values = c(2, 2),
                        guide = guide_legend(reverse = TRUE, 
                                           override.aes = list(color = c("blue", "red")))) +
  scale_color_brewer(palette="Pastel1") + 
  scale_fill_brewer(palette="Pastel1") +
  theme(
    strip.placement = "outside",
    strip.text.x = element_text(hjust = 0.5, vjust = 0.5),
    strip.background = element_blank(),
    panel.background = element_blank(),
    panel.grid = element_blank(),
    axis.text.x = element_blank(),
    axis.text.x.bottom = element_blank(),
    axis.text.x.top = element_blank(),
    axis.title.x.bottom = element_blank(),
    axis.ticks.x = element_blank(),
    axis.ticks.y = element_blank(),
    legend.title = element_blank(),
    legend.text = element_text(size = 8),
    plot.title = element_text(size = 10)
    )
   np1_facet_wrap
```
