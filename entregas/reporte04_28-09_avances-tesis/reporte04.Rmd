---
title: "reporte04"
author: "jmf"
date: '`r Sys.Date()`'
output:
  word_document:
    toc: yes
  pdf_document:
    toc: yes
header-includes:
- \usepackage{pdfpages}
- \usepackage{floatrow}
- \floatsetup[figure]{capposition=top}
---

<!-- setwd("/home/julian/Documents/tesis-maestria_participacion-politica/codigo/c02_limpieza") -->

```{r global_options, include=FALSE}
knitr::opts_chunk$set(echo = TRUE, comment = NA, fig.pos = 'h')
```

# Introducción
Este reporte está separado en tres partes. La primera constituye un análisis estadístico sobre las variables seleccionadas en la base de datos regional de LAPOP del año de 2012. La segunda parte es la revisión teórica que fue realizada hasta ahora para la tesis. La tercera parte concluye.

1) Preguntas: weighting scheme?

# Limpieza de la variable país

Librerías utilizadas
```{r librerias, results = "hide", message=FALSE, warning=FALSE}

library(dplyr) # to use pipe operators
library(haven) # to import database
library(tidyverse) # to use ggplot
library(janitor) # to use compare_df_cols()
library(vetr) # to use alike()
library(diffdf) # to use diffdf()

```
Importación de la base de datos original
```{r import}

lapop12 <- read_sav(
  "/home/julian/Documents/tesis-maestria_participacion-politica/bd_LAPOP/datos-brutos/regional-merge-files/2012/data-set_AmericasBarometer Merged 2012 Spanish Rev1.5_W.sav") %>%
  as.data.frame()

```

#### Factorización y selección de los países de interés

La variable país debe ser factorizada para que sea posible realizar análisis posteriores.

La factorización de la variable es realizada. La función as_factor() que, a diferencia de la función as.factor(), permite mantener las etiquetas (*labels*) de las variables.

```{r}
lapop12$pais <- as_factor(lapop12$pais)
lapop12_pais <- lapop12 %>% 
  group_by(pais) %>% 
  filter(!pais %in% c("Belice", "Canadá", "Guyana", "Haití", "Jamaica", "Surinam",
                   "Trinidad y Tobago", "Estados Unidos", "Venezuela"))
lapop12_pais <- droplevels(lapop12_pais)
```

#### Exportación y prueba de comparación entre dataframes
El dataframe será exportado para la carpeta de datos-procesados como nombre de "lapop12_pais.Rds". A partir de ello, serán realizadas las nuevas limpiezas.

```{r}
readr::write_rds(lapop12_pais, file = "/home/julian/Documents/tesis-maestria_participacion-politica/bd_LAPOP/datos-procesados/lapop12_pais.Rds")
```

El dataframe exportado es leído como un nuevo objeto para checar si no hubo problemas de exportación.
```{r}
lapop12_pais_test <- readr::read_rds(
  "/home/julian/Documents/tesis-maestria_participacion-politica/bd_LAPOP/datos-procesados/lapop12_pais.Rds")
```

El proceso de comparación utiliza las funciones all_equal, compare_df_cols, alike y Diffdf. En todos los casos, no hubó problemas de exportación y los dataframes son identicos.
```{r}
all_equal(lapop12_pais, lapop12_pais_test)
compare_df_cols(lapop12_pais, lapop12_pais_test, return = "mismatch", bind_method = "rbind")
alike(lapop12_pais, lapop12_pais_test)
diffdf(lapop12_pais, lapop12_pais_test)
```




# Análisis estadístico
El análisis estadístico fue realizado en 24 variables del dataframe. En primer lugar, se realizó la limpieza del dataframe para seleccionar solamente los países de interés. A partir de la limpieza de la variable país, se generó un nuevo dataframe que fue utilizado como base para el análisis de todas las otras variables.

## Análisis de las 24 variables
Para presentar de las variables, se establecieron 7 bloques de acuerdo a dos criterios: el primero se fundamentó en la pregunta realizada para medir el tipo de acción política a través del verbo, es decir, verbos como "ayudar", "asistir", están en bloques iguales; la segunda se basó en el tipo de respuesta, es decir, respuestas de "sí" o "no" o de "frecuencia", como la frecuencia en que la persona ha realizado determinada actividad. La tabla resumiendo esa información se encuentra abajo:

**COPIAR TABLA DEL EXCEL**

El análisis de todas las variables tiene como objetivo enseñar las frecuencias totales y porcentuales de cada respuesta de la variable, separadas por país, con excepción de la variable referente a edad, cuyo objetivo es enseñar la distribución de la edad de la encuesta seleccionada. Todos los gráficos están disponibles en el archivo adjunto, así como las tablas de frecuencia y porcentaje de cada variable. Es importante señalar que las líneas horizontales (que interceptan el eje Y) representan el valor promedio de la respuesta de la variable para todos los 17 países.

## Bloque 1
El primer bloque se refiere al contacto que los ciudadanos tienen con la arena formal de la política, como el Congreso, órganos, oficinas y agentes tanto federales como municipales, según una respuesta positiva o negativa. Los promedios generales sugieren que las personas tienen más contacto con la división municipal de poder, la cuál tiende a estar más cerca a ellos.

Ningún país presenta una tendencia significativa en ese bloque, cada uno tiene una variación distinta. Algunos países que se asemejan en este bloque son México, Honduras, Panamá, Bolivia, Paraguay y Brasil por un mismo patrón de respuesta, es decir, todos ellos tienen un no igual o arriba del promedio en todas las variables.


## Bloque 2
El bloque 2 solamente contiene una variable que se refiere al contacto que los ciudadanos tienen con su comunidad, según la frecuencia de contacto. Se destacan en ese bloque Guatemala, Honduras, Ecuador, Bolivia, Perú, Paraguay y República Dominica, países que, más adelante, veremos que tienen una mayor tendencia a la actividad política comunitaria.

## Bloque 3
El tercer bloque se refiere a la asistencia de los ciudadanos a determinados espacios de organización y/o reunión, según la frecuencia de contacto que tienen con esos espacios. Son espacios tanto institucionales/oficiales como espacios más informales y comunitarios, tales como reuniones de partidos políticos, grupos deportivos asociaciones laborales, entre otros.

En ese bloque se percibe una tendencia de actividad política extra-institucional por Guatemala, Ecuador, Bolivia, Perú, Paraguay y República Dominicana - en algunos casos Nicarágua y Colombia - como ciudadanos activos en espacios de participación ciudadana, como comité de juntas de mejoras para la comunidad, grupos recreativos y laborales, grupos de mujeres. Al tomar como referencia el promedio general, los datos sugieren que las personas de esos países tienen una mayor tendencia a involucrase en actividades participativas organizadas por los propios ciudadanos.

A parte de eso, tres puntos tienen que ser destacados en este bloque. El primero se refiere a la asistencia a reuniones de organización religiosa, la variable "cp6" (asistencia a reuiones de organización religiosa) parece medir más la "religiosidad" de la población que algún tipo de comportamiento político. El segundo es la asistencia a reuniones de partidos políticos/movimientos políticos, que tiene un patrón muy distinto de las otras respuestas a en este bloque, con destaque a la mayor asistencia para Nicarágua, Colombia, Paraguay y República Dominicana. La tercera se refiere a la alta cantidad de no aplicables en la variable "cp20", lo que se explica por el hecho que la pregunta fue realizada solamente a mujeres.

## Bloque 4
El cuarto bloque busca medir la asistencia a la arena municipal de política y a protestas, según su frecuencia. Aunque de manera reducida, algunos países tienen mayores tendencias a que sus ciudadanos asistan a reuniones que representan el poder municipal, como Guatemala, El Salvador, Honduras, Ecuador, Perú, Paraguay y República Dominicana, lo que presenta algunas contradicciones respecto a las variables "cp4a" y "np2" del bloque 1. Esas dos variables miden si el ciudadano pidió ayuda a la municipalidad, lo que no parece ser una tendencia en países como Paraguay y República Dominicana. Sin embargo, la diferencia entre promedios generales, 9.5% de "Sí" para la variable np1 mientras que cp4a tiene un "13.1%", puede explicar esa diferencia. Además, se percibe que las personas de los países analizados, a excepción de Bolivia, Perú y Paraguay, tienen una baja participación en protestas.

Una otra observación se refiere a la variable "prot7" debido a su muy elevado número de no aplicables. Eso ocurre debido al diseño de la encuesta: el encuestador solamente preguntaría "prot7" (participar en bloqueo de calle o espacio público) si el encuestado contestara "Sí" a "prot3" (participar en protesta o manifestación). En ese caso, la encuesta ha buscado medir a qué tipo de protesta/manifestación aquellos que realizan esa actividad han ido.

## Bloque 5
El bloque congrega 5 acciones políticas individuales, según la respuesta positiva o negativa del encuestado, a excepción de la variable "pp1" (convencer a votar). Cada variable tiene un patrón de respuesta muy distinto, debido a que miden comportamiento que no son semejantes. Aparentemente no hay ningún patrón en las variables seleccionadas para este bloque. 

Se destaca República Dominicana por presentar una tendencia de comportamiento político y de vínculo con la esfera política, pero, al mismo tiempo, tiene un reducido percentual de votos en elecciones presidenciales en comparación a los otros países. Hay un problema con la variable "prot6" (firmar petición), puesto que en Bolivia no fue realizada esa pregunta. Al consultar la encuesta específica del país, no hay la pregunta, aunque está presentada en el cuestionario general del año y no hay ninguna afirmación de porque ella no fue preguntada en el país.

También, fue añadida la variable "vic44" (organización comunitaria por temor a delicuencia), la cual enseña un poco del patrón de países con mayor participación ciudadana, como Guatemala, Ecuador, Bolivia, Perú y República Dominicana. Además, contiene la única variable referente a participación digital, la variable "prot8", en que se destacan países como Chile, Uruguay, Argentina y República Dominicana; respecto a "prot8", Ecuador no fue mencionado porque presenta un alto porcentaje de no aplicables. 

## Bloque 6
Este bloque contiene una variable distinta de análisis y, como solo tiene una variable, se podría unir al bloque 2. La variable "vb20" (que haría usted si fueran las elecciones) de la manera que es presentada no permite identificar claramente patrones, Se podría colapsar entre dos niveles, "votar" y "no votar", para mejor visualizarla. Sin embargo, contiene un elevado nivel de no aplicables (16%).

## Bloque 7
El último bloque contiene dos variables demográficas, sexo y edad. Respecto a sexo, se percibe que Chile tiene mucho más mujeres que hombres (65% mujeres y 35% hombres). En la información técnica sobre el país, no hay nada que justifique esa diferencia. Hay un párrafo que se repite en todas las informaciones técnicas respecto al uso de cuotas de género y edad para mejorar posibles desequilíbrios en la encuesta, pero no hay nada referente a esa gran diferencia observada en el caso chileno. Pienso que, quizás, por la muestra no estar ajustada ("*The AmericasBarometer samples of Chile are not self-weighted*"), sea un factor de explicación; sin embargo, eso también ocurre en otros países.

La presentación de los grupos de edad se dió a partir de un intervalo entre 6 y 6 años, con intervalo cerrado en el valor más bajo (16 años) hasta el último valor (94 años). Este intervalo fue seleccionado porque era el que permitía la mayor distribución de la edad con la mayor cantidad de grupos de edad según el sexo, sin que el gráfico esté demasiado grande.
