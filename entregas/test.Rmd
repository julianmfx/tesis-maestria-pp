---
title: "c14_mlm_protesta"
author: "jmf"
date: "`r Sys.Date()`"
output:
  pdf_document:
    toc: yes
  html_document:
    toc: yes
    toc_float: yes
    theme: cosmo
    highlight: zenburn
---

Este código tiene como función encontrar el mejor modelo multinivel para el tipo protesta.


```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)
```

# Librerías

```{r, message = FALSE}
library(dplyr)
# library(tidyverse) # to use ggplot
library(labelled) # to deal with "haven_labelled" variables (it is automatical);
library(nlme)
library(lme4) # paquete original
library(lmerTest) # paquete con pvalues
library(fitdistrplus) # to use fitdist()
library(multilevel) # to try new functions
library(r2mlm) # to calculate r-squared. No funciona muy bien
library(sjPlot) # to use tab_model()
library(lattice) # to use qqmath()
```


# Base de datos

La base de datos del modelo multinivel y sus variables están en el [código multinivel 14](./c14_multinivel.Rmd)

# Formalización

El modelo multinivel puede ser escrito de la determinada manera. En el nivel individual tenemos:


$$ Y_{ij} = \beta_{0,j} + \beta_{1}X_{ij}+ \epsilon_{ij} $$
En que:

- $Y_{ij}$ es el puntaje de protesta del individuo $i$ en el país $j$
- $\beta_{0,j}$ el el intercepto para el país $j$
- $\beta_{1}$ es el coeficiente de la pendiente para el efecto de la variable de individuo en el puntaje de protesta
- $X_{ij}$ es la variable de individuo $i$ en el país $j$
- $\epsilon_{ij}$ es el término de error distribuido normalmente con media 0 y varianza $\sigma_{\epsilon}^{2}$


En el nivel nacional tenemos:

$$ \beta_{0,j} = \gamma_{00} +  + \gamma_{0j}Z_{j} + u_{0,j} $$
En que:

 $\gamma_{00}$ es el intercepto general o también llamado de la grande média del modelo
- $\gamma_{0j}$ es el coeficiente de la pendiente para el efecto de la variable $Z$ en el país $j$
- $Z_{j}$ es la variable nacional del país $j$
- $u_{0,j}$ es el efecto aleatorio de del país $j$ o término de error del nivel pais, distribuído normalmente con media 0 y varianza $\sigma_{u_{0}}^{2}$

Al agregar las dos ecuaciones tenemos la fórmula completa del modelo multinivel:

$$ y_{ij} = \gamma_{00} + \gamma_{01}z_{j} + \beta_{1}x_{ij} + u_{0j} + \epsilon_{ij} $$

La primera ecuación representa la parte de los efectos fijos del modelo, donde se modela el puntaje de protesta de cada individuo $y_{ij}$ como una función lineal de las variables individuales $X_{ij}$ y nacionales $Z_{j}$, con variación a nivel del país en la intercepción. La segunda ecuación representa la parte de los efectos aleatorios del modelo, donde la intercepción para cada país $\beta_{0,j}$ se modela como una función de la intercepción general $\gamma_{00}$ y un efecto aleatorio $u_{0,j}$ distribuido normalmente con media cero y varianza $\sigma_{u_{0}}^{2}$.

Se asume que los términos de error $\epsilon_{ij}$ están distribuidos normalmente con media cero y varianza $\sigma_{\epsilon}^{2}$. El modelo supone que los errores y los efectos aleatorios son independientes y distribuidos de manera idéntica (iid) a través de individuos y países.

