Stack Overflow questions
===

[Question](https://stackoverflow.com/questions/72734267/latent-class-analysis-polca-is-removing-one-variable-from-the-analysis) made in 23/06/2022
===
# Problema con en análisis de LCA en Argentina 
I am performing a Latent Class Analysis in R through the poLCA package. When performing the analysis, it automatically quits one variable. The problem is that the "alert warning" doesn't match with the variable characteristics.

**Here is my code:**

```R=

library("haven")
library("summarytools")
library("poLCA")

AR2012_df <- as.data.frame(read_sav("AR2012_basedatos_1744791410Argentina LAPOP AmericasBarometer 2012 Rev1_W.sav"))

f <- (cbind(cp2, cp4a, cp4, np2, np1, cp8, cp13, prot3, prot7, prot6, cp5, prot8, vb2)~1)

LCA4 <- poLCA(f, AR2012_df, nclass=4, maxiter = 200000, nrep = 500)
```
First, I'm importing the database from .sav to a data frame. Then, I'm setting up the variables analyzed to implement the analysis.

This code return the alert: 
> "ALERT: at least one manifest variable contained only one
    outcome category, and has been removed from the analysis."

In this case, the variable removed was "prot3", but "prot3" attributes have more than one outcome category.

Using freq() function from "summary tools package": 

```python
freq(AR2012_df$prot3)


              Freq   % Valid   % Valid Cum.   % Total   % Total Cum.
----------- ------ --------- -------------- --------- --------------
          1    121      8.07           8.07      8.00           8.00
          2   1378     91.93         100.00     91.14          99.14
       <NA>     13                               0.86         100.00
      Total   1512    100.00         100.00    100.00         100.00

```
# Failed attempts of resolution
In order to resolve this problem, I've already tried to convert the database to .csv and creating a new data frame only with the analyzed variables, which didn't work.
Also, the formula of poLCA disponible in https://rdrr.io/cran/poLCA/src/R/poLCA.R affirm that this alert is based on this code:

```R
 if (any(sapply(lapply(as.data.frame(y),table),length)==1)) {
        y <- y[,!(sapply(apply(y,2,table),length)==1)]
        cat("\n ALERT: at least one manifest variable contained only one
    outcome category, and has been removed from the analysis. \n\n")
    }
```

Checking all my variables through this code, that is, applying: 

```R
sapply(lapply(as.data.frame(AR2012_df$prot3),table),length)==1
```
to the "prot3" variable and all the other variables that are in "f", **return the same result**, that is, FALSE, which also doesn't makes sense.
Therefore, I subsetted the original data frame only with the variables that resulted FALSE through sapply() formula above, to quit variables that resulted TRUE and that could be interfering in my analysis, but this also didn't work.

I'm using
- R version 4.2.0 (2022-04-22)
- Platform: x86_64-pc-linux-gnu (64-bit)
- Running under: Ubuntu 20.04.4 LTS
- poLCA_1.6.0.1
- haven_2.5.0
- summarytools_1.0.1

I've never posted something here, so, please, be patient ;D

